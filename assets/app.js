import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './styles/app.css';

// start the Stimulus application
import './bootstrap';
import { Popover } from 'bootstrap'

const popoverTriggerList = document.querySelectorAll('.btn-link[data-bs-content]');
[...popoverTriggerList].map(popoverTriggerEl => new Popover(popoverTriggerEl, { // eslint-disable-line
    html: true,
    placement: 'left',
    toggle: 'popover',
    trigger: 'hover focus',
}));