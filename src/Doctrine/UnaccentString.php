<?php

/**
 * Taken from https://github.com/nicovak/doctrine-unaccent-extension-postgres/blob/master/UnaccentString.php.
 */

namespace App\Doctrine;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

/**
 * Unaccent string using postgresql extension unaccent :
 * http://www.postgresql.org/docs/current/static/unaccent.html
 *
 * Usage : StringFunction UNACCENT(string)
 */
class UnaccentString extends FunctionNode
{
    private ?Node $string;

    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'UNACCENT('.$this->string->dispatch($sqlWalker).')';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);

        $this->string = $parser->StringPrimary();

        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}
