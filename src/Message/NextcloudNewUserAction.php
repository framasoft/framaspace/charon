<?php

namespace App\Message;

use App\Entity\Nextcloud\User;

class NextcloudNewUserAction
{
    private int $spaceId;
    private User $nextcloudUser;

    public function __construct(int $spaceId, User $nextcloudUser)
    {
        $this->spaceId = $spaceId;
        $this->nextcloudUser = $nextcloudUser;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }

    public function getNextcloudUser(): User
    {
        return $this->nextcloudUser;
    }
}
