<?php

namespace App\Message;

class NextcloudSpaceCreationFinalizeAction
{
    private int $spaceId;

    public function __construct(int $spaceId)
    {
        $this->spaceId = $spaceId;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }
}
