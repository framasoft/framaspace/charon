<?php

namespace App\Message;

class NextcloudPostInstallLastStepAction
{
    private int $spaceId;

    public function __construct(int $spaceId)
    {
        $this->spaceId = $spaceId;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }
}
