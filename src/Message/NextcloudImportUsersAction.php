<?php

namespace App\Message;

use App\Entity\Nextcloud\UserImport;

class NextcloudImportUsersAction
{
    private int $spaceId;
    private UserImport $userImport;

    public function __construct(int $spaceId, UserImport $userImport)
    {
        $this->spaceId = $spaceId;
        $this->userImport = $userImport;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }

    public function getUserImport(): UserImport
    {
        return $this->userImport;
    }
}
