<?php

namespace App\Message;

class AdminLoggingAction
{
    private string $title;
    private string $message;
    private array $options;

    public function __construct(string $title, string $message = '', array $options = [])
    {
        $this->title = $title;
        $this->message = $message;
        $this->options = $options;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
