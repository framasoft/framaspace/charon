<?php

namespace App\Message;

class NextcloudBatchAnnouncementAction
{
    private int $announcementId;

    public function __construct(int $announcementId)
    {
        $this->announcementId = $announcementId;
    }

    public function getAnnouncementId(): int
    {
        return $this->announcementId;
    }
}
