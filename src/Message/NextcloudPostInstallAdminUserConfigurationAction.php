<?php

namespace App\Message;

class NextcloudPostInstallAdminUserConfigurationAction
{
    private int $spaceId;
    private string $appPassword;

    public function __construct(int $spaceId, string $appPassword)
    {
        $this->spaceId = $spaceId;
        $this->appPassword = $appPassword;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }

    public function getAppPassword(): string
    {
        return $this->appPassword;
    }
}
