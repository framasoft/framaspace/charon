<?php

namespace App\Message;

class DiscoursePostAction
{
    private int $submissionId;

    public function __construct(int $submissionId)
    {
        $this->submissionId = $submissionId;
    }

    public function getSubmissionId(): int
    {
        return $this->submissionId;
    }
}
