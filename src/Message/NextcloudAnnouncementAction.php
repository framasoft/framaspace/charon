<?php

namespace App\Message;

class NextcloudAnnouncementAction
{
    private int $spaceId;
    private int $announcementId;

    public function __construct(int $spaceId, int $announcementId)
    {
        $this->spaceId = $spaceId;
        $this->announcementId = $announcementId;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }

    public function getAnnouncementId(): int
    {
        return $this->announcementId;
    }
}
