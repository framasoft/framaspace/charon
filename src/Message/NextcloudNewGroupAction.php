<?php

namespace App\Message;

use App\Entity\Nextcloud\Group;

class NextcloudNewGroupAction
{
    private int $spaceId;
    private Group $nextcloudGroup;

    public function __construct(int $spaceId, Group $nextcloudGroup)
    {
        $this->spaceId = $spaceId;
        $this->nextcloudGroup = $nextcloudGroup;
    }

    public function getSpaceId(): int
    {
        return $this->spaceId;
    }

    public function getNextcloudGroup(): Group
    {
        return $this->nextcloudGroup;
    }
}
