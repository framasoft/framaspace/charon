<?php

namespace App\Services;

use App\Repository\MonitoringReportRepository;
use App\Repository\SpaceRepository;
use App\Repository\SubmissionRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class Statistics
{
    public function __construct(private readonly SubmissionRepository $submissionRepository, private readonly SpaceRepository $spaceRepository, private readonly MonitoringReportRepository $monitoringReportRepository, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] private readonly string $quota)
    {
    }

    public function getPendingSubmissionCount(): int
    {
        return $this->submissionRepository->countPendingSubmissions();
    }

    public function getFailedMonitoringReportsCount()
    {
        return $this->monitoringReportRepository->countFailedMonitoringReportsLast24h();
    }

    public function getSpacesOverQuotaCount()
    {
        return $this->monitoringReportRepository->countSpacesOverQuota((float) $this->quota * 1_073_741_824);
    }

    public function getSpacesWithTooMuchUsedSpaceCount()
    {
        return $this->monitoringReportRepository->countSpacesOverQuota((float) $this->quota * 1_073_741_824 * 90 / 100);
    }

    public function getSpacesWithTooMuchUsersCount(): int
    {
        return $this->monitoringReportRepository->countSpacesWithMoreThanXUsers();
    }

    public function getActiveSpacesCount(): int
    {
        return $this->spaceRepository->countActiveSpaces();
    }

    public function getSpacesWithYearlyInactiveUsers(): int
    {
        return $this->monitoringReportRepository->countSpacesWithYearlyInactiveUsers();
    }

    public function getSpacesWith3MonthsInactiveUsers(): int
    {
        return $this->monitoringReportRepository->countSpacesWith3MonthsInactiveUsers();
    }
}
