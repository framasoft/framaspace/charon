<?php

namespace App\Services;

use App\Entity\MonitoringReport;
use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\HTTP\Nextcloud\ServerInfoClient;
use App\Repository\MonitoringReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

readonly class RefreshSpaceMonitoring
{
    public function __construct(private ServerInfoClient $serverInfoClient, private EntityManagerInterface $entityManager, private MonitoringReportRepository $monitoringReportRepository, private LoggerInterface $logger, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] private string $quota)
    {
    }

    public function refreshSpaceMonitoring(Space $space): MonitoringReport
    {
        $monitoringReport = new MonitoringReport();
        $monitoringReport->setSpace($space);

        try {
            $stats = $this->serverInfoClient->getStatistics($space);

            $freeSpace = $stats['nextcloud']['system']['freespace'];
            $usedSpace = (int) $this->quota * 1_073_741_824 - $freeSpace;
            $monitoringReport->setUsedSpace($usedSpace);
            $monitoringReport->setUsersNumber($stats['nextcloud']['storage']['num_users']);
            $monitoringReport->setUsersLastActive5MinutesNumber($stats['activeUsers']['last5minutes']);
            $monitoringReport->setUsersLastActive1HourNumber($stats['activeUsers']['last1hour']);
            $monitoringReport->setUsersLastActive24HNumber($stats['activeUsers']['last24hours']);
            $monitoringReport->setUsersLastActive7DaysNumber($stats['activeUsers']['last7days']);
            $monitoringReport->setUsersLastActive1MonthNumber($stats['activeUsers']['last1month']);
            $monitoringReport->setUsersLastActive3MonthNumber($stats['activeUsers']['last3months']);
            $monitoringReport->setUsersLastActive6MonthNumber($stats['activeUsers']['last6months']);
            $monitoringReport->setUsersLastActiveLastYearNumber($stats['activeUsers']['lastyear']);
            $monitoringReport->setFilesNumber($stats['nextcloud']['storage']['num_files']);
            $monitoringReport->setSharesNumber($stats['nextcloud']['shares']['num_shares']);
            $monitoringReport->setReturnStatusCode(200);
        } catch (NextcloudRemoteOCSException $e) {
            $monitoringReport->setReturnStatusCode((int) $e->getOcsStatusCode());
            $monitoringReport->setReturnErrorDetails($e->getOcsMessage());
        } catch (NextcloudRemoteHTTPException $e) {
            $statusCode = $e->getHttpStatusCode();
            if (401 === $statusCode) {
                $this->logger->error('We are not authorized to connect to statistics endpoint from space "'.$space->getDomain().'" (401)');
            } elseif (404 === $statusCode) {
                $this->logger->error('It seems space "'.$space->getDomain().'" doesn\'t exist (404)');
            }
            $monitoringReport->setReturnStatusCode($statusCode);
            $monitoringReport->setReturnErrorDetails($e->getResponse());
        } finally {
            $nextHour = random_int(12, 24);
            $monitoringReport->setNextRun((new \DateTime())->add(new \DateInterval('PT'.$nextHour.'H')));
        }

        $this->entityManager->persist($monitoringReport);
        $this->entityManager->flush();

        $this->monitoringReportRepository->removeNextRunForOldEntries($monitoringReport->getSpace()->getId(), $monitoringReport->getId());

        return $monitoringReport;
    }
}
