<?php

namespace App\Services\Notifications;

use App\Entity\Invite;
use App\Entity\Space;
use App\Entity\Submission;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Component\Translation\LocaleSwitcher;
use Symfony\Contracts\Translation\TranslatorInterface;

class Mailer implements AdminNotifierInterface
{
    private MailerInterface $mailer;
    private LocaleSwitcher $localeSwitcher;
    private TranslatorInterface $translator;
    private string $logEmailAddress;
    private bool $canSendEmailsToUsers;
    private int $daysBeforeRejectedSubmissionPurge;
    private int $daysBeforeSpacePurge;
    private int $minimumNotificationLevel;

    public function __construct(MailerInterface $mailer, LocaleSwitcher $localeSwitcher, ContainerBagInterface $params, TranslatorInterface $translator)
    {
        $this->mailer = $mailer;
        $this->localeSwitcher = $localeSwitcher;
        $this->translator = $translator;
        $this->logEmailAddress = $params->get('app.logging.email_address');
        $this->canSendEmailsToUsers = $params->get('app.email.users');
        $this->daysBeforeRejectedSubmissionPurge = $params->get('app.submission.days_before_purge');
        $this->daysBeforeSpacePurge = $params->get('app.space.days_before_purge');
        $this->minimumNotificationLevel = $params->get('app.notifications.mailer.min_level');
    }

    public function notify(string $title, string $body, array $options = []): void
    {
        $priority = $options['priority'] ?? AdminNotifierInterface::PRIORITY_DEFAULT;
        if ($priority < $this->minimumNotificationLevel) {
            return;
        }

        $email = (new Email())
            ->to($this->logEmailAddress)
            ->priority($this->convertPriority($priority))
            ->subject($this->prefixTitleFromNotificationType($title, $options))
            ->text($body);
        $this->mailer->send($email);
    }

    public function sendPendingEmail(Submission $submission): void
    {
        $this->localeSwitcher->setLocale($submission->getLocale());
        $email = (new TemplatedEmail())
            ->to(new Address($submission->getEmail(), $submission->getName()))
            ->subject($this->translator->trans('Your Frama.space registration request for {organisationName} is pending', ['organisationName' => $submission->getName()]))
            ->htmlTemplate('emails/pending.html.twig')
            ->context(['organisation' => $submission->getName(), 'daysBeforeRejectedSubmissionPurge' => $this->daysBeforeRejectedSubmissionPurge])
        ;
        $this->send($email);
    }

    public function sendRejectionEmail(Submission $submission): void
    {
        $this->localeSwitcher->setLocale($submission->getLocale());
        $email = (new TemplatedEmail())
            ->to(new Address($submission->getEmail(), $submission->getName()))
            ->subject($this->translator->trans('Sorry: your registration to the Frama.space service has been - at least temporarily - refused'))
            ->htmlTemplate('emails/rejected.html.twig')
            ->context(['domain' => $submission->getDomain(), 'organisation' => $submission->getName(), 'daysBeforeRejectedSubmissionPurge' => $this->daysBeforeRejectedSubmissionPurge])
        ;
        $this->send($email);
    }

    public function sendCreationSuccessEmail(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Good news: your registration to the Frama.space service has been accepted!'))
            ->htmlTemplate('emails/created.html.twig')
            ->context([
                'space' => $space,
                'organisation' => $space->getOrganizationName(),
            ])
        ;
        $this->send($email);
    }

    public function sendDeleteConfirmation(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space account has been deleted'))
            ->htmlTemplate('emails/deleted.html.twig')
            ->context([
                'space' => $space,
                'domain' => $space->getDomain().'.frama.space',
                'complete_deletion_date' => $space->getDeletedAt()->add(new \DateInterval('P'.$this->daysBeforeSpacePurge.'D')),
            ])
        ;
        $this->send($email);
    }

    public function sendReactivationConfirmation(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space account has been reactivated'))
            ->htmlTemplate('emails/reactivation.html.twig')
            ->context([
                'space' => $space,
                'domain' => $space->getDomain().'.frama.space',
            ])
        ;
        $this->send($email);
    }

    public function sendLoginByEmailToken(LoginLinkDetails $details, Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());

        $duration = $details->getExpiresAt()->diff(new \DateTimeImmutable());

        $email = (new TemplatedEmail())
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Sign-in on Frama.space'))
            ->htmlTemplate('emails/login.html.twig')
            ->context([
                'space' => $space,
                'organization' => $space->getOrganizationName(),
                'link' => $details->getUrl(),
                'duration' => $duration->format('%i'),
            ])
        ;
        $this->send($email);
    }

    public function sendInvitation(Invite $invite): void
    {
        $email = (new TemplatedEmail())
            ->to(new Address($invite->getEmail()))
            ->subject($this->translator->trans('Invitation to sign-up on Frama.space'))
            ->htmlTemplate('emails/invite.html.twig')
            ->context([
                'invite' => $invite,
            ])
        ;
        $this->send($email);
    }

    public function sendSubmissionDeletionConfirmation(Submission $submission): void
    {
        $email = (new TemplatedEmail())
            ->to(new Address($submission->getEmail()))
            ->subject($this->translator->trans('Pre-registration form data for {organisationName} deleted', ['organisationName' => $submission->getName()]))
            ->htmlTemplate('emails/submission_deleted.html.twig')
        ;
        $this->send($email);
    }

    private function send(Email $email): void
    {
        if ($this->canSendEmailsToUsers) {
            $this->mailer->send($email);
        }
    }

    private function convertPriority(?int $priority): int
    {
        return match ($priority) {
            AdminNotifierInterface::PRIORITY_LOW => Email::PRIORITY_LOW,
            AdminNotifierInterface::PRIORITY_HIGH => Email::PRIORITY_HIGH,
            default => Email::PRIORITY_NORMAL,
        };
    }

    private function prefixTitleFromNotificationType(string $title, array $options): string
    {
        return match ($options['type'] ?? AdminNotifierInterface::TYPE_DEFAULT) {
            AdminNotifierInterface::TYPE_INFO => 'ℹ️ '.$title,
            AdminNotifierInterface::TYPE_SUCCESS => '✅ '.$title,
            AdminNotifierInterface::TYPE_ERROR => '❌ '.$title,
            default => $title,
        };
    }
}
