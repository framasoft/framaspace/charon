<?php

namespace App\Services\Notifications;

use Gotify\Auth\Token;
use Gotify\Endpoint\Message;
use Gotify\Server;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class Gotify implements AdminNotifierInterface
{
    private string $serverEndpoint;
    /** @var string[] */
    private array $tokens = [];
    private bool $available = false;
    private int $minimumNotificationLevel;

    public function __construct(#[Autowire('%app.notifications.gotify.server%')] string $serverEndpoint, #[Autowire('%app.notifications.gotify.tokens%')] array $tokens, #[Autowire('%app.notifications.gotify.min_level%')] int $minimumNotificationLevel)
    {
        if ($serverEndpoint && count($tokens) > 0) {
            $this->serverEndpoint = $serverEndpoint;
            $this->tokens = $tokens;
            $this->available = true;
            $this->minimumNotificationLevel = $minimumNotificationLevel;
        }
    }

    public function notify(string $title, string $body, array $options = []): void
    {
        if (!$this->available) {
            return;
        }
        $priority = $options['priority'] ?? AdminNotifierInterface::PRIORITY_DEFAULT;
        if ($priority < $this->minimumNotificationLevel) {
            return;
        }

        $title = $this->prefixTitleFromNotificationType($title, $options);

        $server = new Server($this->serverEndpoint);
        foreach ($this->tokens as $token) {
            $auth = new Token($token);
            $message = new Message($server, $auth);
            $message->create($title, $body, $this->convertPriority($priority));
        }
    }

    // For now we're just using the same values
    private function convertPriority(?int $priority): int
    {
        return $priority ?? Message::PRIORITY_DEFAULT;
    }

    private function prefixTitleFromNotificationType(string $title, array $options): string
    {
        return match ($options['type'] ?? AdminNotifierInterface::TYPE_DEFAULT) {
            AdminNotifierInterface::TYPE_INFO => 'ℹ️ '.$title,
            AdminNotifierInterface::TYPE_SUCCESS => '✅ '.$title,
            AdminNotifierInterface::TYPE_ERROR => '❌ '.$title,
            default => $title,
        };
    }
}
