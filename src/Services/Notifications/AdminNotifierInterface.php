<?php

namespace App\Services\Notifications;

interface AdminNotifierInterface
{
    public const PRIORITY_LOW = 1;
    public const PRIORITY_DEFAULT = 4;
    public const PRIORITY_HIGH = 8;
    public const PRIORITY_ERROR = self::PRIORITY_HIGH;

    public const TYPE_INFO = 10;
    public const TYPE_SUCCESS = 50;
    public const TYPE_ERROR = 80;

    public const TYPE_DEFAULT = self::TYPE_INFO;

    /**
     * Notify.
     */
    public function notify(string $title, string $body, array $options = ['priority' => self::PRIORITY_DEFAULT, 'type' => self::TYPE_DEFAULT]): void;
}
