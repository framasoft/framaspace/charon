<?php

namespace App\Services;

use App\Exceptions\NoAvailablePahekoServer;
use App\PahekoServerManager;
use App\Services\Nextcloud\Generator;

class PahekoDetailsService
{
    public function __construct(private readonly Generator $generator, private readonly PahekoServerManager $pahekoServerManager)
    {
    }

    /**
     * @throws NoAvailablePahekoServer
     * @throws \Exception
     */
    public function getDetails(string $spaceDomain): array
    {
        return [
            'paheko_shared_server' => $this->pahekoServerManager->getNewPahekoServerDetails(),
            'paheko_shared_secret_key' => $this->generator->generatePahekoSharedKey(),
            'paheko_api_user' => $this->generator->generatePahekoAPIUserName($spaceDomain),
            'paheko_api_password' => $this->generator->generatePahekoAPIUserPassword(),
        ];
    }
}
