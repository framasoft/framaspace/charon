<?php

namespace App\Services;

use App\Controller\Admin\JobCrudController;
use App\Controller\Admin\SpaceCrudController;
use App\Controller\Admin\SubmissionCrudController;
use App\Entity\Job;
use App\Entity\Space;
use App\Entity\Submission;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\ComparisonType;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator as RootAdminURLGenerator;

class AdminUrlGenerator
{
    private RootAdminURLGenerator $adminUrlGenerator;

    public function __construct(RootAdminURLGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public function backendAdminSubmissionIndex(): string
    {
        return $this->adminUrlGenerator
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();
    }

    public function backendAdminSubmissionURL(Submission $submission): string
    {
        return $this->adminUrlGenerator
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($submission->getId())
            ->generateUrl();
    }

    public function backendAdminSpaceURL(Space $space): string
    {
        return $this->adminUrlGenerator
            ->setController(SpaceCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($space->getId())
            ->generateUrl();
    }

    public function backendAdminJobUrl(Job $job): string
    {
        return $this->adminUrlGenerator
            ->setController(JobCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($job->getId())
            ->generateUrl();
    }

    public function backendAdminErrorJobsUrl(): string
    {
        return $this->adminUrlGenerator
            ->setController(JobCrudController::class)
            ->setAction(Action::INDEX)
            ->set('filters[status][comparison]', ComparisonType::EQ)
            ->set('filters[status][value]', Job::JOB_STATUS_ERROR)
            ->generateUrl();
    }
}
