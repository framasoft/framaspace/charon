<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;

/**
 * This class is only used to easily provide the EntityManagerInterface to Command tests.
 */
class EntityManagerInterfaceProxy
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}
