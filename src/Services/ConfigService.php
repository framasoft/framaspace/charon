<?php

namespace App\Services;

use App\Repository\ConfigRepository;

readonly class ConfigService
{
    public function __construct(private ConfigRepository $configRepository)
    {
    }

    public function isGeneralUpgradeOngoing(): bool
    {
        return $this->configRepository->checkBoolean('upgrade_ongoing');
    }

    public function isDeploymentDisabled(): bool
    {
        return $this->configRepository->checkBoolean('deployment_disabled') || $this->isGeneralUpgradeOngoing();
    }
}
