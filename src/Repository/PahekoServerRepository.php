<?php

namespace App\Repository;

use App\Entity\PahekoServer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PahekoServer>
 */
class PahekoServerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PahekoServer::class);
    }

    public function getAvailableForNewSpacesPahekoServer(): ?PahekoServer
    {
        $qb = $this->createQueryBuilder('ps');

        return $qb
            ->leftJoin('App\Entity\Space', 's', Join::WITH, "JSON_GET_TEXT(s.details, 'paheko_shared_server') = ps.hostname")
            ->andHaving($qb->expr()->eq('ps.isFull', ':isFull'))
            ->andHaving($qb->expr()->lt($qb->expr()->count('s.id'), 'ps.maxServers'))
            ->setParameter('isFull', false)
            ->groupBy('ps.hostname', 'ps.maxServers', 'ps.isFull', 'ps.id')
            ->orderBy($qb->expr()->count('ps.id'), 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    //    /**
    //     * @return PahekoServer[] Returns an array of PahekoServer objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PahekoServer
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
