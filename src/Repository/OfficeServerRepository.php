<?php

namespace App\Repository;

use App\Entity\OfficeServer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OfficeServer>
 *
 * @method OfficeServer|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfficeServer|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfficeServer[]    findAll()
 * @method OfficeServer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfficeServerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfficeServer::class);
    }

    public function save(OfficeServer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(OfficeServer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getAvailableForNewSpacesOfficeServer(int $type): ?OfficeServer
    {
        return $this->findOneBy(['isFull' => false, 'type' => $type], ['createdAt' => 'ASC']);
    }
}
