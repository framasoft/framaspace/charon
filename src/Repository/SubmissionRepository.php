<?php

namespace App\Repository;

use App\Entity\Submission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Submission>
 *
 * @method Submission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Submission|null findOneBy(array $criteria, array $orderBy = null)
 * @method Submission[]    findAll()
 * @method Submission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubmissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Submission::class);
    }

    public function previous(Submission $submission): ?Submission
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->where($qb->expr()->eq('s.id', '('.$this->createQueryBuilder('ss')->select('MAX(ss.id)')->where('ss.id < :id')->getDQL().')'))
            ->setParameter(':id', $submission->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function next(Submission $submission): ?Submission
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->where($qb->expr()->eq('s.id', '('.$this->createQueryBuilder('ss')->select('MIN(ss.id)')->where('ss.id > :id')->getDQL().')'))
            ->setParameter(':id', $submission->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countApprovedSubmissions(): int
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where($qb->expr()->eq('s.status', ':status'))
            ->setParameter('status', Submission::SUBMISSION_STATUS_APPROVED)
            ->select([$qb->expr()->count('s.id').' AS submissionCount']);
        $query = $qb->getQuery();

        return $query->execute()[0]['submissionCount'];
    }

    public function countRejectedSubmissionsToRemove(int $days): int
    {
        $qb = $this->createQueryBuilder('s');

        return $this->submissionsToCleanQueryBuilder($days)
            ->select($qb->expr()->count('s.id').'AS submissionCount')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['submissionCount'];
    }

    public function getRejectedSubmissionsToRemove(int $days, int $maxResults = 50): array
    {
        return $this->submissionsToCleanQueryBuilder($days)
            ->setMaxResults($maxResults)
            ->getQuery()
            ->execute();
    }

    public function countPendingSubmissionsToReject(int $days): int
    {
        $qb = $this->createQueryBuilder('s');

        return $this->submissionsToRejectQueryBuilder($days)
            ->select($qb->expr()->count('s.id').'AS submissionCount')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['submissionCount'];
    }

    public function countPendingSubmissions(): int
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->where($qb->expr()->eq('s.status', ':status'))
            ->setParameter('status', Submission::SUBMISSION_STATUS_PENDING)
            ->select($qb->expr()->count('s.id').'AS submissionCount')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['submissionCount'];
    }

    /**
     * @return Submission[]
     */
    public function getPendingSubmissionsToReject(int $days, int $maxResults = 50): array
    {
        return $this->submissionsToRejectQueryBuilder($days)
            ->setMaxResults($maxResults)
            ->getQuery()
            ->execute();
    }

    private function submissionsToCleanQueryBuilder(int $days): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->lt('s.updatedAt', ':updatedAt'))
            ->setParameter('status', Submission::SUBMISSION_STATUS_REJECTED)
            ->setParameter('updatedAt', (new \DateTimeImmutable())->sub(new \DateInterval('P'.$days.'D')));
    }

    private function submissionsToRejectQueryBuilder(int $days): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->lt('s.updatedAt', ':updatedAt'))
            ->setParameter('status', Submission::SUBMISSION_STATUS_PENDING)
            ->setParameter('updatedAt', (new \DateTimeImmutable())->sub(new \DateInterval('P'.$days.'D')));
    }
}
