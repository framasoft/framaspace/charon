<?php

namespace App\Repository;

use App\Entity\MonitoringReport;
use App\Entity\Space;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MonitoringReport>
 */
class MonitoringReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MonitoringReport::class);
    }

    public function countFailedMonitoringReportsLast24h()
    {
        $qb = $this->createQueryBuilder('mr1');

        return $qb->where($qb->expr()->neq('mr1.returnStatusCode', ':returnStatusCode'))
            ->andWhere($qb->expr()->gte('mr1.createdAt', ':createdAt'))
            ->setParameter('createdAt', (new \DateTimeImmutable())->sub(new \DateInterval('P1D')))
            ->setParameter('returnStatusCode', 200)
            ->select($qb->expr()->countDistinct('mr1.space').' AS monitoringReportsCount')
            ->leftJoin('App\Entity\MonitoringReport', 'mr2', Join::WITH, '(mr1.space = mr2.space AND mr1.createdAt < mr2.createdAt)')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['monitoringReportsCount']
        ;
    }

    public function countSpacesWithMoreThanXUsers(int $nbUsers = 50)
    {
        $qb = $this->createQueryBuilder('mr1');

        return $qb->where($qb->expr()->gte('mr1.usersNumber', ':usersNumber'))
            ->andWhere($qb->expr()->gte('mr1.createdAt', ':createdAt'))
            ->setParameter('createdAt', (new \DateTimeImmutable())->sub(new \DateInterval('P1D')))
            ->setParameter('usersNumber', $nbUsers)
            ->select($qb->expr()->countDistinct('mr1.space').' AS monitoringReportsCount')
            ->leftJoin('App\Entity\MonitoringReport', 'mr2', Join::WITH, '(mr1.space = mr2.space AND mr1.createdAt < mr2.createdAt)')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['monitoringReportsCount']
        ;
    }

    public function countSpacesOverQuota(float $quota)
    {
        $qb = $this->createQueryBuilder('mr1');

        return $qb->where($qb->expr()->gte('mr1.usedSpace', ':quota'))
            ->andWhere($qb->expr()->gte('mr1.createdAt', ':createdAt'))
            ->setParameter('createdAt', (new \DateTimeImmutable())->sub(new \DateInterval('P1D')))
            ->setParameter('quota', $quota)
            ->select($qb->expr()->countDistinct('mr1.space').' AS monitoringReportsCount')
            ->leftJoin('App\Entity\MonitoringReport', 'mr2', Join::WITH, '(mr1.space = mr2.space AND mr1.createdAt < mr2.createdAt)')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['monitoringReportsCount']
        ;
    }

    public function getLastMonitoringReportForSpace(Space $space): ?MonitoringReport
    {
        return $this->findOneBy(['space' => $space], ['createdAt' => 'DESC']);
    }

    public function purgeOldMonitoringReports(int $days = 365)
    {
        $qb = $this->createQueryBuilder('mr');

        return $qb->where($qb->expr()->lte('mr.createdAt', ':createdAt'))
            ->setParameter('createdAt', (new \DateTimeImmutable())->sub(new \DateInterval('P'.$days.'D')))
            ->delete()
            ->getQuery()
            ->execute()
        ;
    }

    public function removeNextRunForOldEntries(int $spaceId, int $newEntryId)
    {
        $qb = $this->createQueryBuilder('mr');

        return $qb->where($qb->expr()->eq('mr.space', ':space'))
            ->andWhere($qb->expr()->neq('mr.id', ':newEntryId'))
            ->setParameter('space', $spaceId)
            ->setParameter('newEntryId', $newEntryId)
            ->set('mr.nextRun', null)
            ->getQuery()
            ->execute()
        ;
    }

    public function countSpacesWithYearlyInactiveUsers()
    {
        $qb = $this->createQueryBuilder('mr1');

        return $qb->where($qb->expr()->eq('mr1.usersLastActiveLastYearNumber', '0'))
            ->select($qb->expr()->countDistinct('mr1.space').' AS monitoringReportsCount')
            ->leftJoin('App\Entity\MonitoringReport', 'mr2', Join::WITH, '(mr1.space = mr2.space AND mr1.createdAt < mr2.createdAt)')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['monitoringReportsCount']
        ;
    }

    public function countSpacesWith3MonthsInactiveUsers()
    {
        $qb = $this->createQueryBuilder('mr1');

        return $qb->where($qb->expr()->eq('mr1.usersLastActive3MonthNumber', '0'))
            ->select($qb->expr()->countDistinct('mr1.space').' AS monitoringReportsCount')
            ->leftJoin('App\Entity\MonitoringReport', 'mr2', Join::WITH, '(mr1.space = mr2.space AND mr1.createdAt < mr2.createdAt)')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['monitoringReportsCount']
        ;
    }
}
