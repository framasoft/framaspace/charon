<?php

namespace App\Repository;

use App\Controller\Admin\DashboardController;
use App\Entity\MonitoringReport;
use App\Entity\OfficeServer;
use App\Entity\Space;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Space>
 *
 * @method Space|null find($id, $lockMode = null, $lockVersion = null)
 * @method Space|null findOneBy(array $criteria, array $orderBy = null)
 * @method Space[]    findAll()
 * @method Space[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Space::class);
    }

    public function findBiggestPortForOfficeServer(string $officeServer, int $min, int $max): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select("MAX(coalesce(CAST(JSON_GET_TEXT(s.details, 'office_port') AS integer), :min)) AS port")
            ->from(Space::class, 's')
            ->where("JSON_GET_TEXT(s.details, 'office_server') = :office_server")
            ->setParameter('office_server', $officeServer)
            // ->setParameter('max', $max)
            ->setParameter('min', $min)
            ->getQuery();

        return $query->getResult()[0]['port'] ?? $min;
    }

    public function findAvailablePortForSharedOfficeServer(OfficeServer $officeServer): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select(
                "CAST(JSON_GET_TEXT(s.details, 'shared_office_port') AS integer) AS port",
                $qb->expr()->count("JSON_GET_TEXT(s.details, 'shared_office_port')").' AS nb_spaces'
            )
            ->from(Space::class, 's')
            ->groupBy('port')
            ->where($qb->expr()->eq("JSON_GET_TEXT(s.details, 'shared_office_server')", ':office_server'))
            ->andHaving($qb->expr()->lte($qb->expr()->count("JSON_GET_TEXT(s.details, 'shared_office_port')"), ':max_servers'))
            ->orderBy('nb_spaces', 'DESC')
            ->setParameter('office_server', $officeServer->getHostname())
            ->setParameter('max_servers', $officeServer->getMaxServers())
            ->getQuery();
        $data = $query->getResult();
        // We take only ports that are not fully populated, depending on the maximum number of spaces for this office container type
        $not_full = array_filter($data, fn ($entry) => $entry['nb_spaces'] < $officeServer->getMaxServers());
        if (empty($not_full)) {
            // Everything is full, attribute a new port
            if (0 === count($data)) {
                return $officeServer->getMinimalPort() + 1;
            }
            $ports = array_column($data, 'port');
            if (1 === count($ports)) {
                // If there's only one element in ports, use it directly
                return $ports[0] + 1;
            }

            return max(...$ports) + 1;
        } else {
            // re-sort the array to populate the nearly full ones, just to be sure
            // we are able to remove containers as soon as possible to reduce the load on the server
            usort($not_full, fn ($entry1, $entry2) => $entry1['nb_spaces'] - $entry2['nb_spaces']);

            return $not_full[0]['port'];
        }
    }

    public function countSharedOfficeSpacesBySharedOfficeNameAndPort(string $sharedOfficeServerName, int $sharedOfficePort): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $data = $qb
            ->select($qb->expr()->count('s.id').'AS count')
            ->from(Space::class, 's')
            ->where($qb->expr()->eq("JSON_GET_TEXT(s.details, 'shared_office_server')", ':shared_office_server_name'))
            ->andWhere($qb->expr()->eq("JSON_GET_TEXT(s.details, 'shared_office_port')", ':shared_office_port'))
            ->andWhere($qb->expr()->in('s.office', ':shared_office_types'))
            ->setParameter('shared_office_server_name', $sharedOfficeServerName)
            ->setParameter('shared_office_port', $sharedOfficePort)
            ->setParameter('shared_office_types', Space::SPACE_OFFICE_SHARED_TYPES)
            ->getQuery()
            ->getResult();

        return $data[0]['count'];
    }

    public function countPahekoSpacesByServerName(string $pahekoServerName): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $data = $qb
            ->select($qb->expr()->count('s.id').'AS count')
            ->from(Space::class, 's')
            ->where($qb->expr()->eq("JSON_GET_TEXT(s.details, 'paheko_shared_server')", ':paheko_shared_server_name'))
            ->setParameter('paheko_shared_server_name', $pahekoServerName)
            ->getQuery()
            ->getResult();

        return $data[0]['count'];
    }

    public function findBiggestNcHpBackendPort(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select("MAX(coalesce(CAST(JSON_GET_TEXT(s.details, 'nc_hp_backend') AS integer), 10000)) AS port")
            ->from(Space::class, 's')
            ->getQuery();

        return $query->getResult()[0]['port'] ?? 10000;
    }

    public function countSpacesToClean(int $days): int
    {
        $qb = $this->createQueryBuilder('s');

        return $this->spacesToCleanQueryBuilder($days)
            ->select($qb->expr()->count('s.id').'AS spaceCount')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['spaceCount'];
    }

    public function getSpacesToClean(int $days, int $maxResults = 50): array
    {
        return $this->spacesToCleanQueryBuilder($days)
            ->setMaxResults($maxResults)
            ->getQuery()
            ->execute();
    }

    public function countNewSpacesByMonths(): array
    {
        $qb = $this->getActiveSpaces();

        $qb->select([$qb->expr()->count('s.id').' AS spaceCount', 'date_trunc(\'month\', s.createdAt) AS createdAtMonth'])
            ->groupBy('createdAtMonth')
            ->orderBy('createdAtMonth', 'DESC')
            ->setMaxResults(12);
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function countSpacesByTopics(): array
    {
        $qb = $this->getActiveSpaces();

        $qb->select(['LOWER(unaccent(json_array_elements_text(s.topics))) AS topic, '.$qb->expr()->count('s.id').' AS topicCount'])
            ->groupBy('topic')
            ->having($qb->expr()->gt($qb->expr()->count('s.id'), 1))
            ->orderBy('topicCount', 'DESC')
            ->setFirstResult(1)
            ->setMaxResults(30)
        ;
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function countSpacesByCreationYear(): array
    {
        $qb = $this->getActiveSpaces();

        $qb->select([$qb->expr()->count('s.id').' AS spaceCount', 's.creationYear'])
            ->groupBy('s.creationYear')
            ->orderBy('s.creationYear', 'ASC');
        $query = $qb->getQuery();

        return $query->execute();
    }

    /**
     * @psalm-return array{spaceCount: int, office: int}
     */
    public function mesureOfficeTypeDistribution(): array
    {
        $qb = $this->getActiveSpaces();

        $qb->select([$qb->expr()->count('s.id').' AS spaceCount', 's.office'])
            ->groupBy('s.office');
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function findAllQueryWithFilterQueryBuilder(?bool $enabled = null, ?int $status = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s')->orderBy('s.id', 'ASC');
        if (null !== $enabled) {
            $qb->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
                ->setParameter('enabled', $enabled);
        }
        if (in_array($status, Space::SPACE_STATUSES, true)) {
            $qb->andWhere($qb->expr()->eq('s.status', ':status'))
                ->setParameter('status', $status);
        }

        return $qb;
    }

    private function spacesToCleanQueryBuilder(int $days): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');

        return $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
            ->andWhere($qb->expr()->lt('s.deletedAt', ':deletionDate'))
            ->setParameter('status', Space::SPACE_STATUS_CREATED)
            ->setParameter('enabled', false)
            ->setParameter('deletionDate', (new \DateTimeImmutable())->sub(new \DateInterval('P'.$days.'D')));
    }

    /**
     * @psalm-return array{spaceCount: int, type: string}
     */
    public function countSpacesByOrganisationType(): array
    {
        $qb = $this->getActiveSpaces();

        $qb->setParameter('status', Space::SPACE_STATUS_CREATED)
            ->select([$qb->expr()->count('s.id').' AS spaceCount', 's.organisationType'])
            ->groupBy('s.organisationType');
        $query = $qb->getQuery();

        return $query->execute();
    }

    public function exportCreatedSpaces(): QueryBuilder
    {
        return $this->getActiveSpaces()
            ->select(array_map(fn ($name) => 's.'.$name, DashboardController::COLUMN_NAMES))
            ->orderBy('s.id', 'ASC');
    }

    public function countActiveSpaces()
    {
        $qb = $this->getActiveSpaces();

        return $qb
            ->select($qb->expr()->count('s.id').'AS spaceCount')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()['spaceCount'];
    }

    public function getSpacesToRefresh(): array
    {
        $qb = $this->getActiveSpaces();

        return $qb
            ->leftJoin('s.monitoringReports', 'mr')
            ->andWhere($qb->expr()->lte('mr.nextRun', ':nextRun'))
            ->andWhere($qb->expr()->eq('mr.updatedAt', '(SELECT MAX(mr2.updatedAt) FROM App\\Entity\\MonitoringReport mr2 WHERE mr2.space = mr.space)'))
            ->setParameter('nextRun', new \DateTime())
            ->getQuery()
            ->execute()
        ;
    }

    public function getUsersForSpaces()
    {
        $qb = $this->getActiveSpaces();

        return $qb
            ->select('mr.usersNumber', 's.domain')
            ->distinct()
            ->join('s.monitoringReports', 'mr')
            ->andWhere($qb->expr()->isNotNull('mr.usersNumber'))
            ->andWhere(
                $qb->expr()->eq(
                    'mr.createdAt',
                    '('.
                    $this->getEntityManager()->createQueryBuilder()
                        ->select($qb->expr()->max('mr2.createdAt'))
                        ->from(MonitoringReport::class, 'mr2')
                        ->where($qb->expr()->eq('mr2.space', 's.id'))
                        ->getDQL()
                    .')'
                )
            )
            ->orderBy('mr.usersNumber', 'DESC')
            ->getQuery()
            ->execute()
        ;
    }

    public function getDiskSpaceUsedSpaces()
    {
        $qb = $this->getActiveSpaces();

        return $qb
            ->select('mr.usedSpace', 's.domain')
            ->join('s.monitoringReports', 'mr')
            ->andWhere($qb->expr()->isNotNull('mr.usedSpace'))
            ->andWhere(
                $qb->expr()->eq(
                    'mr.createdAt',
                    '('.
                    $this->getEntityManager()->createQueryBuilder()
                        ->select($qb->expr()->max('mr2.createdAt'))
                        ->from(MonitoringReport::class, 'mr2')
                        ->where($qb->expr()->eq('mr2.space', 's.id'))
                        ->getDQL()
                    .')'
                )
            )
            ->orderBy('mr.usedSpace', 'DESC')
            ->getQuery()
            ->execute()
        ;
    }

    private function getActiveSpaces(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
            ->setParameter('status', Space::SPACE_STATUS_CREATED)
            ->setParameter('enabled', true);

        return $qb;
    }

    public function getSpacesWithoutPaheko(int $max)
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->where($qb->expr()->isNull("JSON_GET_TEXT(s.details, 'paheko_shared_server')"))
            ->orderBy('s.id', 'ASC')
            ->setMaxResults($max)
            ->getQuery()
            ->execute()
        ;
    }
}
