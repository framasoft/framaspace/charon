<?php

namespace App\Scheduler\Task;

use App\Entity\Job;
use App\Message\AdminLoggingAction;
use App\Repository\JobRepository;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\AdminNotifierInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Scheduler\Attribute\AsPeriodicTask;

#[AsPeriodicTask(frequency: '1 hour')]
readonly class CreatePendingErroredJobsReport
{
    public function __construct(private JobRepository $jobRepository, private MessageBusInterface $bus, private AdminUrlGenerator $adminUrlGenerator)
    {
    }

    public function __invoke(): void
    {
        $jobs = $this->jobRepository->findBy([
            'status' => Job::JOB_STATUS_ERROR,
        ]);
        $nbJobs = count($jobs);
        if (!$nbJobs) {
            return;
        }
        $erroredJobLines = [];
        $adminBackendAllErroredJobsURL = $this->adminUrlGenerator->backendAdminErrorJobsUrl();
        foreach ($jobs as $job) {
            $adminBackendURL = $this->adminUrlGenerator->backendAdminJobUrl($job);
            $spaceDomain = $job->getSpace()->getDomain();
            $erroredJobLines[] = '- Space domain: '.$spaceDomain.'.frama.space - Update date: '.$job->getUpdatedAt()->format(\DateTimeInterface::ATOM).' - Job Page: '.$adminBackendURL;
        }
        $this->bus->dispatch(new AdminLoggingAction('You have '.$nbJobs.' errored jobs', 'There are '.$nbJobs." jobs with the error status. Make sure this sounds right. To ignore the status for an errored job, set it's status to success.\n\nDetails:\n".implode("\n\n", $erroredJobLines)."\n\nView all errored jobs: ".$adminBackendAllErroredJobsURL, ['priority' => AdminNotifierInterface::PRIORITY_DEFAULT, 'type' => AdminNotifierInterface::TYPE_INFO]));
    }
}
