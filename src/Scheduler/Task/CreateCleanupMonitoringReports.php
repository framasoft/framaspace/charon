<?php

namespace App\Scheduler\Task;

use App\Repository\MonitoringReportRepository;
use App\Scheduler\Message\CleanupMonitoringReports;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
readonly class CreateCleanupMonitoringReports
{
    public function __construct(private MonitoringReportRepository $monitoringReportRepository, private LoggerInterface $logger)
    {
    }

    public function __invoke(CleanupMonitoringReports $cleanupMonitoringReports): void
    {
        $this->logger->debug('Started to cleanup old monitoring reports');
        $nbDeletedMonitoringReports = $this->monitoringReportRepository->purgeOldMonitoringReports();
        $this->logger->info(sprintf('Cleanup %d old monitoring reports', $nbDeletedMonitoringReports));
    }
}
