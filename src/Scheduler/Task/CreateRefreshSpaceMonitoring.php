<?php

namespace App\Scheduler\Task;

use App\Repository\SpaceRepository;
use App\Scheduler\Message\RefreshSpaceMonitoring;
use App\Services\RefreshSpaceMonitoring as RefreshSpaceMonitoringService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
readonly class CreateRefreshSpaceMonitoring
{
    public function __construct(private RefreshSpaceMonitoringService $refreshSpaceMonitoringService, private SpaceRepository $spaceRepository)
    {
    }

    public function __invoke(RefreshSpaceMonitoring $refreshSpaceMonitoring): void
    {
        $spaces = $this->spaceRepository->getSpacesToRefresh();
        foreach ($spaces as $space) {
            $this->refreshSpaceMonitoringService->refreshSpaceMonitoring($space);
        }
    }
}
