<?php

namespace App\Scheduler;

use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;
use Symfony\Contracts\Cache\CacheInterface;

#[AsSchedule(name: 'default')]
final readonly class RefreshSpaceMonitoring implements ScheduleProviderInterface
{
    public function __construct(
        private CacheInterface $cache,
    ) {
    }

    public function getSchedule(): Schedule
    {
        return (new Schedule())
            ->add(
                RecurringMessage::every('1 minutes', new Message\RefreshSpaceMonitoring())->withJitter(),
                RecurringMessage::every('1 day', new Message\CleanupMonitoringReports())->withJitter(),
            )
            ->stateful($this->cache)
        ;
    }
}
