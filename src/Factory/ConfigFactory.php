<?php

namespace App\Factory;

use App\Entity\Config;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

final class ConfigFactory extends PersistentProxyObjectFactory
{
    protected function defaults(): array|callable
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'type' => Config::TYPE_STRING,
            'key' => self::faker()->word(),
            'value' => self::faker()->word(),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    protected function initialize(): static
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
        ;
    }

    public static function class(): string
    {
        return Config::class;
    }
}
