<?php

namespace App\Factory;

use App\Entity\PahekoServer;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

/**
 * @extends PersistentProxyObjectFactory<PahekoServer>
 */
final class PahekoServerFactory extends PersistentProxyObjectFactory
{
    protected function initialize(): static
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Space $space): void {})
        ;
    }

    public static function class(): string
    {
        return PahekoServer::class;
    }

    protected function defaults(): array|callable
    {
        return [
            'hostname' => self::faker()->unique()->domainWord(),
            'isFull' => false,
            'maxServers' => self::faker()->randomDigitNotZero(),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }
}
