<?php

namespace App\Factory;

use App\Entity\Submission;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

final class SubmissionFactory extends PersistentProxyObjectFactory
{
    protected function defaults(): array|callable
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'type' => self::faker()->randomElement(Submission::SUBMISSION_TYPES),
            'name' => self::faker()->name(),
            'object' => self::faker()->text(),
            'topics' => self::faker()->words(),
            'mainActions' => self::faker()->text(),
            'domain' => self::faker()->unique()->domainWord(),
            'identity' => self::faker()->name(),
            'email' => self::faker()->email(),
            'status' => self::faker()->randomElement(Submission::SUBMISSION_STATUSES),
            'locale' => self::faker()->locale(),
            'alreadyUsed' => self::faker()->randomElements(Submission::SUBMISSION_ALREADY_USED_VALUES),
            'accountsNeeded' => 'something',
            'responsible' => self::faker()->randomElement(Submission::SUBMISSION_RESPONSIBLE_VALUES),
            'metadata' => [],
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    protected function initialize(): static
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Submission $submission): void {})
        ;
    }

    public static function class(): string
    {
        return Submission::class;
    }
}
