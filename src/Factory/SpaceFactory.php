<?php

namespace App\Factory;

use App\Entity\Space;
use App\SpaceManager;
use Zenstruck\Foundry\Persistence\PersistentProxyObjectFactory;

final class SpaceFactory extends PersistentProxyObjectFactory
{
    protected function initialize(): static
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Space $space): void {})
        ;
    }

    public static function class(): string
    {
        return Space::class;
    }

    protected function defaults(): array|callable
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'domain' => self::faker()->unique()->domainWord(),
            'details' => [
                'nc_instanceid' => 'ocga9hsuvgjy',
                'nc_admin_username' => SpaceManager::DEFAULT_ADMIN_USERNAME,
                'nc_admin_email' => self::faker()->email(),
                'nc_remote_admin_username' => SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME,
                'nc_remote_admin_password' => self::faker()->password(),
                'nc_remote_admin_email' => SpaceManager::DEFAULT_REMOTE_ADMIN_EMAIL,
                'nc_passwordsalt' => "rKdap5hz9jMWJ4iyzXCs0ZwnrldK\/y",
                'nc_secret' => 'uHP4W29JL0CXkTk+\/ZlpmItGW2V9vwmCN24KYsUByUrsSdnT',

                // Common office details
                'office_port' => 1026,
                'office_server' => 'office-1',

                // OnlyOffice details
                'oo_secret' => 'yGBp18zCqHzS0XG9wqnKXzZXnP4O9u',

                // High performance notify push server
                'nc_hp_backend' => 1027,
            ],
            'contactEmail' => self::faker()->email(),
            'contactName' => self::faker()->name(),
            'locale' => self::faker()->locale(),
            'status' => self::faker()->randomElement(Space::SPACE_STATUSES),
            'organizationName' => self::faker()->name(),
            'enabled' => self::faker()->boolean(),
            'office' => self::faker()->randomElement(Space::SPACE_OFFICE_TYPES),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'submission' => SubmissionFactory::createOne(),
        ];
    }
}
