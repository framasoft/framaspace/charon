<?php

namespace App\Form\Type;

use EasyCorp\Bundle\EasyAdminBundle\Form\Type\CodeEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @extends AbstractType<void>
 */
class JsonCodeEditorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->addModelTransformer(new CallbackTransformer(
                static fn (mixed $object) => json_encode($object, \JSON_THROW_ON_ERROR | \JSON_PRETTY_PRINT),
                static fn (string $json) => json_decode($json, true, 512, \JSON_THROW_ON_ERROR)
            ));
    }

    public function getParent(): string
    {
        return CodeEditorType::class;
    }
}
