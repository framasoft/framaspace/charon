<?php

namespace App\Form\Type;

use App\Entity\Space;
use App\Entity\Submission;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Twig\Environment;

/**
 * @extends AbstractType<Space>
 */
class SpaceType extends AbstractType
{
    private Environment $twig;

    public function __construct(Environment $twig, #[Autowire('%app.office.allow_change_type%')] private readonly bool $allowChangeType)
    {
        $this->twig = $twig;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Identification
            ->add('contactName', TextType::class, [
                'label' => new TranslatableMessage('Your name (or nickname)'),
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => new TranslatableMessage('Your email'),
            ])
            // Organisation
            ->add('organizationName', TextType::class, [
                'label' => new TranslatableMessage('Name of the organization'),
            ])
            ->add('futureOfficeType', ChoiceType::class, [
                'label' => new TranslatableMessage('Office solution type'),
                'row_attr' => ['class' => 'mb-3'],
                'expanded' => true,
                'multiple' => false,
                'disabled' => !$this->allowChangeType,
                'choices' => [
                    'Collabora Office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE,
                    'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                ],
                'label_html' => true,
                'choice_label' => function (int $choice) {
                    return match ($choice) {
                        Space::SPACE_OFFICE_TYPE_ONLYOFFICE => $this->twig->render('space/office/onlyoffice.html.twig'),
                        Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => $this->twig->render('space/office/collabora_online.html.twig'),
                        default => null,
                    };
                },
                'help' => new TranslatableMessage('We give more details and how to choose between the two solutions <a target="_blank" rel="noopener noreferrer" href="https://www.frama.space/abc/faq#q-text-editor">in our FAQ</a>. Nextcloud Text is always activated for simple text documents.'),
                'help_html' => true,
            ])
            ->add('externalIdentifier', TextType::class, ['label' => new TranslatableMessage('Identification number (SIREN / RNA)'), 'required' => false])
            ->add('organisationType', ChoiceType::class, [
                'choices' => array_flip(Submission::submissionTypesToNames()),
                'label' => new TranslatableMessage('Organization type'),
                'required' => true,
                'help' => new TranslatableMessage('If you are an association under Alsatian-Moselle local law (loi 1908), choose Association loi 1901'),
            ])
            ->add('extraType', TextType::class, [
                'label' => new TranslatableMessage('Please specify the type of your organisation'),
                'mapped' => false,
                'required' => false,
                'row_attr' => ['class' => 'd-none'],
                'label_attr' => ['class' => 'fw-normal fs-6 mt-2'],
            ])
            ->add('object', TextareaType::class, [
                'label' => new TranslatableMessage('Purpose or mission'),
                'constraints' => [new Length(min: 100)],
                'attr' => ['placeholder' => new TranslatableMessage('E.g.: "Framasoft is an association for popular education on digital issues".'), 'rows' => 5, 'minlength' => 100],
                'help' => new TranslatableMessage('Make an effort to give sufficient details ("Cultural association" is a bit light, for example), but remain concise (no need to write a novel). As a reminder, the purpose of an association is often indicated in its statutes. 100 characters required.'),
                // 'sanitize_html' => true,
                'required' => false,
            ])
            ->add('topics', ChoiceType::class, [
                'choices' => [
                    'Culture' => 'culture',
                    'Sport' => 'sport',
                    'Loisirs' => 'loisirs',
                    'Social' => 'social',
                    'Amicales / Entraide' => 'amicale',
                    'Éducation / Formation' => 'education',
                    'Économie' => 'economie',
                    'Santé' => 'sante',
                    'Environnement' => 'environnement',
                    'Défense des droits fondamentaux' => 'droits',
                    'Activités politiques' => 'politique',
                    'Activités religieuses, spirituelles, philosophiques' => 'religious_philo',
                    'Recherches' => 'recherches',
                    'Tourisme' => 'tourisme',
                    'Justice' => 'justice',
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => new TranslatableMessage('Fields of intervention'),
                // 'help' => "Vous pouvez indiquez un ou plusieurs domaines d'intervention de votre structure. N'hésitez pas à cocher \"Autres\" pour en indiquer d'autres.",
                'mapped' => false,
                'attr' => [
                    'class' => 'row mx-0 p-2 border rounded align-items-center',
                    'data_choice_row_class' => 'col-md-6',
                ],
            ])
            ->add('extraTopics', TextType::class, [
                'label' => new TranslatableMessage('Other'),
                'row_attr' => [
                    'class' => 'input-group input-group-sm',
                ],
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "civil protection, finance, legal aid, ergonomics, etc."')],
                'mapped' => false,
                'required' => false,
            ])
            ->add('mainActions', TextareaType::class, [
                'attr' => ['rows' => 5, 'minlength' => 300],
                'constraints' => [new Length(min: 300)],
                'help' => new TranslatableMessage('State here, in your own words, the main actions of the organization. 300 characters required.'),
                // 'sanitize_html' => true,
                'required' => false,
            ])
            ->add('creationYear', TextType::class, [
                'label' => new TranslatableMessage('Year of creation of the organization'),
                'required' => false,
                'attr' => ['placeholder' => 'ex: « 2001 »'],
            ])
            ->add('website', UrlType::class, [
                'label' => new TranslatableMessage('Website'),
                'required' => false,
                'help' => new TranslatableMessage('Address of your website (if you have one).'),
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "my-asso.com"')],
                'default_protocol' => 'http',
            ])
            ->add('country', CountryType::class, [
                'label' => new TranslatableMessage('Country of the organisation'),
                'choices' => [...array_flip(array_filter(Countries::getNames(), fn ($code) => in_array($code, ['FR', 'BE', 'CH', 'CA']), ARRAY_FILTER_USE_KEY)), 'Autre' => 'other'],
                'choice_loader' => null,
            ])
            ->add('nbBeneficiaries', IntegerType::class, [
                'label' => new TranslatableMessage('Number of beneficiaries'),
                'help' => new TranslatableMessage('According to you, how many people does your structure "reaches" each year? NB: this criterion is not discriminatory. We can perfectly well select for the test phase of Frama.space structures in training that do not yet reach the public.'),
                'help_html' => true,
                'required' => false,
            ])
            ->add('nbEmployees', IntegerType::class, [
                'label' => new TranslatableMessage('Number of employees'),
                'help' => new TranslatableMessage('Indicate the number of employees in Full Time Equivalent (the figure may be approximate. There is no need to make complex calculations)'),
                'required' => false,
            ])
            ->add('nbMembers', IntegerType::class, [
                'label' => new TranslatableMessage('Number of members'),
                'help' => new TranslatableMessage('If your organisation is an association or collective, indicate the number of members in your organisation.'),
                'required' => false,
            ])
            ->add('budget', MoneyType::class, [
                'label' => new TranslatableMessage('Annual budget'),
                'help' => new TranslatableMessage('If your organisation has an annual budget, please indicate it here (even approximately). If the organisation is a company, please indicate the "accounting" amount of the income, of your profit and loss account'),
                'required' => false,
                'html5' => true,
                'attr' => ['min' => 0],
            ])

            // Raisons
            ->add('reasons', TextareaType::class, [
                'label' => new TranslatableMessage('Reasons for needing a Frama.space account'),
                'constraints' => [new Length(min: 100)],
                'attr' => ['placeholder' => new TranslatableMessage('As a manager of my organization, I would like to open a Frama.space account because…'), 'rows' => 3, 'minlength' => 100],
                'required' => true,
                // 'sanitize_html' => true
                'help' => new TranslatableMessage('{count} characters required.', ['count' => 100]),
            ])
            ->add('save', SubmitType::class, ['label' => new TranslatableMessage('Save'), 'disabled' => $options['disabled']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Space::class,
            'sanitize_html' => true,
        ]);
    }
}
