<?php

namespace App\Form\Type;

use App\Entity\Submission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;

/**
 * @extends AbstractType<Submission>
 */
class SubmissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Identification
            ->add('identity', TextType::class, [
                'label' => new TranslatableMessage('Your name (or nickname)'),
                'attr' => ['placeholder' => new TranslatableMessage('ex: « Camille »')],
            ])
            ->add('email', EmailType::class, [
                'label' => new TranslatableMessage("Administrator's account email"),
                'attr' => [
                    'placeholder' => new TranslatableMessage('ex: « camille@mon-asso.fr »'),
                    'autocomplete' => 'email',
                ],
            ])
            ->add('responsible', ChoiceType::class, [
                'label' => new TranslatableMessage('I am one of the people in charge of the organisation'),
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Yes' => Submission::SUBMISSION_RESPONSIBLE,
                    'No' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                    "I'm not sure" => Submission::SUBMISSION_RESPONSIBLE_UNKNOWN,
                ],
            ])
            ->add('alreadyUsed', ChoiceType::class, [
                'label' => new TranslatableMessage('I have already used the free software Nextcloud'),
                'label_html' => true,
                'multiple' => true,
                'expanded' => true,
                'attr' => ['class' => 'p-2 border rounded'],
                'choices' => [
                    'As an administrator' => Submission::SUBMISSION_ALREADY_USED_AS_ADMIN,
                    'As an user' => Submission::SUBMISSION_ALREADY_USED_AS_USER,
                    'At a host of the <a href="https://chatons.org" target="_blank">CHATONS</a> collective' => Submission::SUBMISSION_ALREADY_USED_AT_CHATONS,
                    'I am not familiar with Nextcloud' => Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN,
                ],
                'help' => new TranslatableMessage("<a href=\"https://nextcloud.com/\" target='_blank' rel='noopener noreferrer'>Nextcloud</a> is the free/libre software behind Frama.space."),
                'help_html' => true,
            ])
            ->add('accountsNeeded', ChoiceType::class, [
                'label' => new TranslatableMessage('Estimated number of Frama.space accounts needed'),
                'help' => new TranslatableMessage('Indicate the number of Frama.space accounts you estimate you will need after one year. This number can be much lower than the number of members (for example, an association with 100 members might only need about 15 accounts for its employees and its Board of Directors)'),
                'choices' => [
                    '- Aucun -' => '',
                    '1 à 5' => '1-5',
                    '6 à 10' => '6-10',
                    '11 à 20' => '11-20',
                    '21 à 30' => '21-30',
                    '31 à 40' => '31-40',
                    '41 à 50' => '41-50',
                    '51 à 100' => '51-100',
                    '101 à 200' => '101-200',
                    '201 à 500' => '201-500',
                    '+ de 500' => '+500',
                ],
            ])
            // Organisation
            ->add('identifier', TextType::class, [
                'label' => new TranslatableMessage('Identification number (SIREN / RNA)'),
                'help' => new TranslatableMessage('This field is not mandatory. If it is filled in, it will be used to retrieve the information needed to automatically pre-fill the following fields.'),
                'required' => false,
                'attr' => ['placeholder' => 'ex: « 500715776 » ou « W751179246 »'],
            ])
            ->add('name', TextType::class, [
                'label' => new TranslatableMessage('Name of the organization'),
                'attr' => ['placeholder' => new TranslatableMessage('For example "Framasoft" or "Graine Auvergne Rhône Alpes" or "Squat de la rue Lepic".')],
                'required' => true,
            ])
            ->add('domain', TextType::class, [
                'label' => new TranslatableMessage('Frama.space identifier'),
                'help' => new TranslatableMessage('Choose your identifier carefully. It will be used to define the address of your Frama.space and cannot be changed.'),
                'help_html' => true,
                'required' => true,
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(Submission::submissionTypesToNames()),
                'label' => new TranslatableMessage('Organization type'),
                'required' => true,
                'help' => new TranslatableMessage('If you are an association under Alsatian-Moselle local law (loi 1908), choose Association loi 1901'),
            ])
            ->add('extraType', TextType::class, [
                'label' => new TranslatableMessage('Please specify the type of your organisation'),
                'mapped' => false,
                'required' => false,
                'row_attr' => ['class' => 'd-none'],
                'label_attr' => ['class' => 'fw-normal fs-6 mt-2'],
            ])
            ->add('country', CountryType::class, [
                'label' => new TranslatableMessage('Country of the organisation'),
                'choices' => [...array_flip(array_filter(Countries::getNames(), fn ($code) => in_array($code, ['FR', 'BE', 'CH', 'CA', 'LU', 'BI', 'BJ', 'CD', 'CG', 'CI', 'CM', 'CF', 'KM', 'DJ', 'GA', 'GN', 'GQ', 'HT', 'MC', 'MG', 'NE', 'RW', 'SC', 'SN', 'TD', 'TG', 'VU']), ARRAY_FILTER_USE_KEY)), 'Autre' => 'other'],
                'preferred_choices' => ['FR', 'BE', 'CH', 'CA', 'LU'],
                'choice_loader' => null,
            ])
            ->add('postalCode', TextType::class, [
                'label' => new TranslatableMessage('Postal code'),
                'required' => false,
                'row_attr' => ['class' => 'd-none'],
            ])
            ->add('organisationLanguage', LanguageType::class, [
                'label' => new TranslatableMessage('Main language of the organisation'),
                'preferred_choices' => [
                    'Français' => 'fr',
                    'English' => 'en',
                ],
            ])
            ->add('object', TextareaType::class, [
                'label' => new TranslatableMessage('Purpose or mission'),
                'constraints' => [new Length(min: 100)],
                'attr' => ['placeholder' => new TranslatableMessage('E.g.: "Framasoft is an association for popular education on digital issues".'), 'rows' => 5, 'minlength' => 100],
                'help' => new TranslatableMessage('Make an effort to give sufficient details ("Cultural association" is a bit light, for example), but remain concise (no need to write a novel). As a reminder, the purpose of an association is often indicated in its statutes. 100 characters required.'),
            ])
            ->add('topics', ChoiceType::class, [
                'choices' => [
                    'Culture' => 'culture',
                    'Sport' => 'sport',
                    'Loisirs' => 'loisirs',
                    'Social' => 'social',
                    'Amicales / Entraide' => 'amicale',
                    'Éducation / Formation' => 'education',
                    'Économie' => 'economie',
                    'Santé' => 'sante',
                    'Environnement' => 'environnement',
                    'Défense des droits fondamentaux' => 'droits',
                    'Activités politiques' => 'politique',
                    'Activités religieuses, spirituelles, philosophiques' => 'religious_philo',
                    'Recherches' => 'recherches',
                    'Tourisme' => 'tourisme',
                    'Justice' => 'justice',
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => new TranslatableMessage('Fields of intervention'),
                // 'help' => "Vous pouvez indiquez un ou plusieurs domaines d'intervention de votre structure. N'hésitez pas à cocher \"Autres\" pour en indiquer d'autres.",
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'row mx-0 p-2 border rounded align-items-center',
                    'data_choice_row_class' => 'col-md-6',
                ],
            ])
            ->add('extraTopics', TextType::class, [
                'label' => new TranslatableMessage('Other'),
                'row_attr' => [
                    'class' => 'input-group input-group-sm',
                ],
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "civil protection, finance, legal aid, ergonomics, etc."')],
                'mapped' => false,
                'required' => false,
            ])
            ->add('mainActions', TextareaType::class, [
                'attr' => ['rows' => 5, 'minlength' => 300],
                'constraints' => [new Length(min: 300)],
                'help' => new TranslatableMessage('State here, in your own words, the main actions of the organization. 300 characters required.'),
            ])
            ->add('creationYear', IntegerType::class, [
                'label' => new TranslatableMessage('Year of creation of the organization'),
                'required' => false,
                'attr' => ['placeholder' => 'ex: « 2001 »', 'min' => 1700, 'max' => 2030],
            ])
            ->add('website', UrlType::class, [
                'label' => new TranslatableMessage('Website'),
                'required' => false,
                'help' => new TranslatableMessage('Address of your website (if you have one).'),
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "my-asso.com"')],
                'default_protocol' => 'http',
            ])
            ->add('nbBeneficiaries', IntegerType::class, [
                'label' => new TranslatableMessage('Number of beneficiaries'),
                'help' => new TranslatableMessage('According to you, how many people does your structure "reaches" each year? NB: this criterion is not discriminatory. We can perfectly well select for the test phase of Frama.space structures in training that do not yet reach the public.'),
                'help_html' => true,
                'required' => false,
            ])
            ->add('nbEmployees', IntegerType::class, [
                'label' => new TranslatableMessage('Number of employees'),
                'help' => new TranslatableMessage('Indicate the number of employees in Full Time Equivalent (the figure may be approximate. There is no need to make complex calculations)'),
                'required' => false,
            ])
            ->add('nbMembers', IntegerType::class, [
                'label' => new TranslatableMessage('Number of members'),
                'help' => new TranslatableMessage('If your organisation is an association or collective, indicate the number of members in your organisation.'),
                'required' => false,
            ])
            ->add('budget', MoneyType::class, [
                'label' => new TranslatableMessage('Annual budget'),
                'help' => new TranslatableMessage('If your organisation has an annual budget, please indicate it here (even approximately). If the organisation is a company, please indicate the "accounting" amount of the income, of your profit and loss account'),
                'required' => false,
                'html5' => true,
                'attr' => ['min' => 0],
                'input' => 'integer',
            ])

            // Raisons
            ->add('reasons', TextareaType::class, [
                'label' => new TranslatableMessage('Reasons for needing a Frama.space account'),
                'attr' => ['placeholder' => new TranslatableMessage('As a manager of my organization, I would like to open a Frama.space account because…'), 'rows' => 3, 'minlength' => 100],
                'constraints' => [new Length(min: 100)],
                'required' => true,
                'help' => new TranslatableMessage('{count} characters required.', ['count' => 100]),
            ])
            ->add('acceptContact', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage('I agree to be contacted by Framasoft'),
                'constraints' => [new IsTrue()],
            ])
            ->add('betaWarning', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage('I agree to be contacted for the beta test phase'),
                'constraints' => [new IsTrue()],
            ])
            ->add('acceptCGU', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage('I have read and accept the ToS of the Framasoft association'),
                'help' => new TranslatableMessage("Our Terms of Use can be found at the following address: <a href=\"https://framasoft.org/cgu/\" target='_blank' rel='noopener noreferrer'>https://framasoft.org/cgu/</a>. If you do not comply with the law, we obviously reserve the right to suspend your account."),
                'help_html' => true,
                'constraints' => [new IsTrue()],
            ])
            ->add('acceptCSU', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage('I have read and accept the STU of the Frama.space service'),
                'help' => new TranslatableMessage("Our Specific Terms of Use can be found at <a href=\"https://www.frama.space/abc/csu\" target='_blank' rel='noopener noreferrer'>https://www.frama.space/abc/csu</a> If you fail to comply with these terms, we obviously reserve the right to suspend or delete your account."),
                'help_html' => true,
                'constraints' => [new IsTrue()],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-lg text-uppercase d-inline-flex'],
                'label' => new TranslatableMessage('Send application'),
            ]);
    }

    //    public function getBlockPrefix(): string
    //    {
    //        return 'createSubmission';
    //    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Submission::class,
            'attr' => ['class' => 'new-submission-form'],
        ]);
    }
}
