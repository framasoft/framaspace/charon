<?php

namespace App;

use App\Exceptions\NoAvailablePahekoServer;
use App\Repository\PahekoServerRepository;

class PahekoServerManager
{
    public function __construct(private readonly PahekoServerRepository $pahekoServerRepository)
    {
    }

    /**
     * @throws NoAvailablePahekoServer
     */
    public function getNewPahekoServerDetails(): string
    {
        $pahekoServer = $this->pahekoServerRepository->getAvailableForNewSpacesPahekoServer();
        if (!$pahekoServer) {
            throw new NoAvailablePahekoServer();
        }
        $hostname = $pahekoServer->getHostname();
        if (!$hostname) {
            throw new \ValueError("Paheko server hostname isn't defined");
        }

        return $hostname;
    }

    public function isPahekoServerFull(string $pahekoServer): bool
    {
        return null !== $this->pahekoServerRepository->findOneBy(['hostname' => $pahekoServer, 'isFull' => true]);
    }
}
