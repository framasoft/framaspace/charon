<?php

namespace App\Command;

use App\Entity\Space;
use App\Repository\SpaceRepository;
use App\Services\EntityManagerInterfaceProxy;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:maintenance:move-office-type-to-shared')]
class MoveOfficeTypeToShared extends Command
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        private SpaceRepository $spaceRepository,
        EntityManagerInterfaceProxy $entityManagerInterfaceProxy,
    ) {
        $this->entityManager = $entityManagerInterfaceProxy->getEntityManager();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:maintenance:move-office-type-to-shared')
            ->setDescription('Moves all Collabora stand-alone office server types to shared collabora office servers types')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'Maximum number of spaces to process', 50)
            ->setHelp('A one-time maintenance command to move stand-alone office server types to shared office server types')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $max = (int) $input->getOption('max');

        if ($max < 0) {
            $io->error('The number of spaces to process must be positive or zero.');

            return 1;
        }

        if (false !== $input->getOption('dry-run')) {
            $io->note('Dry mode enabled');

            $count = count($this->spaceRepository->findBy(['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'futureOfficeType' => null], null, $max));
        } else {
            $spaces = $this->spaceRepository->findBy(['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'futureOfficeType' => null], null, $max);
            $count = count($spaces);
            foreach ($spaces as $space) {
                $this->moveSpaceToSharedCollabora($space);
            }
        }

        $io->success(sprintf('Moved %d spaces to collabora office.', $count));

        return 0;
    }

    private function moveSpaceToSharedCollabora(Space $space): void
    {
        $space->setFutureOfficeType(Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
    }
}
