<?php

namespace App\Command;

use App\Entity\Job;
use App\Entity\Space;
use App\Repository\SpaceRepository;
use App\Services\EntityManagerInterfaceProxy;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(name: 'app:space:cleanup')]
class SpaceCleanupCommand extends Command
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        private SpaceRepository $spaceRepository,
        EntityManagerInterfaceProxy $entityManagerInterfaceProxy,
        #[Autowire('%app.space.days_before_purge%')] private int $daysBeforePurge,
    ) {
        $this->entityManager = $entityManagerInterfaceProxy->getEntityManager();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:space:cleanup')
            ->setDescription('Creates DELETE_SPACE jobs to remove space configuration and data')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
            ->addOption('days', 'd', InputOption::VALUE_OPTIONAL, 'Number of days after the deactivation', $this->daysBeforePurge)
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'Maximum number of spaces to process', 50)
            ->setHelp('Allows to purge deactivated spaces '.$this->daysBeforePurge.' days after their deactivation')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $days = (int) $input->getOption('days');
        $max = (int) $input->getOption('max');
        if ($days !== $this->daysBeforePurge) {
            if ($days < 0) {
                $io->error('The number of days must be positive or zero.');

                return 1;
            }
            $io->note('Using custom days after deactivation value: '.$days.' (default: '.$this->daysBeforePurge.')');
        }

        if ($max < 0) {
            $io->error('The number of spaces to process must be positive or zero.');

            return 1;
        }

        if (false !== $input->getOption('dry-run')) {
            $io->note('Dry mode enabled');

            $count = $this->spaceRepository->countSpacesToClean($days);
        } else {
            $spaces = $this->spaceRepository->getSpacesToClean($days, $max);
            $count = count($spaces);
            foreach ($spaces as $space) {
                $this->createDeletionJob($space);
            }
        }

        $io->success(sprintf('Started "%d" jobs to completely delete spaces.', $count));

        return 0;
    }

    private function createDeletionJob(Space $space): void
    {
        $job = (new Job())
            ->setType(Job::JOB_TYPE_DELETE_SPACE)
            ->setSpace($space)
            ->setData([...$space->getDetails(), 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'organisation_name' => $space->getOrganizationName()]);
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }
}
