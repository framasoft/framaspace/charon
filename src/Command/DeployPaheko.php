<?php

namespace App\Command;

use App\Entity\Job;
use App\Entity\Space;
use App\Exceptions\NoAvailablePahekoServer;
use App\Repository\SpaceRepository;
use App\Services\PahekoDetailsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(name: 'app:maintenance:deploy-paheko')]
class DeployPaheko extends Command
{
    public function __construct(
        private readonly SpaceRepository $spaceRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly PahekoDetailsService $pahekoDetailsService,
        private readonly MessageBusInterface $bus,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:maintenance:deploy-paheko')
            ->setDescription('Deploy Paheko for all the existing spaces')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'Maximum number of spaces to process', 50)
            ->addArgument('space', InputArgument::OPTIONAL, 'A specific space domain to use')
            ->setHelp('A one-time maintenance command to generate Paheko details and trigger deployment of Paheko spaces')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $max = (int) $input->getOption('max');

        if ($max < 0) {
            $io->error('The number of spaces to process must be positive or zero.');

            return 1;
        }

        $dryRun = false !== $input->getOption('dry-run');

        try {
            if ($specificSpace = $input->getArgument('space')) {
                $space = $this->spaceRepository->findOneBy(['domain' => $specificSpace, 'status' => Space::SPACE_STATUS_CREATED, 'enabled' => true]);

                if (!$space) {
                    $io->error(sprintf("Space with domain %s does not exist or isn't created or enabled", $specificSpace));

                    return 1;
                }

                if (isset($space->getDetails()['paheko_shared_server'])) {
                    $io->error(sprintf('Space with domain %s already has Paheko details', $specificSpace));

                    return 1;
                }

                if ($dryRun) {
                    $io->note('Dry mode enabled');
                    $details = $this->pahekoDetailsService->getDetails($space->getDomain());
                    $io->table(['Key', 'Generated values'], array_map(null, array_keys($details), array_values($details)));
                } else {
                    $this->saveDetailsForPaheko($space);
                }
                $io->success(sprintf('Deployed Paheko for space %s.', $space->getDomain()));

                return 0;
            }
            $spaces = $this->spaceRepository->getSpacesWithoutPaheko($max);

            if ($dryRun) {
                $io->note('Dry mode enabled');

                $count = count($spaces);
            } else {
                $spaces = $this->spaceRepository->getSpacesWithoutPaheko($max);
                $count = count($spaces);
                foreach ($spaces as $space) {
                    $this->saveDetailsForPaheko($space);
                }
            }
        } catch (NoAvailablePahekoServer) {
            $io->error('No available Paheko server to deploy spaces to');

            return 1;
        }

        $io->success(sprintf('Deployed Paheko for %d spaces.', $count));

        return 0;
    }

    /**
     * @throws NoAvailablePahekoServer
     */
    private function saveDetailsForPaheko(Space $space): void
    {
        $space->setDetails([...$space->getDetails(), ...$this->pahekoDetailsService->getDetails($space->getDomain())]);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
        $job = (new Job())
            ->setSpace($space)
            ->setType(Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO)
            ->setStatus(Job::JOB_STATUS_PENDING)
            ->setCreatedAt(new \DateTimeImmutable())
            ->setData([...$space->getDetails(), 'domain' => $space->getDomain(), 'organisation_name' => $space->getOrganizationName(), 'contact_name' => $space->getContactName(), 'country' => $space->getCountry() ?? 'FR', 'contact_email' => $space->getContactEmail(), 'office_type' => $space->getOffice()]);
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }
}
