<?php

namespace App\Command;

use App\Controller\Admin\SubmissionCrudController;
use App\Entity\Submission;
use App\Message\AdminLoggingAction;
use App\Repository\SubmissionRepository;
use App\Services\EntityManagerInterfaceProxy;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(name: 'app:submission:auto-rejection')]
class SubmissionAutomaticRejectionCommand extends Command
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        private SubmissionRepository $submissionRepository,
        EntityManagerInterfaceProxy $entityManagerInterfaceProxy,
        private Mailer $mailer,
        private LoggerInterface $logger,
        private MessageBusInterface $bus,
        private AdminUrlGenerator $adminURLGenerator,
        #[Autowire('%app.submission.days_before_automatic_rejection%')] private int $daysBeforeAutomaticRejection,
        #[Autowire('%app.submission.days_before_purge%')] private int $daysBeforePurge,
    ) {
        $this->entityManager = $entityManagerInterfaceProxy->getEntityManager();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:submission:auto-rejection')
            ->setDescription('Automatically rejects submissions after a while')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
            ->addOption('days', 'd', InputOption::VALUE_OPTIONAL, 'Number of days after the last update', $this->daysBeforeAutomaticRejection)
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'Maximum number of submissions to process', 50)
            ->setHelp('Allows to automatically reject submissions '.$this->daysBeforeAutomaticRejection.' days after their last update')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $days = (int) $input->getOption('days');
        $max = (int) $input->getOption('max');
        if ($days !== $this->daysBeforeAutomaticRejection) {
            if ($days < 0) {
                $io->error('The number of days must be positive or zero.');

                return 1;
            }
            $io->note('Using custom days after deactivation value: '.$days.' (default: '.$this->daysBeforeAutomaticRejection.')');
        }

        if ($max < 0) {
            $io->error('The number of submissions to process must be positive or zero.');

            return 1;
        }

        if (false !== $input->getOption('dry-run')) {
            $io->note('Dry mode enabled');

            $count = $this->submissionRepository->countPendingSubmissionsToReject($days);
        } else {
            $submissions = $this->submissionRepository->getPendingSubmissionsToReject($days, $max);
            $count = count($submissions);
            foreach ($submissions as $submission) {
                $submission->setStatus(Submission::SUBMISSION_STATUS_REJECTED);
                $this->entityManager->persist($submission);

                $this->logRejection($submission);

                $this->mailer->sendRejectionEmail($submission);
            }
            $this->entityManager->flush();
        }

        $io->success(sprintf('Rejected %d submissions.', $count));

        return 0;
    }

    private function logRejection(Submission $submission): void
    {
        $url = $this->adminURLGenerator
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($submission->getId())
            ->generateUrl();

        $this->logger->info('Submission n°'.$submission->getId().' ('.$submission->getName().' - '.$submission->getDomain().'.frama.space) was automatically rejected');
        $this->bus->dispatch(new AdminLoggingAction('Submission for space '.$submission->getName().' has been automatically rejected.', 'Submission for space '.$submission->getName().' ('.$submission->getDomain().').'."\nAccess the submission details: ".$url."\nThe submission data will be deleted in ".$this->daysBeforePurge.' days.', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }
}
