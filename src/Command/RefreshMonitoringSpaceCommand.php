<?php

namespace App\Command;

use App\Entity\MonitoringReport;
use App\Entity\Space;
use App\Repository\MonitoringReportRepository;
use App\Repository\SpaceRepository;
use App\Services\RefreshSpaceMonitoring;
use App\Twig\FormatBytesExtension;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(
    name: 'app:monitoring:refresh-space',
    description: 'Create a monitoring report for a space',
)]
class RefreshMonitoringSpaceCommand extends Command
{
    public const NB_SPACES_TO_FETCH_STATISTICS_AT_ONCE = 10;
    public const SECONDS_TO_WAIT_BETWEEN_EACH_BATCH = 5;

    public function __construct(private readonly SpaceRepository $spaceRepository, private readonly MonitoringReportRepository $monitoringReportRepository, private readonly RefreshSpaceMonitoring $refreshSpaceMonitoring, private readonly FormatBytesExtension $formatBytesExtension, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] private readonly string $quota)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('space', InputArgument::OPTIONAL, 'Space domain name')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Create monitoring reports for all')
            ->addOption('batch-size', 'b', InputOption::VALUE_REQUIRED, 'How much spaces to get statistics for at each request', self::NB_SPACES_TO_FETCH_STATISTICS_AT_ONCE)
            ->addOption('sleep', 's', InputOption::VALUE_REQUIRED, 'How many seconds to wait between each batch of requests', self::SECONDS_TO_WAIT_BETWEEN_EACH_BATCH)
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force space refresh if next run time has not been reached')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('space');
        $allOption = $input->getOption('all');
        $force = $input->getOption('force');
        if ($allOption && $arg1) {
            $io->error("Can't provide both space domain name and option --all");

            return Command::FAILURE;
        } elseif ($arg1) {
            return $this->singleSpace($io, $arg1, $force);
        } elseif ($allOption) {
            $batchSize = max(min((int) $input->getOption('batch-size'), 50), 0);
            $sleep = max(min((int) $input->getOption('sleep'), 60), 0);

            return $this->allSpaces($output, $io, $force, $batchSize, $sleep);
        }

        return Command::FAILURE;
    }

    private function singleSpace(SymfonyStyle $io, string $arg1, bool $force): int
    {
        $space = $this->spaceRepository->findOneBy(['domain' => $arg1]);
        if (!$space) {
            $io->error(sprintf('Space "%s" not found', $arg1));

            return Command::FAILURE;
        }
        if (Space::SPACE_STATUS_CREATED !== $space->getStatus()) {
            $io->error(sprintf('Space "%s" has not been created yet', $arg1));

            return Command::FAILURE;
        }

        if (!$space->isEnabled()) {
            $io->error(sprintf('Space "%s" has been disabled', $arg1));

            return Command::FAILURE;
        }

        $lastMonitoringReport = $this->monitoringReportRepository->getLastMonitoringReportForSpace($space);
        $hasRefreshed = false;

        if (!$force && $lastMonitoringReport && $lastMonitoringReport->getNextRun() > new \DateTime()) {
            $monitoringReport = $lastMonitoringReport;
        } else {
            $monitoringReport = $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
            $hasRefreshed = true;
        }

        if (200 === $monitoringReport->getReturnStatusCode()) {
            $io->definitionList(
                ['Used storage' => $this->formatBytesExtension->formatBytes((int) $monitoringReport->getUsedSpace()).' / '.$this->quota.'GB'],
                new TableSeparator(),
                ['Number of users' => $monitoringReport->getUsersNumber()],
                ['Number of users (last 24h)' => $monitoringReport->getUsersLastActive24HNumber()],
                new TableSeparator(),
                ['Number of files' => $monitoringReport->getFilesNumber()],
                ['Number of shares' => $monitoringReport->getSharesNumber()],
                new TableSeparator(),
                ['Monitoring report date' => $monitoringReport->getCreatedAt()->format(DATE_ATOM)],
            );
        } else {
            $io->definitionList(
                ['Error code returned' => $monitoringReport->getReturnStatusCode()],
                ['Details' => $monitoringReport->getReturnErrorDetails()],
                new TableSeparator(),
                ['Monitoring report date' => $monitoringReport->getCreatedAt()->format(DATE_ATOM)],
            );
        }

        if ($hasRefreshed) {
            $io->success(sprintf('Space %s has been monitored', $space->getDomain()));
        } else {
            $io->info(sprintf('Too soon to refresh space %s, using existing data. Use --force to override.', $space->getDomain()));
        }

        return Command::SUCCESS;
    }

    private function allSpaces(OutputInterface $output, SymfonyStyle $io, bool $force, int $batchSize, int $sleep): int
    {
        $output->writeln("Creating monitoring reports for all spaces with a batch size of $batchSize and $sleep seconds between each batch.", OutputInterface::VERBOSITY_VERBOSE);
        $allSpaces = $this->spaceRepository->findBy(['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true]);
        if ($force) {
            $spaces = $allSpaces;
        } else {
            $spaces = $this->spaceRepository->getSpacesToRefresh();
        }
        $nbSpaces = count($spaces);
        $nbAllSpaces = count($allSpaces);
        $chunkedSpaces = array_chunk($spaces, $batchSize);

        if (OutputInterface::VERBOSITY_NORMAL === $output->getVerbosity()) {
            $io->progressStart(count($chunkedSpaces));
        }

        $results = [];

        $output->writeln("Starting to refresh $nbSpaces spaces", OutputInterface::VERBOSITY_VERBOSE);

        foreach ($chunkedSpaces as $spaces) {
            foreach ($spaces as $space) {
                $results[] = $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
                $output->writeln("Refreshed monitoring for space $space.", OutputInterface::VERBOSITY_VERBOSE);
            }
            if (OutputInterface::VERBOSITY_NORMAL === $output->getVerbosity()) {
                $io->progressAdvance();
            }
            sleep($sleep);
        }

        if (OutputInterface::VERBOSITY_NORMAL === $output->getVerbosity()) {
            $io->progressFinish();
        }

        $erroredReports = array_filter($results, fn (MonitoringReport $spaceMonitoringReport) => 200 !== $spaceMonitoringReport->getReturnStatusCode());

        $nbErroredReports = count($erroredReports);
        $io->definitionList(
            ['Errored reports' => $nbErroredReports.' ('.(0 == $nbSpaces ? 0 : round($nbErroredReports * 100 / $nbSpaces)).'%)'],
            ['Total reports to refresh' => $nbSpaces.' ('.(0 == $nbAllSpaces ? 0 : round($nbSpaces * 100 / $nbAllSpaces)).'%)'],
            ['Total spaces' => $nbAllSpaces]
        );

        $io->success(sprintf('Monitored %d spaces successfully', $nbSpaces));

        return Command::SUCCESS;
    }
}
