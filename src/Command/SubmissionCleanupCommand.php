<?php

namespace App\Command;

use App\Repository\SubmissionRepository;
use App\Services\EntityManagerInterfaceProxy;
use App\Services\Notifications\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(name: 'app:submission:cleanup')]
class SubmissionCleanupCommand extends Command
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        private SubmissionRepository $submissionRepository,
        EntityManagerInterfaceProxy $entityManagerInterfaceProxy,
        private Mailer $mailer,
        #[Autowire('%app.submission.days_before_purge%')] private int $daysBeforePurge,
    ) {
        $this->entityManager = $entityManagerInterfaceProxy->getEntityManager();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:submission:cleanup')
            ->setDescription("Removes submission data after they're rejected")
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run')
            ->addOption('days', 'd', InputOption::VALUE_OPTIONAL, 'Number of days after the deactivation', $this->daysBeforePurge)
            ->addOption('max', 'm', InputOption::VALUE_OPTIONAL, 'Maximum number of submissions to process', 50)
            ->setHelp('Allows to purge rejected submissions '.$this->daysBeforePurge.' days after their deactivation')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $days = (int) $input->getOption('days');
        $max = (int) $input->getOption('max');
        if ($days !== $this->daysBeforePurge) {
            if ($days < 0) {
                $io->error('The number of days must be positive or zero.');

                return 1;
            }
            $io->note('Using custom days after deactivation value: '.$days.' (default: '.$this->daysBeforePurge.')');
        }

        if ($max < 0) {
            $io->error('The number of submissions to process must be positive or zero.');

            return 1;
        }

        if (false !== $input->getOption('dry-run')) {
            $io->note('Dry mode enabled');

            $count = $this->submissionRepository->countRejectedSubmissionsToRemove($days);
        } else {
            $submissions = $this->submissionRepository->getRejectedSubmissionsToRemove($days, $max);
            $count = count($submissions);
            foreach ($submissions as $submission) {
                $this->mailer->sendSubmissionDeletionConfirmation($submission);
                $this->entityManager->remove($submission);
            }
            $this->entityManager->flush();
        }

        $io->success(sprintf('Removed %d submissions.', $count));

        return 0;
    }
}
