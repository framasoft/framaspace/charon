<?php

namespace App\Events;

use App\Entity\Submission;
use Symfony\Contracts\EventDispatcher\Event;

class SubmissionCreatedEvent extends Event
{
    public const NAME = 'submission.created';

    private Submission $submission;

    public function __construct(Submission $submission)
    {
        $this->submission = $submission;
    }

    public function getSubmission(): Submission
    {
        return $this->submission;
    }
}
