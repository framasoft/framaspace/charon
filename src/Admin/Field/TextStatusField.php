<?php

namespace App\Admin\Field;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

final class TextStatusField implements FieldInterface
{
    use FieldTrait;

    /**
     * @param string|false|null $label
     */
    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)

            // this template is used in 'index' and 'detail' pages
            ->setTemplatePath('admin/field/text_status.html.twig')
            ->setMaximumValue(100)
            ->setDangerLevel(90)
            ->setWarningLevel(75)
            ->showPercentage(false)
        ;
    }

    public function setMaximumValue(float $value): TextStatusField
    {
        $this->setCustomOption('maximumValue', $value);

        return $this;
    }

    public function setDangerLevel(int $level): TextStatusField
    {
        $this->setCustomOption('dangerLevel', $level);

        return $this;
    }

    public function setWarningLevel(int $level): TextStatusField
    {
        $this->setCustomOption('warningLevel', $level);

        return $this;
    }

    public function showPercentage(bool $showPercentage = true): TextStatusField
    {
        $this->setCustomOption('showPercentage', $showPercentage);

        return $this;
    }
}
