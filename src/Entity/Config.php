<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\ConfigRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ConfigRepository::class)]
class Config
{
    use TimeStampableTrait;

    public const TYPE_STRING = 1;
    public const TYPE_INTEGER = 2;
    public const TYPE_FLOAT = 3;
    public const TYPE_BOOLEAN = 4;
    public const TYPE_ARRAY = 5;
    public const VALID_TYPES = [self::TYPE_STRING, self::TYPE_INTEGER, self::TYPE_FLOAT, self::TYPE_BOOLEAN, self::TYPE_ARRAY];

    #[ORM\Id]
    #[ORM\Column(length: 255)]
    #[OA\Property(example: 'number_of_candidates_max')]
    #[Assert\NotBlank]
    private ?string $key = null;

    #[ORM\Column]
    #[OA\Property(description: "The type of the configuration item.\nString: `1`\nInteger: `2`\nFloat: `3`\nBoolean: `4`\nArray: `5`", type: Types::INTEGER, enum: [self::TYPE_STRING, self::TYPE_INTEGER, self::TYPE_FLOAT, self::TYPE_BOOLEAN, self::TYPE_ARRAY], example: 2)]
    #[Assert\Choice([self::VALID_TYPES])]
    private ?int $type = self::TYPE_STRING;

    #[ORM\Column(type: Types::TEXT)]
    #[OA\Property(example: '57')]
    #[Assert\NotBlank]
    private ?string $value = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    private function getId(): ?string
    {
        return $this->getKey();
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    #[OA\Property(title: 'Typed value', type: ['string', 'number', 'boolean', 'array'], example: 57)]
    public function getTypedValue(): mixed
    {
        return match ($this->type) {
            self::TYPE_ARRAY => json_decode($this->value),
            self::TYPE_BOOLEAN => 'true' === $this->value,
            self::TYPE_FLOAT => (float) $this->value,
            self::TYPE_INTEGER => (int) $this->value,
            self::TYPE_STRING => (string) $this->value,
        };
    }
}
