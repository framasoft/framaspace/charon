<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\PahekoServerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PahekoServerRepository::class)]
class PahekoServer
{
    use TimeStampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $hostname = null;

    #[ORM\Column(nullable: true)]
    #[Assert\Positive]
    private ?int $maxServers = null;

    #[ORM\Column(options: ['default' => false])]
    private ?bool $isFull = false;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(string $hostname): static
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getMaxServers(): ?int
    {
        return $this->maxServers;
    }

    public function setMaxServers(?int $maxServers): static
    {
        $this->maxServers = $maxServers;

        return $this;
    }

    public function isFull(): ?bool
    {
        return $this->isFull;
    }

    public function setIsFull(bool $isFull): static
    {
        $this->isFull = $isFull;

        return $this;
    }
}
