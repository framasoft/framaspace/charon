<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\OfficeServerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OfficeServerRepository::class)]
class OfficeServer
{
    use TimeStampableTrait;
    public const OFFICE_TYPE_STANDALONE = 5;

    public const OFFICE_SERVER_TYPES = [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE, self::OFFICE_TYPE_STANDALONE];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private ?string $hostname = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Positive]
    private ?int $minimalPort = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Positive]
    private ?int $maximalPort = null;

    #[ORM\Column]
    #[Assert\Choice(choices: self::OFFICE_SERVER_TYPES)]
    private ?int $type = null;

    #[ORM\Column(options: ['default' => false])]
    private ?bool $isFull = false;

    /**
     * The maximum number of spaces for these office containers.
     */
    #[ORM\Column(nullable: true)]
    #[Assert\Positive]
    private ?int $maxServers = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getMinimalPort(): ?int
    {
        return $this->minimalPort;
    }

    public function setMinimalPort(int $minimalPort): self
    {
        $this->minimalPort = $minimalPort;

        return $this;
    }

    public function getMaximalPort(): ?int
    {
        return $this->maximalPort;
    }

    public function setMaximalPort(int $maximalPort): self
    {
        $this->maximalPort = $maximalPort;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString(): string
    {
        return $this->hostname.' ('.$this->minimalPort.' - '.$this->maximalPort.') - '.$this->type;
    }

    public function isIsFull(): ?bool
    {
        return $this->isFull;
    }

    public function setIsFull(bool $isFull): self
    {
        $this->isFull = $isFull;

        return $this;
    }

    /**
     * Returns the maximum number of spaces that can use an office container in that office server.
     */
    public function getMaxServers(): ?int
    {
        return $this->maxServers;
    }

    public function setMaxServers(?int $maxServers): self
    {
        $this->maxServers = $maxServers;

        return $this;
    }
}
