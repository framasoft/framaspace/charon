<?php

namespace App\Entity\Nextcloud;

class UserImport
{
    /** @var User[] */
    private array $users;
    /** @var Group[] */
    private array $groups;

    public function __construct(array $users, array $groups)
    {
        $this->users = $users;
        $this->groups = $groups;
    }

    public function getUsers(): array
    {
        return $this->users;
    }

    public function setUsers(array $users): UserImport
    {
        $this->users = $users;

        return $this;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function setGroups(array $groups): UserImport
    {
        $this->groups = $groups;

        return $this;
    }
}
