<?php

namespace App\Entity\Nextcloud;

class Group
{
    private string $groupId;

    public function getGroupId(): string
    {
        return $this->groupId;
    }

    public function setGroupId(string $groupId): Group
    {
        $this->groupId = $groupId;

        return $this;
    }
}
