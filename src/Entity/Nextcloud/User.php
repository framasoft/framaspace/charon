<?php

namespace App\Entity\Nextcloud;

use Symfony\Component\Validator\Constraints as Assert;

class User
{
    #[Assert\Regex('/^[a-zA-Z0-9 _.@\-\']+$/', message: 'User identifiers may only contain alphanumeric characters and the following symbols: _, @, - ')]
    #[Assert\Regex(pattern: '/framadmin/', message: "You can't import a user named \"framadmin\"", match: false)]
    private string $userId;
    private ?string $displayName = null;
    #[Assert\Email]
    private string $email;
    /** @var string[] */
    private array $groupIds = [];
    private array $subAdmin = [];
    private ?string $language = null;
    private ?string $quota = null;

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): User
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(?string $displayName): User
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getGroupIds(): array
    {
        return $this->groupIds;
    }

    public function setGroupIds(array $groupIds): User
    {
        $this->groupIds = $groupIds;

        return $this;
    }

    public function getSubAdmin(): array
    {
        return $this->subAdmin;
    }

    public function setSubAdmin(array $subadmin): User
    {
        $this->subAdmin = $subadmin;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): User
    {
        $this->language = $language;

        return $this;
    }

    public function getQuota(): ?string
    {
        return $this->quota;
    }

    public function setQuota(?string $quota): User
    {
        $this->quota = $quota;

        return $this;
    }
}
