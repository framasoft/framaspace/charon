<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\AnnouncementRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AnnouncementRepository::class)]
class Announcement
{
    use TimeStampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $message = null;

    #[ORM\Column(options: ['default' => false])]
    private bool $onlyAdmins = false;

    #[ORM\Column(options: ['default' => true])]
    private bool $createActivities = true;

    #[ORM\Column(options: ['default' => true])]
    private bool $createNotifications = true;

    #[ORM\Column(options: ['default' => true])]
    private bool $sendEmails = true;

    #[ORM\Column(options: ['default' => true])]
    private bool $allowComments = true;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function isOnlyAdmins(): ?bool
    {
        return $this->onlyAdmins;
    }

    public function setOnlyAdmins(bool $onlyAdmins): self
    {
        $this->onlyAdmins = $onlyAdmins;

        return $this;
    }

    public function isCreateActivities(): ?bool
    {
        return $this->createActivities;
    }

    public function setCreateActivities(bool $createActivities): self
    {
        $this->createActivities = $createActivities;

        return $this;
    }

    public function isCreateNotifications(): ?bool
    {
        return $this->createNotifications;
    }

    public function setCreateNotifications(bool $createNotifications): self
    {
        $this->createNotifications = $createNotifications;

        return $this;
    }

    public function isSendEmails(): ?bool
    {
        return $this->sendEmails;
    }

    public function setSendEmails(bool $sendEmails): self
    {
        $this->sendEmails = $sendEmails;

        return $this;
    }

    public function isAllowComments(): ?bool
    {
        return $this->allowComments;
    }

    public function setAllowComments(bool $allowComments): self
    {
        $this->allowComments = $allowComments;

        return $this;
    }
}
