<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\SpaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes as OA;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SpaceRepository::class)]
#[ORM\Index(name: 'space_status_idx', columns: ['status'])]
#[ORM\HasLifecycleCallbacks]
class Space implements UserInterface
{
    use TimeStampableTrait;
    public const SPACE_STATUS_PENDING = 0;
    public const SPACE_STATUS_CREATED = 10;
    public const SPACE_STATUSES = [self::SPACE_STATUS_PENDING, self::SPACE_STATUS_CREATED];

    public const SPACE_OFFICE_TYPE_COLLABORA_OFFICE = 10;
    public const SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE = 30;
    public const SPACE_OFFICE_TYPE_ONLYOFFICE = 20;
    public const SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE = 40;
    public const SPACE_OFFICE_TYPE_DEFAULT = self::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE;

    public const SPACE_OFFICE_SHARED_TYPES = [self::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, self::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE];

    public const SPACE_OFFICE_STANDALONE_TYPES = [self::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, self::SPACE_OFFICE_TYPE_ONLYOFFICE];

    public const SPACE_OFFICE_TYPES = [self::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, self::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, self::SPACE_OFFICE_TYPE_ONLYOFFICE, self::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE];

    public const SPACE_DISABLING_REASON_BY_SPACE_ADMIN = 10;
    public const SPACE_DISABLING_REASON_BY_INACTIVITY = 20;

    #[Groups('space')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 50)]
    #[Assert\Regex('/^[a-z0-9][a-z0-9-]*[a-z0-9]$/')]
    #[Assert\NoSuspiciousCharacters]
    private ?string $domain;

    #[Ignore]
    #[ORM\OneToMany(targetEntity: Job::class, mappedBy: 'space', orphanRemoval: true)]
    private Collection $jobs;

    #[Groups('space')]
    #[ORM\Column(type: 'json')]
    #[OA\Property(ref: '#/components/schemas/SpaceDetails')]
    private array $details = [];

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    #[Assert\NotBlank]
    #[Assert\NoSuspiciousCharacters]
    private ?string $contactEmail;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $contactName;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Locale]
    private ?string $locale = 'en';

    #[Groups('space')]
    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    private ?int $status = self::SPACE_STATUS_PENDING;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\NoSuspiciousCharacters]
    private ?string $organizationName;

    #[Groups('space')]
    #[ORM\Column]
    private ?bool $enabled = true;

    #[Groups('space')]
    #[ORM\Column(type: 'integer')]
    private ?int $office = self::SPACE_OFFICE_TYPE_DEFAULT;

    #[Ignore]
    #[ORM\OneToOne(inversedBy: 'space', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Submission $submission = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $futureOfficeType = null;

    #[ORM\Column(nullable: true)]
    private ?int $disabledReason = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $object = null;

    #[ORM\Column(type: Types::JSON, options: ['default' => '[]'])]
    #[OA\Property(type: 'array', items: new OA\Items(type: 'string'))]
    private array $topics = [];

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $mainActions = null;

    #[ORM\Column(type: 'integer', nullable: true, )]
    private ?int $creationYear = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $website = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $nbEmployees = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $nbMembers = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $nbBeneficiaries = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $budget = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $accountsNeeded = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $country = null;

    #[ORM\Column(options: ['default' => Submission::SUBMISSION_RESPONSIBLE])]
    private ?int $responsible = Submission::SUBMISSION_RESPONSIBLE;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $organisationLanguage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $organisationType = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $reasons = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $externalIdentifier = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $postalCode = null;

    /**
     * @var Collection<int, MonitoringReport>
     */
    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'space', targetEntity: MonitoringReport::class, orphanRemoval: true)]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private Collection $monitoringReports;

    #[ORM\Column(options: ['default' => false])]
    private ?bool $maintenanceActivated = false;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $maintenanceMessage = null;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->monitoringReports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(?string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->setSpace($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getSpace() === $this) {
                $job->setSpace(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @psalm-return array<string,mixed>
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    public function setDetails(array $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrganizationName(): ?string
    {
        return $this->organizationName;
    }

    public function setOrganizationName(?string $organizationName): self
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    #[Ignore]
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials(): void
    {
        // Nothing to do
    }

    #[Ignore]
    public function getUserIdentifier(): string
    {
        return $this->contactEmail;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getOffice(): ?int
    {
        return $this->office;
    }

    public function setOffice(?int $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function __toString(): string
    {
        return $this->domain ?? 'space without domain';
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getFutureOfficeType(): ?int
    {
        return $this->futureOfficeType ?? $this->office;
    }

    public function setFutureOfficeType(?int $futureOfficeType): self
    {
        $this->futureOfficeType = $futureOfficeType;

        return $this;
    }

    public function getDisabledReason(): ?int
    {
        return $this->disabledReason;
    }

    public function setDisabledReason(?int $disabledReason): self
    {
        $this->disabledReason = $disabledReason;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(?string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function setTopics(array $topics): self
    {
        $this->topics = $topics;

        return $this;
    }

    public function getMainActions(): ?string
    {
        return $this->mainActions;
    }

    public function setMainActions(?string $mainActions): self
    {
        $this->mainActions = $mainActions;

        return $this;
    }

    public function getCreationYear(): ?int
    {
        return $this->creationYear;
    }

    public function setCreationYear(?int $creationYear): self
    {
        $this->creationYear = $creationYear;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getNbEmployees(): ?string
    {
        return $this->nbEmployees;
    }

    public function setNbEmployees(?string $nbEmployees): self
    {
        $this->nbEmployees = $nbEmployees;

        return $this;
    }

    public function getNbMembers(): ?string
    {
        return $this->nbMembers;
    }

    public function setNbMembers(?string $nbMembers): self
    {
        $this->nbMembers = $nbMembers;

        return $this;
    }

    public function getNbBeneficiaries(): ?string
    {
        return $this->nbBeneficiaries;
    }

    public function setNbBeneficiaries(?string $nbBeneficiaries): self
    {
        $this->nbBeneficiaries = $nbBeneficiaries;

        return $this;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(?string $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getAccountsNeeded(): ?string
    {
        return $this->accountsNeeded;
    }

    public function setAccountsNeeded(?string $accountsNeeded): self
    {
        $this->accountsNeeded = $accountsNeeded;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getResponsible(): ?int
    {
        return $this->responsible;
    }

    public function setResponsible(?int $responsible): self
    {
        $this->responsible = $responsible;

        return $this;
    }

    public function getOrganisationLanguage(): ?string
    {
        return $this->organisationLanguage;
    }

    public function setOrganisationLanguage(?string $organisationLanguage): self
    {
        $this->organisationLanguage = $organisationLanguage;

        return $this;
    }

    public function getOrganisationType(): ?string
    {
        return $this->organisationType;
    }

    public function setOrganisationType(?string $organisationType): self
    {
        $this->organisationType = $organisationType;

        return $this;
    }

    public static function fromSubmission(Submission $submission): Space
    {
        return (new Space())
            ->setDomain($submission->getDomain())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setContactEmail($submission->getEmail())
            ->setContactName($submission->getIdentity())
            ->setLocale($submission->getLocale())
            ->setOrganizationName($submission->getName())
            ->setSubmission(null)

            ->setObject($submission->getObject())
            ->setTopics($submission->getTopics())
            ->setMainActions($submission->getMainActions())
            ->setCreationYear($submission->getCreationYear())
            ->setWebsite($submission->getWebsite())
            ->setNbEmployees($submission->getNbEmployees())
            ->setNbMembers($submission->getNbMembers())
            ->setNbBeneficiaries($submission->getNbBeneficiaries())
            ->setBudget($submission->getBudget())
            ->setAccountsNeeded($submission->getAccountsNeeded())
            ->setCountry($submission->getCountry())
            ->setPostalCode($submission->getPostalCode())
            ->setResponsible($submission->getResponsible())
            ->setOrganisationLanguage($submission->getOrganisationLanguage())
            ->setOrganisationType($submission->getType())
            ->setReasons($submission->getReasons())
            ->setExternalIdentifier($submission->getIdentifier())
        ;
    }

    public function getReasons(): ?string
    {
        return $this->reasons;
    }

    public function setReasons(?string $reasons): self
    {
        $this->reasons = $reasons;

        return $this;
    }

    public function getExternalIdentifier(): ?string
    {
        return $this->externalIdentifier;
    }

    public function setExternalIdentifier(?string $externalIdentifier): self
    {
        $this->externalIdentifier = $externalIdentifier;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): static
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return Collection<int, MonitoringReport>
     */
    public function getMonitoringReports(): Collection
    {
        return $this->monitoringReports;
    }

    public function addMonitoringReport(MonitoringReport $monitoringReport): static
    {
        if (!$this->monitoringReports->contains($monitoringReport)) {
            $this->monitoringReports->add($monitoringReport);
            $monitoringReport->setSpace($this);
        }

        return $this;
    }

    public function removeMonitoringReport(MonitoringReport $monitoringReport): static
    {
        if ($this->monitoringReports->removeElement($monitoringReport)) {
            // set the owning side to null (unless already changed)
            if ($monitoringReport->getSpace() === $this) {
                $monitoringReport->setSpace(null);
            }
        }

        return $this;
    }

    public function isMaintenanceActivated(): ?bool
    {
        return $this->maintenanceActivated;
    }

    public function setMaintenanceActivated(bool $maintenanceActivated): static
    {
        $this->maintenanceActivated = $maintenanceActivated;

        return $this;
    }

    public function getMaintenanceMessage(): ?string
    {
        return $this->maintenanceMessage;
    }

    public function setMaintenanceMessage(?string $maintenanceMessage): static
    {
        $this->maintenanceMessage = $maintenanceMessage;

        return $this;
    }
}
