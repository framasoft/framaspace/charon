<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\MonitoringReportRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: MonitoringReportRepository::class)]
#[ORM\Index(name: 'monitoring_report_active_last_year_number_updated_at_idx', columns: ['users_last_active_last_year_number', 'updated_at'])]
#[ORM\Index(name: 'monitoring_report_active3_month_number_updated_at_idx', columns: ['users_last_active3_month_number', 'updated_at'])]
#[ORM\Index(name: 'monitoring_report_space_id_updated_at_idx', columns: ['space_id', 'updated_at'])]
class MonitoringReport
{
    use TimeStampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Ignore]
    #[ORM\ManyToOne(inversedBy: 'monitoringReports')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Space $space = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $usedSpace = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive24HNumber = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $filesNumber = null;

    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?string $sharesNumber = null;

    #[ORM\Column]
    private ?int $returnStatusCode = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $returnErrorDetails = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $nextRun = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive1MonthNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive6MonthNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive5MinutesNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive1HourNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive7DaysNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActive3MonthNumber = null;

    #[ORM\Column(nullable: true)]
    private ?int $usersLastActiveLastYearNumber = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // only used for tests
    public function setId(?int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getSpace(): ?Space
    {
        return $this->space;
    }

    public function setSpace(?Space $space): static
    {
        $this->space = $space;

        return $this;
    }

    public function getUsedSpace(): ?string
    {
        return $this->usedSpace;
    }

    public function setUsedSpace(?string $usedSpace): static
    {
        $this->usedSpace = $usedSpace;

        return $this;
    }

    public function getUsersNumber(): ?int
    {
        return $this->usersNumber;
    }

    public function setUsersNumber(?int $usersNumber): static
    {
        $this->usersNumber = $usersNumber;

        return $this;
    }

    public function getUsersLastActive24HNumber(): ?int
    {
        return $this->usersLastActive24HNumber;
    }

    public function setUsersLastActive24HNumber(?int $usersLastActive24HNumber): static
    {
        $this->usersLastActive24HNumber = $usersLastActive24HNumber;

        return $this;
    }

    public function getFilesNumber(): ?string
    {
        return $this->filesNumber;
    }

    public function setFilesNumber(?string $filesNumber): static
    {
        $this->filesNumber = $filesNumber;

        return $this;
    }

    public function getSharesNumber(): ?string
    {
        return $this->sharesNumber;
    }

    public function setSharesNumber(?string $sharesNumber): static
    {
        $this->sharesNumber = $sharesNumber;

        return $this;
    }

    public function getReturnStatusCode(): ?int
    {
        return $this->returnStatusCode;
    }

    public function setReturnStatusCode(int $returnStatusCode): static
    {
        $this->returnStatusCode = $returnStatusCode;

        return $this;
    }

    public function getReturnErrorDetails(): ?string
    {
        return $this->returnErrorDetails;
    }

    public function setReturnErrorDetails(?string $returnErrorDetails): static
    {
        $this->returnErrorDetails = $returnErrorDetails;

        return $this;
    }

    public function getNextRun(): ?\DateTimeInterface
    {
        return $this->nextRun;
    }

    public function setNextRun(\DateTimeInterface $nextRun): static
    {
        $this->nextRun = $nextRun;

        return $this;
    }

    public function isReturnStatusCodeOK(): bool
    {
        return Response::HTTP_OK === $this->returnStatusCode;
    }

    public function getUsersLastActive1MonthNumber(): ?int
    {
        return $this->usersLastActive1MonthNumber;
    }

    public function setUsersLastActive1MonthNumber(?int $usersLastActive1MonthNumber): static
    {
        $this->usersLastActive1MonthNumber = $usersLastActive1MonthNumber;

        return $this;
    }

    public function getUsersLastActive6MonthNumber(): ?int
    {
        return $this->usersLastActive6MonthNumber;
    }

    public function setUsersLastActive6MonthNumber(?int $usersLastActive6MonthNumber): static
    {
        $this->usersLastActive6MonthNumber = $usersLastActive6MonthNumber;

        return $this;
    }

    public function getUsersLastActive5MinutesNumber(): ?int
    {
        return $this->usersLastActive5MinutesNumber;
    }

    public function setUsersLastActive5MinutesNumber(?int $usersLastActive5MinutesNumber): static
    {
        $this->usersLastActive5MinutesNumber = $usersLastActive5MinutesNumber;

        return $this;
    }

    public function getUsersLastActive1HourNumber(): ?int
    {
        return $this->usersLastActive1HourNumber;
    }

    public function setUsersLastActive1HourNumber(?int $usersLastActive1HourNumber): static
    {
        $this->usersLastActive1HourNumber = $usersLastActive1HourNumber;

        return $this;
    }

    public function getUsersLastActive7DaysNumber(): ?int
    {
        return $this->usersLastActive7DaysNumber;
    }

    public function setUsersLastActive7DaysNumber(?int $usersLastActive7DaysNumber): static
    {
        $this->usersLastActive7DaysNumber = $usersLastActive7DaysNumber;

        return $this;
    }

    public function getUsersLastActive3MonthNumber(): ?int
    {
        return $this->usersLastActive3MonthNumber;
    }

    public function setUsersLastActive3MonthNumber(?int $usersLastActive3MonthNumber): static
    {
        $this->usersLastActive3MonthNumber = $usersLastActive3MonthNumber;

        return $this;
    }

    public function getUsersLastActiveLastYearNumber(): ?int
    {
        return $this->usersLastActiveLastYearNumber;
    }

    public function setUsersLastActiveLastYearNumber(?int $usersLastActiveLastYearNumber): static
    {
        $this->usersLastActiveLastYearNumber = $usersLastActiveLastYearNumber;

        return $this;
    }
}
