<?php

namespace App\Entity;

class NormalizedOrganization
{
    private string $name;
    private string $source;
    private ?string $object;
    private ?string $siren;
    private ?string $siret;
    private ?string $rna;
    private ?string $creationYear;
    private int $nbEmployees;
    private ?string $categorieEntreprise;
    private ?bool $isESS;
    private ?string $categorieJuridique;
    private ?string $trancheEffectifs;
    private ?string $nature;
    private ?bool $isUnion;
    private ?bool $isFederation;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): NormalizedOrganization
    {
        $this->name = $name;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): NormalizedOrganization
    {
        $this->siren = $siren;

        return $this;
    }

    public function getCreationYear(): ?string
    {
        return $this->creationYear;
    }

    public function setCreationYear(?string $creationYear): NormalizedOrganization
    {
        $this->creationYear = $creationYear;

        return $this;
    }

    public function getNbEmployees(): int
    {
        return $this->nbEmployees;
    }

    public function setNbEmployees(int $nbEmployees): NormalizedOrganization
    {
        $this->nbEmployees = $nbEmployees;

        return $this;
    }

    public function getCategorieEntreprise(): ?string
    {
        return $this->categorieEntreprise;
    }

    public function setCategorieEntreprise(?string $categorieEntreprise): NormalizedOrganization
    {
        $this->categorieEntreprise = $categorieEntreprise;

        return $this;
    }

    public function getIsESS(): ?bool
    {
        return $this->isESS;
    }

    public function setIsESS(?bool $isESS): NormalizedOrganization
    {
        $this->isESS = $isESS;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): NormalizedOrganization
    {
        $this->siret = $siret;

        return $this;
    }

    public function getCategorieJuridique(): ?string
    {
        return $this->categorieJuridique;
    }

    public function setCategorieJuridique(?string $categorieJuridique): NormalizedOrganization
    {
        $this->categorieJuridique = $categorieJuridique;

        return $this;
    }

    public function getTrancheEffectifs(): ?string
    {
        return $this->trancheEffectifs;
    }

    public function setTrancheEffectifs(?string $trancheEffectifs): NormalizedOrganization
    {
        $this->trancheEffectifs = $trancheEffectifs;

        return $this;
    }

    public function setRna(?string $rna): NormalizedOrganization
    {
        $this->rna = $rna;

        return $this;
    }

    public function getRna(): ?string
    {
        return $this->rna;
    }

    public function setObject(?string $object): NormalizedOrganization
    {
        $this->object = $object;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setNature(?string $nature): NormalizedOrganization
    {
        $this->nature = $nature;

        return $this;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setIsUnion(?bool $isUnion): NormalizedOrganization
    {
        $this->isUnion = $isUnion;

        return $this;
    }

    public function getIsUnion(): ?bool
    {
        return $this->isUnion;
    }

    public function setIsFederation(?bool $isFederation): NormalizedOrganization
    {
        $this->isFederation = $isFederation;

        return $this;
    }

    public function getIsFederation(): ?bool
    {
        return $this->isFederation;
    }

    public function setSource(string $source): NormalizedOrganization
    {
        $this->source = $source;

        return $this;
    }

    public function getSource(): string
    {
        return $this->source;
    }
}
