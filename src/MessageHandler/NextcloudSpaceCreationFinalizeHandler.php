<?php

namespace App\MessageHandler;

use App\Entity\Job;
use App\Entity\Nextcloud\User;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\HTTP\Nextcloud\ProvisioningClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudSpaceCreationFinalizeAction;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class NextcloudSpaceCreationFinalizeHandler
{
    private SpaceRepository $spaceRepository;
    private ProvisioningClient $provisioningClient;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;
    private EntityManagerInterface $entityManager;

    public function __construct(SpaceRepository $spaceRepository, ProvisioningClient $provisioningClient, LoggerInterface $logger, MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->spaceRepository = $spaceRepository;
        $this->provisioningClient = $provisioningClient;
        $this->logger = $logger;
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    public function __invoke(NextcloudSpaceCreationFinalizeAction $nextcloudSpaceCreationFinalizeAction): void
    {
        $space = $this->spaceRepository->find($nextcloudSpaceCreationFinalizeAction->getSpaceId());

        $nextcloudUser = (new User())
        ->setEmail($space->getContactEmail())
        ->setDisplayName(mb_substr($space->getContactName(), 0, 60)) // Nextcloud limits display name to 64 characters
        ->setUserId($space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME)
        ->setGroupIds(['admin'])
        ->setLanguage($space->getLocale() ?? 'fr');

        try {
            /* Create the admin user on Nextcloud. This will send an email. */
            $this->provisioningClient->addUser($space, $nextcloudUser);

            /** Create a job to get an application password to then perform some tasks on behalf of the admin */
            $job = (new Job())
                ->setType(Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION)
                ->setSpace($space)
                ->setData([...$space->getDetails(), 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'organisation_name' => $space->getOrganizationName()])
            ;

            $this->entityManager->persist($job);
            $this->entityManager->flush();

            $this->logger->info('Finalized space installation '.$space->getDomain());
            $this->bus->dispatch(new AdminLoggingAction('Installed space '.$space->getDomain(), 'The space '.$space->getDomain().' has been successfully installed', ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        } catch (NextcloudRemoteOCSException $e) {
            $title = 'An error occurred while creating user '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'statuscode' => $e->getOcsStatusCode(),
                'message' => $e->getOcsMessage(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned code: '.$e->getOcsStatusCode()."\nReturned message: ".$e->getOcsMessage()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteXMLException $e) {
            $title = 'An error occurred while decoding returned XML from user creation for '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned content: '.$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteHTTPException $e) {
            $title = 'An HTTP error occurred while creating user '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'code' => $e->getHttpStatusCode(),
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'HTTP Code: '.$e->getHttpStatusCode()."\nReturned content: ".$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }
}
