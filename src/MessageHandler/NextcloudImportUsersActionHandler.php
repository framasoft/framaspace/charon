<?php

namespace App\MessageHandler;

use App\Entity\Nextcloud\Group;
use App\Entity\Nextcloud\User;
use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\HTTP\Nextcloud\ProvisioningClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudImportUsersAction;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
readonly class NextcloudImportUsersActionHandler
{
    public function __construct(private SpaceRepository $spaceRepository, private ProvisioningClient $provisioningClient, private MessageBusInterface $bus)
    {
    }

    public function __invoke(NextcloudImportUsersAction $nextcloudImportUsersAction): void
    {
        $space = $this->spaceRepository->find($nextcloudImportUsersAction->getSpaceId());
        if (!$space) {
            $this->bus->dispatch(new AdminLoggingAction('An error occurred when importing new users to space '.$nextcloudImportUsersAction->getSpaceId(), 'Space ID '.$nextcloudImportUsersAction->getSpaceId()." doesn't exist.", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));

            return;
        }
        $userImport = $nextcloudImportUsersAction->getUserImport();
        foreach ($userImport->getGroups() as $group) {
            $this->createGroup($space, $group);
        }
        foreach ($userImport->getUsers() as $user) {
            $this->createUser($space, $user);
        }
    }

    private function createGroup(Space $space, Group $group): void
    {
        try {
            $this->provisioningClient->addGroup($space, $group);
        } catch (NextcloudRemoteHTTPException|NextcloudRemoteOCSException|NextcloudRemoteXMLException $e) {
            // Ignore if groupname already exists
            if ($e instanceof NextcloudRemoteOCSException && 102 === (int) $e->getOcsStatusCode()) {
                return;
            }
            $this->bus->dispatch(new AdminLoggingAction('An error occurred when creating new group '.$group->getGroupId().' on space '.$space->getDomain(), 'Type: '.$e->getMessage()."\n\nResponse:\n".$e->getResponse(), ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }

    private function createUser(Space $space, User $user): void
    {
        try {
            $this->provisioningClient->addUser($space, $user);
        } catch (NextcloudRemoteHTTPException|NextcloudRemoteOCSException|NextcloudRemoteXMLException $e) {
            // Ignore if username already exists
            if ($e instanceof NextcloudRemoteOCSException && 102 === (int) $e->getOcsStatusCode()) {
                return;
            }
            $this->bus->dispatch(new AdminLoggingAction('An error occurred when creating new user '.$user->getUserId().' on space '.$space->getDomain(), 'Type: '.$e->getMessage()."\n\nResponse:\n".$e->getResponse(), ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }
}
