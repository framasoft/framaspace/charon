<?php

namespace App\MessageHandler;

use App\Entity\Job;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\HTTP\Nextcloud\AppPasswordClient;
use App\HTTP\Nextcloud\NotificationClient;
use App\HTTP\Nextcloud\SharingClient;
use App\HTTP\Nextcloud\WebDAVClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallAdminUserConfigurationAction;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;

#[AsMessageHandler]
class NextcloudPostInstallAdminUserConfigurationHandler
{
    private SpaceRepository $spaceRepository;
    private WebDAVClient $webDAVClient;
    private SharingClient $sharingClient;
    private AppPasswordClient $appPasswordClient;
    private NotificationClient $notificationClient;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;
    private EntityManagerInterface $entityManager;
    private string $commonFolderName;
    private string $commonFolderReadmePath;
    private string $everyoneGroupName;

    public function __construct(SpaceRepository $spaceRepository, WebDAVClient $webDAVClient, SharingClient $sharingClient, NotificationClient $notificationClient, AppPasswordClient $appPasswordClient, LoggerInterface $logger, MessageBusInterface $bus, EntityManagerInterface $entityManager, #[Autowire('%app.nextcloud.common_folder_name%')] string $commonFolderName, #[Autowire('%app.nextcloud.everyone_group_name%')] string $everyoneGroupName, #[Autowire('%app.nextcloud.common_folder_readme_path%')] string $commonFolderReadmePath)
    {
        $this->spaceRepository = $spaceRepository;
        $this->webDAVClient = $webDAVClient;
        $this->sharingClient = $sharingClient;
        $this->appPasswordClient = $appPasswordClient;
        $this->notificationClient = $notificationClient;
        $this->logger = $logger;
        $this->bus = $bus;
        $this->entityManager = $entityManager;
        $this->commonFolderName = $commonFolderName;
        $this->commonFolderReadmePath = $commonFolderReadmePath;
        $this->everyoneGroupName = $everyoneGroupName;
    }

    public function __invoke(NextcloudPostInstallAdminUserConfigurationAction $nextcloudPostInstallAdminUserConfigurationAction): void
    {
        $space = $this->spaceRepository->find($nextcloudPostInstallAdminUserConfigurationAction->getSpaceId());
        $appPassword = $nextcloudPostInstallAdminUserConfigurationAction->getAppPassword();
        $adminUser = $space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME;
        $basicAuth = [$adminUser, $appPassword];

        try {
            $fs = new Filesystem();
            $rootDir = getcwd();
            $path = $rootDir.'/'.$this->commonFolderReadmePath;

            if (!$fs->exists($path)) {
                $title = 'Unable to get shared folder Readme.md contents';
                $this->logger->error($title, [
                    'path' => $path,
                ]);
                $this->bus->dispatch(new AdminLoggingAction($title, 'Path: '.$path."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));

                return;
            }
            $readmeContent = file_get_contents($this->commonFolderReadmePath);

            try {
                $this->webDAVClient->createFolder($space, $appPassword, $this->commonFolderName);
            } catch (HttpExceptionInterface $exception) {
                $this->logFailure($exception, 'An error occurred while creating common shared folder for space '.$space->getDomain());

                if (405 !== $exception->getCode()) {
                    throw $exception;
                }
                // If 405: we already created
            }
            try {
                $this->webDAVClient->createFile($space, $appPassword, $this->commonFolderName.'/README.md', $readmeContent);
            } catch (HttpExceptionInterface $exception) {
                $this->logFailure($exception, 'An error occurred while creating README.md file in common folder for space '.$space->getDomain());

                if (405 !== $exception->getCode()) {
                    throw $exception;
                }
                // If 405: we already created
            }
            $this->sharingClient->share($space, $basicAuth, $this->commonFolderName, $this->everyoneGroupName, 1, 15);
            // TODO: is it possible to handle share conflict?
            $this->notificationClient->deleteAll($space, $basicAuth);
            $this->appPasswordClient->deleteAppPassword($space, $basicAuth);

            /** Create a job to finalize things (reset terms_of_service acceptation and clear activities) */
            $job = (new Job())
                ->setType(Job::JOB_TYPE_POST_INSTALL_LAST_STEP)
                ->setSpace($space)
                ->setData([...$space->getDetails(), 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'organisation_name' => $space->getOrganizationName()])
            ;

            $this->entityManager->persist($job);
            $this->entityManager->flush();
        } catch (NextcloudRemoteOCSException $e) {
            $title = 'An error occurred while setting up properly user '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'statuscode' => $e->getOcsStatusCode(),
                'message' => $e->getOcsMessage(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned code: '.$e->getOcsStatusCode()."\nReturned message: ".$e->getOcsMessage()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteXMLException $e) {
            $title = 'An error occurred while decoding returned XML from setting up user '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned content: '.$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteHTTPException $e) {
            $title = 'An HTTP error occurred while setting up properly user '.$e->getNextcloudUserId().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'code' => $e->getHttpStatusCode(),
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'HTTP Code: '.$e->getHttpStatusCode()."\nReturned content: ".$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }

    private function logFailure(HttpExceptionInterface $exception, string $title): void
    {
        $this->logger->error($title, [
            'statuscode' => $exception->getCode(),
            'message' => $exception->getMessage(),
        ]);
        $this->bus->dispatch(new AdminLoggingAction($title, 'Returned code: '.$exception->getCode()."\nReturned message: ".$exception->getMessage()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
    }
}
