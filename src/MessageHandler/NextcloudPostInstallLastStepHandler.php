<?php

namespace App\MessageHandler;

use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallLastStepAction;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use App\Services\RefreshSpaceMonitoring;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class NextcloudPostInstallLastStepHandler
{
    public function __construct(private SpaceRepository $spaceRepository, private LoggerInterface $logger, private Mailer $mailer, private MessageBusInterface $bus, private readonly RefreshSpaceMonitoring $refreshSpaceMonitoring)
    {
    }

    public function __invoke(NextcloudPostInstallLastStepAction $nextcloudPostInstallLastStepAction): void
    {
        $space = $this->spaceRepository->find($nextcloudPostInstallLastStepAction->getSpaceId());

        $this->mailer->sendCreationSuccessEmail($space);
        $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
        $this->logger->info('Finalized space creation '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Created space '.$space->getDomain(), 'The space '.$space->getDomain().' has been successfully created', ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }
}
