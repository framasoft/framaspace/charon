<?php

namespace App\MessageHandler;

use App\Message\AdminLoggingAction;
use App\Services\Notifications\AdminNotifierInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class AdminLoggingHandler
{
    /** @var iterable<AdminNotifierInterface> */
    private iterable $handlers;

    public function __construct(iterable $handlers)
    {
        $this->handlers = $handlers;
    }

    public function __invoke(AdminLoggingAction $adminLoggingAction): void
    {
        foreach ($this->handlers as $handler) {
            $handler->notify($adminLoggingAction->getTitle(), $adminLoggingAction->getMessage(), $adminLoggingAction->getOptions());
        }
    }
}
