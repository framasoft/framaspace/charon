<?php

namespace App\MessageHandler;

use App\Entity\Space;
use App\Message\NextcloudAnnouncementAction;
use App\Message\NextcloudBatchAnnouncementAction;
use App\Repository\AnnouncementRepository;
use App\Repository\SpaceRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class NextcloudBatchAnnouncementHandler
{
    private SpaceRepository $spaceRepository;
    private AnnouncementRepository $announcementRepository;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;

    public function __construct(SpaceRepository $spaceRepository, AnnouncementRepository $announcementRepository, LoggerInterface $logger, MessageBusInterface $bus)
    {
        $this->spaceRepository = $spaceRepository;
        $this->announcementRepository = $announcementRepository;
        $this->logger = $logger;
        $this->bus = $bus;
    }

    public function __invoke(NextcloudBatchAnnouncementAction $nextcloudBatchAnnouncementAction): void
    {
        $announcementId = $nextcloudBatchAnnouncementAction->getAnnouncementId();
        $announcement = $this->announcementRepository->find($announcementId);

        $this->logger->debug('Sending announcement '.$announcementId.' to all active spaces');

        if (!$announcement) {
            $this->logger->debug('Announcement '.$announcementId.' was not found, aborting.');

            return;
        }

        $spaces = $this->spaceRepository->findBy([
            'status' => Space::SPACE_STATUS_CREATED,
            'enabled' => true,
            'deletedAt' => null,
        ]);

        foreach ($spaces as $space) {
            $this->bus->dispatch(new NextcloudAnnouncementAction($space->getId(), $announcement->getId()));
        }
    }
}
