<?php

namespace App\MessageHandler;

use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\HTTP\Nextcloud\ProvisioningClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudNewUserAction;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class NextcloudNewUserActionHandler
{
    private SpaceRepository $spaceRepository;
    private ProvisioningClient $provisioningClient;
    private MessageBusInterface $bus;

    public function __construct(SpaceRepository $spaceRepository, ProvisioningClient $provisioningClient, MessageBusInterface $bus)
    {
        $this->spaceRepository = $spaceRepository;
        $this->provisioningClient = $provisioningClient;
        $this->bus = $bus;
    }

    public function __invoke(NextcloudNewUserAction $nextcloudNewUserAction): void
    {
        $space = $this->spaceRepository->find($nextcloudNewUserAction->getSpaceId());
        try {
            $this->provisioningClient->addUser($space, $nextcloudNewUserAction->getNextcloudUser());
        } catch (NextcloudRemoteHTTPException|NextcloudRemoteOCSException|NextcloudRemoteXMLException $e) {
            // Ignore if username already exists
            if ($e instanceof NextcloudRemoteOCSException && 102 === (int) $e->getOcsStatusCode()) {
                return;
            }
            $this->bus->dispatch(new AdminLoggingAction('An error occurred when creating new user '.$nextcloudNewUserAction->getNextcloudUser()->getUserId().' on space '.$space->getDomain(), 'Type: '.$e->getMessage()."\n\nResponse:\n".$e->getResponse(), ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }
}
