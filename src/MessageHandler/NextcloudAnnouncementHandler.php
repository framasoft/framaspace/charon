<?php

namespace App\MessageHandler;

use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\HTTP\Nextcloud\AnnouncementClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudAnnouncementAction;
use App\Repository\AnnouncementRepository;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class NextcloudAnnouncementHandler
{
    private SpaceRepository $spaceRepository;
    private AnnouncementRepository $announcementRepository;
    private AnnouncementClient $announcementClient;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;

    public function __construct(SpaceRepository $spaceRepository, AnnouncementRepository $announcementRepository, AnnouncementClient $announcementClient, LoggerInterface $logger, MessageBusInterface $bus)
    {
        $this->spaceRepository = $spaceRepository;
        $this->announcementRepository = $announcementRepository;
        $this->announcementClient = $announcementClient;
        $this->logger = $logger;
        $this->bus = $bus;
    }

    public function __invoke(NextcloudAnnouncementAction $nextcloudAnnouncementAction): void
    {
        $spaceId = $nextcloudAnnouncementAction->getSpaceId();
        $announcementId = $nextcloudAnnouncementAction->getAnnouncementId();

        $space = $this->spaceRepository->find($spaceId);
        $announcement = $this->announcementRepository->find($announcementId);

        $this->logger->debug('Sending announcement '.$announcementId.' to space '.$spaceId);

        if (!$announcement) {
            $this->logger->debug('Announcement '.$announcementId.' was not found, skipping.');

            return;
        }

        if (!$space) {
            $this->logger->debug('Space '.$spaceId.' was not found, skipping.');

            return;
        }

        if (!$space->isEnabled() || Space::SPACE_STATUS_CREATED !== $space->getStatus() || $space->getDeletedAt()) {
            $this->logger->debug('Space '.$spaceId." isn't ready to receive announcements, skipping.");

            return;
        }

        try {
            $this->announcementClient->addAnnouncement($space, $announcement);

            $this->logger->info('Sent announcement '.$announcement->getSubject().' to space '.$space->getDomain());
            $this->bus->dispatch(new AdminLoggingAction('Sent announcement '.$announcement->getSubject().' to space '.$space->getDomain(), 'Sent announcement '.$announcement->getSubject().' to space '.$space->getDomain(), ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        } catch (NextcloudRemoteOCSException $e) {
            $title = 'An error occurred while performing OCS request on creating announcement '.$announcement->getSubject().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'statuscode' => $e->getOcsStatusCode(),
                'message' => $e->getOcsMessage(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned code: '.$e->getOcsStatusCode()."\nReturned message: ".$e->getOcsMessage()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteXMLException $e) {
            $title = 'An error occurred while decoding returned XML from OCS request while creating announcement '.$announcement->getSubject().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'Returned content: '.$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        } catch (NextcloudRemoteHTTPException $e) {
            $title = 'An HTTP error occurred while creating announcement '.$announcement->getSubject().' on '.$e->getSpaceDomain();
            $this->logger->error($title, [
                'code' => $e->getHttpStatusCode(),
                'content' => $e->getResponse(),
            ]);
            $this->bus->dispatch(new AdminLoggingAction($title, 'HTTP Code: '.$e->getHttpStatusCode()."\nReturned content: ".$e->getResponse()."\n", ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
        }
    }
}
