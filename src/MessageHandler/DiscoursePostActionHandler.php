<?php

namespace App\MessageHandler;

use App\Message\DiscoursePostAction;
use App\Repository\SubmissionRepository;
use App\Services\DiscourseService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class DiscoursePostActionHandler
{
    private SubmissionRepository $submissionRepository;
    private DiscourseService $discourseService;
    private LoggerInterface $logger;

    public function __construct(SubmissionRepository $submissionRepository, DiscourseService $discourseService, LoggerInterface $logger)
    {
        $this->submissionRepository = $submissionRepository;
        $this->discourseService = $discourseService;
        $this->logger = $logger;
    }

    public function __invoke(DiscoursePostAction $discoursePostAction): void
    {
        $submissionId = $discoursePostAction->getSubmissionId();
        $submission = $this->submissionRepository->find($submissionId);
        if (!$submission) {
            $this->logger->warning("When trying to post submission $submissionId, this submission was not found");

            return;
        }
        $this->discourseService->postNewSubmission($submission);
    }
}
