<?php

namespace App\Controller;

use App\Entity\Invite;
use App\Entity\Submission;
use App\Events\SubmissionCreatedEvent;
use App\Form\Type\SubmissionType;
use Doctrine\Persistence\ManagerRegistry;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{
    #[Route('/')]
    public function index(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('manage_space');
        }

        return $this->render('index.html.twig', [
        ]);
    }

    #[Route('/new')]
    public function newSubmission(Request $request, ManagerRegistry $doctrine, EventDispatcherInterface $dispatcher, ContainerBagInterface $params): Response
    {
        $submission = new Submission();
        $submission->setCreatedAt(new \DateTimeImmutable());
        $submission->setUpdatedAt(new \DateTimeImmutable());
        $submission->setLocale($request->getLocale());

        $inviteToken = $request->query->get('inviteToken');
        /** @var Invite|null $invite */
        $invite = $doctrine->getRepository(Invite::class)->findOneBy(['token' => $inviteToken]);

        if ($invite && $invite->getEmail()) {
            $submission->setEmail($invite->getEmail());
        }

        $form = $this->createForm(SubmissionType::class, $submission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var list<string> $formData */
            $formData = $form->get('topics')->getData();
            $submission->setTopics(array_filter([...$formData, $form->get('extraTopics')->getData()]));
            if (Submission::SUBMISSION_TYPE_OTHER === $form->get('type')->getData()) {
                $submission->setType($form->get('extraType')->getData() ?? '');
            }

            $submission->setDomain(mb_strtolower($form->get('domain')->getData()));

            $entityManager = $doctrine->getManager();

            if ($invite && null === $invite->getUsedAt()) {
                $invite->setUsedAt(new \DateTimeImmutable());
                $submission->setInvite($invite);
                $entityManager->persist($invite);
            }

            $entityManager->persist($submission);
            $entityManager->flush();
            $dispatcher->dispatch(new SubmissionCreatedEvent($submission));

            return $this->redirectToRoute('app_index_after_new_submission');
        }

        /** @var bool $submissionsOpened */
        $submissionsOpened = $params->get('app.submissions.opened');
        if ($submissionsOpened || $invite) {
            return $this->render('space/new.html.twig', ['form' => $form, 'invite' => $invite]);
        }

        return $this->render('space/submissions_closed.html.twig');
    }

    #[Route('/new/done', name: 'app_index_after_new_submission')]
    public function afterNewSubmission(): Response
    {
        return $this->render('space/submission/done.html.twig');
    }
}
