<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Nextcloud\Group;
use App\Entity\Nextcloud\User;
use App\Entity\Nextcloud\UserImport;
use App\Entity\Space;
use App\Exceptions\NoAvailableOfficeServer;
use App\Form\Type\DeleteSpaceType;
use App\Form\Type\ReactivateSpaceType;
use App\Form\Type\SpaceType;
use App\Form\Type\UserProvisioningImportType;
use App\HTTP\Nextcloud\ServerInfoClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudImportUsersAction;
use App\Services\ConfigService;
use App\Services\Notifications\AdminNotifierInterface;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\CharsetConverter;
use League\Csv\Reader;
use League\Csv\Statement;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SpaceController extends AbstractController
{
    public function __construct(private readonly SpaceManager $spaceManager, private readonly LoggerInterface $logger, private readonly MessageBusInterface $bus)
    {
    }

    #[Route('/space', name: 'view_space')]
    public function index(ServerInfoClient $serverInfoClient, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] string $quota): Response
    {
        /** @var Space $space */
        $space = $this->getUser();
        $jobs = array_filter($space->getJobs()->getValues(), fn (Job $job) => Job::JOB_STATUS_DONE !== $job->getStatus());
        $hasOngoingJobs = count($jobs) > 0;
        try {
            $statistics = $serverInfoClient->getStatistics($space);
        } catch (\Throwable) {
            $statistics = false;
        }

        return $this->render('space/view.html.twig', ['space' => $space, 'has_ongoing_jobs' => $hasOngoingJobs, 'deleted_at' => $space->getDeletedAt(), 'real_deletion_date' => $space->getDeletedAt()?->add(new \DateInterval('P30D')), 'statistics' => $statistics, 'storage_quota' => (int) $quota * 1_073_741_824]);
    }

    #[Route('/space/edit', name: 'manage_space')]
    public function edit(Request $request, EntityManagerInterface $entityManager, ConfigService $configService): Response
    {
        /** @var Space $space */
        $space = $this->getUser();
        $jobs = array_filter($space->getJobs()->getValues(), fn (Job $job) => Job::JOB_STATUS_DONE !== $job->getStatus());
        $hasOngoingJobs = count($jobs) > 0;
        $form = $this->createForm(SpaceType::class, $space, ['disabled' => $hasOngoingJobs || null !== $space->getDeletedAt() || $space->isMaintenanceActivated() || $configService->isDeploymentDisabled()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->persist($space);
                $entityManager->flush();

                if ($space->getOffice() !== $space->getFutureOfficeType()) {
                    // Creating a new job is handled by SpaceChangedNotifier
                    $this->addFlash('info', new TranslatableMessage('The change of office type has been scheduled. The changes should be applied in a few minutes. Your space might be briefly unavailable during the changes.'));
                }

                // Job not yet created, but let's make sure we lock the form
                return $this->redirectToRoute('view_space');
            } catch (NoAvailableOfficeServer $e) {
                $officeServerTypeName = SpaceManager::officeTypeToName($e->getOfficeType());
                $this->bus->dispatch(new AdminLoggingAction('Office server of type '.$officeServerTypeName.' not available', 'There was available office server of type '.$officeServerTypeName.' when space '.$space->getDomain().' tried to update their office type', ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
                $this->logger->error('There was available office server of type '.$officeServerTypeName.' when space '.$space->getDomain().' tried to update their office type');
                $this->addFlash('info', new TranslatableMessage('An error has occurred. There was no office server available to handle this office server change. This error has been reported.'));
            }
        }

        return $this->render('space/edit.html.twig', ['form' => $form, 'organization' => $space->getOrganizationName(), 'has_ongoing_jobs' => $hasOngoingJobs, 'deleted_at' => $space->getDeletedAt(), 'real_deletion_date' => $space->getDeletedAt()?->add(new \DateInterval('P30D')), 'deployment_disabled' => $configService->isDeploymentDisabled(), 'maintenance_activated' => $space->isMaintenanceActivated(), 'maintenance_message' => $space->getMaintenanceMessage()]);
    }

    #[Route('/space/pre-delete', name: 'pre-delete')]
    public function preDeleteConfirmation(Request $request): Response
    {
        /** @var Space $space */
        $space = $this->getUser();

        $form = $this->createForm(DeleteSpaceType::class, [], ['subdomain_compare' => $space->getDomain()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->spaceManager->preDisableSpace($space);
            $this->spaceManager->sendDisableSpaceJobs($space);

            $this->addFlash('info', new TranslatableMessage("The deletion of your space has been scheduled. The changes should be applied in a few minutes. We will send you an email when that's done."));

            return $this->redirectToRoute('view_space');
        }

        return $this->render('space/delete_confirmation.html.twig', [
            'form' => $form,
            'organization' => $space->getOrganizationName(),
            'subdomain' => $space->getDomain(),
            'deletion_date' => (new \DateTime())->add(new \DateInterval('P30D')),
        ]);
    }

    #[Route('/space/pre-reactivate', name: 'pre-reactivate')]
    public function preReactivate(Request $request): Response
    {
        /** @var Space $space */
        $space = $this->getUser();

        $form = $this->createForm(ReactivateSpaceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->spaceManager->preReenableSpace($space);
            $this->spaceManager->sendReenableSpaceJobs($space);

            $this->addFlash('info', new TranslatableMessage("The reactivation of your space has been scheduled. The changes should be applied in a few minutes. We will send you an email when that's done."));

            return $this->redirectToRoute('view_space');
        }

        return $this->render('space/reactivate.html.twig', [
            'form' => $form,
            'organization' => $space->getOrganizationName(),
            'subdomain' => $space->getDomain(),
            'deletion_date' => (new \DateTime())->add(new \DateInterval('P30D')),
        ]);
    }

    #[Route('/disabled-space/{domain}')]
    public function disabledSpace(Space $space, #[Autowire('%app.space.days_before_purge%')] string $daysBeforeSpacePurge): Response
    {
        $spaceDeletedAt = $space->getDeletedAt();
        if ($spaceDeletedAt && Space::SPACE_DISABLING_REASON_BY_INACTIVITY === $space->getDisabledReason()) {
            return $this->render('space/disabled.html.twig', ['space' => $space, 'complete_deletion_date' => $spaceDeletedAt->add(new \DateInterval('P'.$daysBeforeSpacePurge.'D'))]);
        }

        return $this->redirect('https://www.frama.space');
    }

    #[Route('/space/import-users', name: 'space-import-users')]
    public function import(Request $request, ValidatorInterface $validator, TranslatorInterface $translator): Response
    {
        /** @var Space $space */
        $space = $this->getUser();
        $form = $this->createForm(UserProvisioningImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Space $space */
            $space = $this->getUser();

            /** @var UploadedFile $importFile */
            $importFile = $form->get('file')->getData();

            if (!$importFile) {
                $form->addError(new FormError($translator->trans('File given is empty')));

                return $this->render('space/import.html.twig', ['form' => $form, 'organization' => $space->getOrganizationName()]);
            }

            $content = $importFile->getContent();
            mb_detect_order(['ASCII', 'UTF-8', 'ISO-8859-1']);
            $encoding = mb_detect_encoding($content, mb_detect_order(), true);

            $csv = Reader::createFromString($content);
            $csv->setDelimiter(';');
            // $csv->setHeaderOffset(1);
            $stmt = Statement::create()
                ->offset(1)
                ->limit(1000)
            ;

            $records = $stmt->process($csv);

            if (!in_array($encoding, ['ASCII', 'UTF-8'])) {
                try {
                    $encoder = (new CharsetConverter())->inputEncoding($encoding);
                    $records = $encoder->convert($records);
                } catch (\OutOfRangeException $e) {
                    $form->addError(new FormError($translator->trans('File encoding is incorrect. Make sure you provide an UTF-8 encoded file.')));

                    return $this->render('space/import.html.twig', ['form' => $form, 'organization' => $space->getOrganizationName()]);
                }
            }

            /** @var User[] $users */
            $users = [];
            /** @var string[] $groups */
            $groups = [];
            $validationErrors = [];

            foreach ($records as $record) {
                $groupString = $record[3];

                $userId = mb_substr(trim($record[1]), 0, 60);
                $userEmail = trim($record[0]);

                if (!$userId || !$userEmail) {
                    continue;
                }

                $userGroups = [];
                if ($groupString) {
                    $userGroups = array_map(function (string $group) {
                        return mb_substr(trim($group), 0, 60);
                    }, explode(',', $groupString));
                    array_push($groups, ...$userGroups);
                }

                $user = new User();
                $user->setEmail($userEmail)
                    ->setUserId($userId)
                    ->setDisplayName(mb_substr(trim($record[2]), 0, 60))
                    ->setGroupIds($userGroups);

                $newValidationErrors = $validator->validate($user);
                if (count($newValidationErrors) > 0) {
                    foreach ($newValidationErrors as $validationError) {
                        /* @var ConstraintViolationInterface $validationError */
                        $form->addError(new FormError($translator->trans('Validation error for the user "{userId}": {error}', ['userId' => $userId, 'error' => $validationError->getMessage()])));
                    }
                } else {
                    $users[] = $user;
                }
                array_push($validationErrors, ...$newValidationErrors);
            }

            $nbUsers = count($users);

            if (0 === $nbUsers && empty($validationErrors)) {
                $form->addError(new FormError($translator->trans('No valid users to import were found in the file. Check the structure.')));
            } elseif (0 === $nbUsers && count($validationErrors) > 0) {
                $form->addError(new FormError($translator->trans("Every listed user in the provided file failed validation and wasn't imported. Please check the above issues.")));
            } elseif ($nbUsers > 0 && count($validationErrors) > 0) {
                $form->addError(new FormError($translator->trans('Some users listed in the file have some validation issues. Please fix them before importing the file again.')));
            } elseif ($nbUsers <= 50) {
                $groups = array_map(fn ($group) => (new Group())->setGroupId($group), array_unique($groups));
                $this->logger->debug('Dispatching user import for space '.$space->getId());
                $this->bus->dispatch(new NextcloudImportUsersAction($space->getId(), new UserImport($users, $groups)));
                $this->addFlash('success', new TranslatableMessage('The import of {nbUsers} users has been scheduled.', ['nbUsers' => $nbUsers]));

                return $this->redirectToRoute('view_space');
            } else {
                $form->addError(new FormError($translator->trans('The import is limited to 50 users')));
            }
        }

        return $this->render('space/import.html.twig', ['form' => $form, 'organization' => $space->getOrganizationName()]);
    }
}
