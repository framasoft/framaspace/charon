<?php

namespace App\Controller\Admin;

use App\Entity\PahekoServer;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

class PahekoServerCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPaginatorPageSize(50)
            ->setPageTitle('index', new TranslatableMessage('Paheko servers'))
            ->setEntityLabelInSingular(new TranslatableMessage('Paheko server'))
            ->setEntityLabelInPlural(new TranslatableMessage('Paheko servers'))
        ;
    }

    public static function getEntityFqcn(): string
    {
        return PahekoServer::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('hostname')->setLabel(new TranslatableMessage('Hostname')),
            IntegerField::new('maxServers')->setLabel(new TranslatableMessage('Maximum servers')),
            BooleanField::new('isFull')->setLabel(new TranslatableMessage('Full')),
        ];
    }
}
