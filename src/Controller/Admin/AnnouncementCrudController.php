<?php

namespace App\Controller\Admin;

use App\Entity\Announcement;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

class AnnouncementCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPaginatorPageSize(50)
        ;
    }

    public static function getEntityFqcn(): string
    {
        return Announcement::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('subject')->setLabel(new TranslatableMessage('Subject')),
            CodeEditorField::new('message')->setLabel(new TranslatableMessage('Message'))->setLanguage('markdown'),
            BooleanField::new('onlyAdmins')->setLabel(new TranslatableMessage('Only admins')),
            BooleanField::new('createActivities')->setLabel(new TranslatableMessage('Create activities')),
            BooleanField::new('createNotifications')->setLabel(new TranslatableMessage('Create notifications')),
            BooleanField::new('sendEmails')->setLabel(new TranslatableMessage('Send emails')),
            BooleanField::new('allowComments')->setLabel(new TranslatableMessage('Allow comments')),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at'))->hideOnForm(),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->hideOnForm(),
        ];
    }
}
