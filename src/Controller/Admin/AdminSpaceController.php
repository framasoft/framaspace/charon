<?php

namespace App\Controller\Admin;

use App\Entity\MonitoringReport;
use App\Entity\Space;
use App\HTTP\Nextcloud\ServerInfoClient;
use App\Services\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class AdminSpaceController extends AbstractController
{
    public function __construct(private readonly ChartBuilderInterface $chartBuilder, private readonly TranslatorInterface $translator)
    {
    }

    public const FRAMA_COLORS = [
        '#977ac2',
        '#487982',
        '#338650',
        '#dd6418',
        '#e65740',
        '#677490',
    ];

    #[Route('/custom-admin/space/{id}', name: 'admin_space_show')]
    public function showSpaceDetails(Space $space, ServerInfoClient $serverInfoClient, AdminUrlGenerator $adminUrlGenerator, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] string $quota): Response
    {
        try {
            $statistics = $serverInfoClient->getStatistics($space);
        } catch (\Throwable) {
            $statistics = false;
        }

        $easyAdminSpaceDetailUrl = $adminUrlGenerator->backendAdminSpaceURL($space);

        return $this->render('admin/space/view.html.twig', [
            'space' => $space, 'deleted_at' => $space->getDeletedAt(), 'statistics' => $statistics, 'storage_quota' => (int) $quota * 1_073_741_824,
            'easy_admin_space_detail_url' => $easyAdminSpaceDetailUrl,
            'errored_monitoring_reports' => $this->erroredMonitoringReports($space->getMonitoringReports()->toArray()),
            'used_space_graph' => $this->generateUsedSpacePerDay($space->getMonitoringReports()),
            'number_of_users_graph' => $this->generateNumberOfUsersPerDay($space->getMonitoringReports()),
            'number_of_users_5m_graph' => $this->generateNumberOfUsers5MinutesPerDay($space->getMonitoringReports()),
            'number_of_users_1h_graph' => $this->generateNumberOfUsersHourPerDay($space->getMonitoringReports()),
            'number_of_users_24h_graph' => $this->generateNumberOfUsers24HPerDay($space->getMonitoringReports()),
            'number_of_users_7d_graph' => $this->generateNumberOfUsers7DaysPerDay($space->getMonitoringReports()),
            'number_of_users_1M_graph' => $this->generateNumberOfUsers1MonthPerDay($space->getMonitoringReports()),
            'number_of_users_3M_graph' => $this->generateNumberOfUsers3MonthPerDay($space->getMonitoringReports()),
            'number_of_users_6M_graph' => $this->generateNumberOfUsers6MonthPerDay($space->getMonitoringReports()),
            'number_of_users_1Y_graph' => $this->generateNumberOfUsersYearPerDay($space->getMonitoringReports()),
            'number_of_files_graph' => $this->generateNumberOfFilesPerDay($space->getMonitoringReports()),
            'number_of_shares_graph' => $this->generateNumberOfSharesPerDay($space->getMonitoringReports()),
        ]);
    }

    private function generateUsedSpacePerDay(iterable $monitoringReports): Chart
    {
        $data = [];
        foreach ($monitoringReports as $monitoringReport) {
            /** @var MonitoringReport $monitoringReport */
            $updatedAt = $monitoringReport->getUpdatedAt();
            $data[$updatedAt->format('d/m/Y')] = (int) ($monitoringReport->getUsedSpace() ?? 0) / 1_073_741_824;
        }
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $dateFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Europe/Paris');

        uksort($data, [$this, 'compareDates']);

        $chart->setData([
            'labels' => array_map(fn ($elem) => $dateFormatter->format(\DateTimeImmutable::createFromFormat('d/m/Y', $elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Used space'),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'scales' => [
                'y' => [
                    'title' => [
                        'display' => true,
                        'text' => $this->translator->trans('Used space (in GB)'),
                    ],
                    'max' => 50,
                    'ticks' => [
                        'stepSize' => 1,
                    ],
                ],
            ],
        ]);

        return $chart;
    }

    private function generateNumberOfUsersPerDay(iterable $monitoringReports): Chart
    {
        $data = [];
        foreach ($monitoringReports as $monitoringReport) {
            /** @var MonitoringReport $monitoringReport */
            $updatedAt = $monitoringReport->getUpdatedAt();
            $data[$updatedAt->format('d/m/Y')] = $monitoringReport->getUsersNumber();
        }
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $dateFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Europe/Paris');

        uksort($data, [$this, 'compareDates']);

        $chart->setData([
            'labels' => array_map(fn ($elem) => $dateFormatter->format(\DateTimeImmutable::createFromFormat('d/m/Y', $elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Number of users'),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'scales' => [
                'y' => [
                    'min' => 0,
                    'max' => 50,
                    'ticks' => [
                        'stepSize' => 5,
                    ],
                ],
            ],
        ]);

        return $chart;
    }

    private function generateNumberOfUsers5MinutesPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive5MinutesNumber', $this->translator->trans('Number of users in the last 5 minutes'));
    }

    private function generateNumberOfUsersHourPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive1HourNumber', $this->translator->trans('Number of users in the last hour'));
    }

    private function generateNumberOfUsers24HPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive24HNumber', $this->translator->trans('Number of users in the last 24 hours'));
    }

    private function generateNumberOfUsers7DaysPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive7DaysNumber', $this->translator->trans('Number of users in the last 7 days'));
    }

    private function generateNumberOfUsers1MonthPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive1MonthNumber', $this->translator->trans('Number of users in the last month'));
    }

    private function generateNumberOfUsers3MonthPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive3MonthNumber', $this->translator->trans('Number of users in the last 3 months'));
    }

    private function generateNumberOfUsers6MonthPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActive6MonthNumber', $this->translator->trans('Number of users in the last 6 months'));
    }

    private function generateNumberOfUsersYearPerDay(iterable $monitoringReports): Chart
    {
        return $this->generateNumberOfUsersXXPerDay($monitoringReports, 'getUsersLastActiveLastYearNumber', $this->translator->trans('Number of users in the last year'));
    }

    private function generateNumberOfUsersXXPerDay(iterable $monitoringReports, string $method, string $label): Chart
    {
        $data = [];
        foreach ($monitoringReports as $monitoringReport) {
            /** @var MonitoringReport $monitoringReport */
            $updatedAt = $monitoringReport->getUpdatedAt();
            $data[$updatedAt->format('d/m/Y')] = $monitoringReport->$method();
        }
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $dateFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Europe/Paris');

        uksort($data, [$this, 'compareDates']);

        $chart->setData([
            'labels' => array_map(fn ($elem) => $dateFormatter->format(\DateTimeImmutable::createFromFormat('d/m/Y', $elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans($label),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'scales' => [
                'y' => [
                    'min' => 0,
                    'max' => 50,
                    'ticks' => [
                        'stepSize' => 1,
                    ],
                ],
            ],
        ]);

        return $chart;
    }

    private function generateNumberOfFilesPerDay(iterable $monitoringReports): Chart
    {
        $data = [];
        foreach ($monitoringReports as $monitoringReport) {
            /** @var MonitoringReport $monitoringReport */
            $updatedAt = $monitoringReport->getUpdatedAt();
            $data[$updatedAt->format('d/m/Y')] = $monitoringReport->getFilesNumber();
        }
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $dateFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Europe/Paris');

        uksort($data, [$this, 'compareDates']);

        $chart->setData([
            'labels' => array_map(fn ($elem) => $dateFormatter->format(\DateTimeImmutable::createFromFormat('d/m/Y', $elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Number of files'),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        return $chart;
    }

    private function generateNumberOfSharesPerDay(iterable $monitoringReports): Chart
    {
        $data = [];
        foreach ($monitoringReports as $monitoringReport) {
            /** @var MonitoringReport $monitoringReport */
            $updatedAt = $monitoringReport->getUpdatedAt();
            $data[$updatedAt->format('d/m/Y')] = $monitoringReport->getSharesNumber();
        }
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $dateFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Europe/Paris');

        uksort($data, [$this, 'compareDates']);

        $chart->setData([
            'labels' => array_map(fn ($elem) => $dateFormatter->format(\DateTimeImmutable::createFromFormat('d/m/Y', $elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Number of shares'),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'scales' => [
                'y' => [
                    'ticks' => [
                        'stepSize' => 1,
                    ],
                ],
            ],
        ]);

        return $chart;
    }

    private function erroredMonitoringReports(array $monitoringReports): array
    {
        $reports = array_filter($monitoringReports, fn (MonitoringReport $monitoringReport) => !$monitoringReport->isReturnStatusCodeOK());
        usort($reports, fn ($a, $b) => $a->getUpdatedAt() <=> $b->getUpdatedAt());

        return array_slice($reports, 0, 5);
    }

    private function compareDates($date1, $date2): int
    {
        return \DateTimeImmutable::createFromFormat('d/m/Y', $date1)->getTimestamp() - \DateTimeImmutable::createFromFormat('d/m/Y', $date2)->getTimestamp();
    }
}
