<?php

namespace App\Controller\Admin;

use App\Admin\Field\IdentifierLinkField;
use App\Admin\Field\InviteField;
use App\Admin\Field\ScoreField;
use App\Admin\Field\TextDetailsField;
use App\Admin\Field\TextLinkField;
use App\Entity\Submission;
use App\Exceptions\NoAvailableOfficeServer;
use App\Message\AdminLoggingAction;
use App\Message\DiscoursePostAction;
use App\Repository\SubmissionRepository;
use App\Services\AdminUrlGenerator;
use App\Services\ConfigService;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LocaleField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatableMessage;

class SubmissionCrudController extends AbstractCrudController
{
    public function __construct(private readonly SpaceManager $spaceManager, private readonly Mailer $mailer, private readonly MessageBusInterface $bus, private readonly ContainerBagInterface $params, private readonly LoggerInterface $logger, private readonly SubmissionRepository $repository, private readonly ConfigService $configService, private readonly AdminUrlGenerator $adminURLGenerator)
    {
    }

    public static function getEntityFqcn(): string
    {
        return Submission::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPageTitle('index', new TranslatableMessage('Submissions'))
            ->setDefaultSort(['status' => 'ASC', 'invite.id' => 'ASC', 'createdAt' => 'ASC'])
            ->setPaginatorPageSize(50)
            ->setEntityLabelInSingular(new TranslatableMessage('Submission'))
            ->setEntityLabelInSingular(new TranslatableMessage('Submissions'))
            ->renderSidebarMinimized()
            ->overrideTemplate('layout', 'admin/layout.html.twig')
            ->overrideTemplate('crud/detail', 'admin/submission/detail.html.twig')
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(ChoiceFilter::new('status')->setChoices([
                'Pending' => Submission::SUBMISSION_STATUS_PENDING,
                'Approved' => Submission::SUBMISSION_STATUS_APPROVED,
                'Rejected' => Submission::SUBMISSION_STATUS_REJECTED,
            ]))
            // ->add('invite')
            ->add('email')
            ->add('identity')
            ->add(ChoiceFilter::new('responsible')->setChoices([
                'Yes' => Submission::SUBMISSION_RESPONSIBLE,
                'No' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                "I'm not sure" => Submission::SUBMISSION_RESPONSIBLE_UNKNOWN,
            ]))
            ->add(ChoiceFilter::new('accountsNeeded')->setChoices([
                '- Aucun -' => '',
                '1 à 5' => '1-5',
                '6 à 10' => '6-10',
                '11 à 20' => '11-20',
                '21 à 30' => '21-30',
                '31 à 40' => '31-40',
                '41 à 50' => '41-50',
                '51 à 100' => '51-100',
                '101 à 200' => '101-200',
                '201 à 500' => '201-500',
                '+ de 500' => '+500',
            ]))
            ->add('name')
            ->add('domain')
            ->add(ChoiceFilter::new('type')->setChoices([
                'Association de fait (collectif informel)' => Submission::SUBMISSION_TYPE_COLLECTIVE,
                'Association loi 1901' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901,
                'Association loi 1907' => Submission::SUBMISSION_TYPE_ASSOCIATION_1907,
                'Syndicat' => Submission::SUBMISSION_TYPE_SYNDICATE,
                'SCOP' => Submission::SUBMISSION_TYPE_SCOP,
                'SCIC' => Submission::SUBMISSION_TYPE_SCIC,
                'Entreprise (SA, SARL, SAS)' => Submission::SUBMISSION_TYPE_ENTREPRISE,
                'Institution publique' => Submission::SUBMISSION_TYPE_PUBLIC,
                'Autre' => Submission::SUBMISSION_TYPE_OTHER,
            ]))
            ->add('nbBeneficiaries')
            ->add('nbEmployees')
            ->add('nbMembers')
            ->add('budget')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $approveAction = Action::new('approveSubmission', new TranslatableMessage('Approve Submission'), 'fa-solid fa-thumbs-up')
            ->linkToCrudAction('approveSubmission')
            ->setCssClass('menu-item-custom menu-item-success')
            ->displayIf(function (Submission $entity) {
                return Submission::SUBMISSION_STATUS_APPROVED !== $entity->getStatus() && !$this->configService->isDeploymentDisabled();
            })
            ->addCssClass('confirm-action')
            ->setHtmlAttributes([
                'data-bs-toggle' => 'modal',
                'data-bs-target' => '#modal-confirm',
            ])
        ;

        $rejectAction = Action::new('rejectSubmission', new TranslatableMessage('Reject Submission'), 'fa-solid fa-thumbs-down')
            ->linkToCrudAction('rejectSubmission')
            ->setCssClass('menu-item-custom menu-item-danger')
            ->displayIf(function (Submission $entity) {
                return Submission::SUBMISSION_STATUS_PENDING === $entity->getStatus() && !$this->configService->isDeploymentDisabled();
            })
            ->addCssClass('confirm-action')
            ->setHtmlAttributes([
                'data-bs-toggle' => 'modal',
                'data-bs-target' => '#modal-confirm',
            ])
        ;

        $createDiscussionAction = Action::new('createDiscussionOnDiscourse', new TranslatableMessage('Create discussion'), 'fa-solid fa-comment')
            ->linkToCrudAction('createDiscussionOnDiscourse')
            ->setCssClass('menu-item-custom menu-item-info')
            ->displayIf(static function (Submission $entity) {
                return Submission::SUBMISSION_STATUS_PENDING === $entity->getStatus() && !isset($entity->getMetadata()['discourse_topic_id']);
            })
        ;

        $approveMultipleAction = Action::new('approveMultipleSubmission', new TranslatableMessage('Approve submissions'), 'fa-solid fa-thumbs-up')
            ->linkToCrudAction('approveMultipleSubmission')
            ->setCssClass('btn btn-success')
        ;

        $rejectMultipleAction = Action::new('rejectMultipleSubmission', new TranslatableMessage('Reject submissions'), 'fa-solid fa-thumbs-down')
            ->linkToCrudAction('rejectMultipleSubmission')
            ->setCssClass('btn btn-warning')
        ;

        $createMultipleDiscussionsAction = Action::new('createMultipleDiscussionsOnDiscourse', new TranslatableMessage('Create discussions'), 'fa-solid fa-comment')
            ->linkToCrudAction('createMultipleDiscussionsOnDiscourse')
            ->setCssClass('btn btn-info')
        ;

        $nextEntityAction = Action::new('nextEntity', new TranslatableMessage('Next'), 'fa-solid fa-arrow-right')
            ->linkToCrudAction('nextEntity');

        $prevEntityAction = Action::new('prevEntity', new TranslatableMessage('Previous'), 'fa-solid fa-arrow-left')
            ->linkToCrudAction('prevEntity');

        return $actions
            ->add(Crud::PAGE_DETAIL, $nextEntityAction)
            ->add(Crud::PAGE_DETAIL, $prevEntityAction)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, $approveAction)
            ->add(Crud::PAGE_EDIT, $approveAction)
            ->add(Crud::PAGE_DETAIL, $approveAction)
            ->add(Crud::PAGE_INDEX, $rejectAction)
            ->add(Crud::PAGE_EDIT, $rejectAction)
            ->add(Crud::PAGE_DETAIL, $rejectAction)
            ->add(Crud::PAGE_INDEX, $createDiscussionAction)
            ->add(Crud::PAGE_EDIT, $createDiscussionAction)
            ->add(Crud::PAGE_DETAIL, $createDiscussionAction)
            ->addBatchAction($approveMultipleAction)
            ->addBatchAction($rejectMultipleAction)
            ->addBatchAction($createMultipleDiscussionsAction)
            ->reorder(
                Crud::PAGE_INDEX,
                [
                    Action::DETAIL,
                    'approveSubmission',
                    'rejectSubmission',
                    'createDiscussionOnDiscourse',
                    Action::EDIT,
                    Action::DELETE,
                    'approveMultipleSubmission',
                    'rejectMultipleSubmission',
                    'createMultipleDiscussionsOnDiscourse',
                    Action::BATCH_DELETE]
            )
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addFieldset(new TranslatableMessage('Metadata')),
            IdField::new('id')->setLabel(new TranslatableMessage('ID'))->hideOnForm(),
            ChoiceField::new('status')->setLabel(new TranslatableMessage('Status'))
                ->renderExpanded()
                ->renderAsBadges([
                    Submission::SUBMISSION_STATUS_PENDING => 'info',
                    Submission::SUBMISSION_STATUS_APPROVED => 'success',
                    Submission::SUBMISSION_STATUS_REJECTED => 'danger',
                ])
                ->setTranslatableChoices([
                    Submission::SUBMISSION_STATUS_PENDING => new TranslatableMessage('Pending'),
                    Submission::SUBMISSION_STATUS_APPROVED => new TranslatableMessage('Approved'),
                    Submission::SUBMISSION_STATUS_REJECTED => new TranslatableMessage('Rejected'),
                ]),
            ScoreField::new('score')->setLabel(new TranslatableMessage('Score'))
                ->hideOnForm(),
            InviteField::new('invite')->setLabel(new TranslatableMessage('Invite'))->onlyOnIndex(),
            AssociationField::new('space')->setLabel(new TranslatableMessage('Space'))->hideOnForm()->hideOnIndex(),
            AssociationField::new('invite')->setLabel(new TranslatableMessage('Invite'))->hideOnIndex(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at'))->onlyOnDetail()->onlyOnIndex(),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->onlyOnDetail(),

            FormField::addFieldset(new TranslatableMessage('Identity')),
            EmailField::new('email')->hideOnIndex()->setLabel(new TranslatableMessage('Email')),
            TextField::new('identity')->setLabel(new TranslatableMessage('Identity')),
            ChoiceField::new('responsible')->setLabel(new TranslatableMessage('Responsible'))->hideOnIndex()
                ->renderAsBadges([
                    Submission::SUBMISSION_NOT_RESPONSIBLE => 'warning',
                    Submission::SUBMISSION_RESPONSIBLE => 'success',
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => 'warning',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Submission::SUBMISSION_RESPONSIBLE => new TranslatableMessage('Yes'),
                    Submission::SUBMISSION_NOT_RESPONSIBLE => new TranslatableMessage('No'),
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => new TranslatableMessage("I'm not sure"),
                ]),
            ChoiceField::new('alreadyUsed')->setLabel(new TranslatableMessage('Already used Nextcloud'))
                ->renderAsBadges([
                    Submission::SUBMISSION_ALREADY_USED_AS_USER => 'info',
                    Submission::SUBMISSION_ALREADY_USED_AS_ADMIN => 'success',
                    Submission::SUBMISSION_ALREADY_USED_AT_CHATONS => 'warning',
                    Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN => 'info',
                ])
                ->allowMultipleChoices()
                ->renderExpanded()
                ->setTranslatableChoices([
                    Submission::SUBMISSION_ALREADY_USED_AS_USER => new TranslatableMessage('As an user'),
                    Submission::SUBMISSION_ALREADY_USED_AS_ADMIN => new TranslatableMessage('As an administrator'),
                    Submission::SUBMISSION_ALREADY_USED_AT_CHATONS => new TranslatableMessage('At a host of the <a href="https://chatons.org" target="_blank">CHATONS</a> collective'),
                    Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN => new TranslatableMessage('I am not familiar with Nextcloud'),
                ]),
            ChoiceField::new('accountsNeeded')->setLabel(new TranslatableMessage('Accounts needed'))
                ->renderAsBadges([
                    '1-5' => 'success',
                    '6-10' => 'success',
                    '11-20' => 'info',
                    '21-30' => 'info',
                    '31-40' => 'info',
                    '41-50' => 'warning',
                    '51-100' => 'danger',
                    '101-200' => 'danger',
                    '201-500' => 'danger',
                    '+500' => 'danger',
                ])
                ->setChoices([
                    '- Aucun -' => '',
                    '1 à 5' => '1-5',
                    '6 à 10' => '6-10',
                    '11 à 20' => '11-20',
                    '21 à 30' => '21-30',
                    '31 à 40' => '31-40',
                    '41 à 50' => '41-50',
                    '51 à 100' => '51-100',
                    '101 à 200' => '101-200',
                    '201 à 500' => '201-500',
                    '+ de 500' => '+500',
                ])
                ->hideOnIndex(),

            FormField::addFieldset(new TranslatableMessage('Structure')),
            IdentifierLinkField::new('identifier')->hideOnIndex()->setLabel(new TranslatableMessage('Identification number (SIREN / RNA)')),
            TextField::new('name')->setLabel(new TranslatableMessage('Name')),
            TextLinkField::new('domain')->setLabel(new TranslatableMessage('Frama.space identifier')),
            ChoiceField::new('type')->setLabel(new TranslatableMessage('Organization type'))
                ->renderAsBadges([
                    Submission::SUBMISSION_TYPE_COLLECTIVE => 'info',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1901 => 'success',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1907 => 'info',
                    Submission::SUBMISSION_TYPE_SYNDICATE => 'info',
                    Submission::SUBMISSION_TYPE_SCOP => 'warning',
                    Submission::SUBMISSION_TYPE_SCIC => 'warning',
                    Submission::SUBMISSION_TYPE_ENTREPRISE => 'danger',
                    Submission::SUBMISSION_TYPE_PUBLIC => 'danger',
                    Submission::SUBMISSION_TYPE_OTHER => 'info',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices(Submission::submissionTypesToNames()),
            IntegerField::new('creationYear')->hideOnIndex()->setLabel(new TranslatableMessage('Creation year')),
            LanguageField::new('organisationLanguage')->setLabel(new TranslatableMessage('Main language of the organisation'))->hideOnIndex(),
            CountryField::new('country')->setLabel(new TranslatableMessage('Country of the organisation'))->hideOnIndex()->includeOnly(['FR', 'BE', 'CH', 'CA', 'LU', 'BI', 'BJ', 'CD', 'CG', 'CI', 'CM', 'CF', 'KM', 'DJ', 'GA', 'GN', 'GQ', 'HT', 'MC', 'MG', 'NE', 'RW', 'SC', 'SN', 'TD', 'TG', 'VU']),
            TextField::new('postalCode')->setLabel(new TranslatableMessage('Postal code'))->hideOnIndex(),
            TextDetailsField::new('mainActions')->setLabel(new TranslatableMessage('Motivations'))->onlyOnIndex(),
            TextEditorField::new('object')->setLabel(new TranslatableMessage('Purpose or mission'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),
            TextEditorField::new('mainActions')->setLabel(new TranslatableMessage('Main actions'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),

            FormField::addFieldset(new TranslatableMessage('Details')),
            ArrayField::new('topics')->setLabel(new TranslatableMessage('Fields of intervention'))->setRequired(false)->hideOnIndex(),
            TextEditorField::new('reasons')->setLabel(new TranslatableMessage('Reasons for needing a Frama.space account'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),
            UrlField::new('website')->setLabel(new TranslatableMessage('Website')),
            IntegerField::new('nbBeneficiaries')->hideOnIndex()->setLabel(new TranslatableMessage('Number of beneficiaries')),
            IntegerField::new('nbEmployees')->hideOnIndex()->setLabel(new TranslatableMessage('Number of employees')),
            IntegerField::new('nbMembers')->hideOnIndex()->setLabel(new TranslatableMessage('Number of members')),
            MoneyField::new('budget')->setLabel(new TranslatableMessage('Annual budget'))->setCurrency('EUR')->setNumDecimals(0)->setStoredAsCents(false)->hideOnIndex(),
            LocaleField::new('locale')->setLabel(new TranslatableMessage('Locale'))->setRequired(false)->hideOnIndex(),
            UrlField::new('getDiscourseDiscussionUrl')->setLabel(new TranslatableMessage('Discussion'))->onlyOnIndex(),
        ];
    }

    public function approveSubmission(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Submission $submission */
        $submission = $entity->getInstance();
        $oldSubmissionStatus = $submission->getStatus();

        $submission->setStatus(Submission::SUBMISSION_STATUS_APPROVED);

        $entityManager->persist($submission);
        $entityManager->flush();

        $this->logApproval($submission, $context->getUser());

        try {
            $space = $this->spaceManager->createSpaceFromSubmission($submission);
            $url = $this->adminURLGenerator->backendAdminSpaceURL($space);

            return $this->redirect($url);
        } catch (NoAvailableOfficeServer) {
            $this->addFlash('danger', new TranslatableMessage('There is no available office server for this submission. You need to create a new office server before approving this submission.'));
            $submission->setStatus($oldSubmissionStatus ?? 0);
            $entityManager->persist($submission);
            $entityManager->flush();

            return $this->redirect($this->adminURLGenerator->backendAdminSubmissionIndex());
        }
    }

    public function rejectSubmission(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Submission $submission */
        $submission = $entity->getInstance();

        $submission->setStatus(Submission::SUBMISSION_STATUS_REJECTED);
        $entityManager->persist($submission);
        $entityManager->flush();

        $this->logRejection($submission, $context->getUser());
        $this->mailer->sendRejectionEmail($submission);

        return $this->redirect($this->adminURLGenerator->backendAdminSubmissionIndex());
    }

    public function createDiscussionOnDiscourse(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        /** @var Submission $submission */
        $submission = $entity->getInstance();

        $this->logDiscourseDiscussionCreation($submission);

        $this->bus->dispatch(new DiscoursePostAction($submission->getId()));

        $this->addFlash('info', new TranslatableMessage('Forum post was scheduled to be created'));

        $url = $this->adminURLGenerator->backendAdminSubmissionURL($submission);

        return $this->redirect($url);
    }

    public function approveMultipleSubmission(AdminContext $context, BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Submission $submission */
            $submission = $entityManager->find($className, $id);
            if (Submission::SUBMISSION_STATUS_PENDING != $submission->getStatus()) {
                continue;
            }
            $submission->setStatus(Submission::SUBMISSION_STATUS_APPROVED);
            $entityManager->persist($submission);
            $this->logApproval($submission, $context->getUser());
            $this->spaceManager->createSpaceFromSubmission($submission);
        }

        $entityManager->flush();

        return $this->redirect($context->getRequest()->headers->get('referer'));
    }

    public function rejectMultipleSubmission(AdminContext $context, BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Submission $submission */
            $submission = $entityManager->find($className, $id);
            $submission->setStatus(Submission::SUBMISSION_STATUS_REJECTED);
            $entityManager->persist($submission);

            $this->logRejection($submission, $context->getUser());

            $this->mailer->sendRejectionEmail($submission);
        }

        $entityManager->flush();

        return $this->redirect($context->getRequest()->headers->get('referer'));
    }

    public function createMultipleDiscussionsOnDiscourse(AdminContext $context, BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Submission|null $submission */
            $submission = $entityManager->find($className, $id);
            $this->logDiscourseDiscussionCreation($submission);
            $this->bus->dispatch(new DiscoursePostAction($id));
        }

        $entityManager->flush();

        return $this->redirect($context->getRequest()->headers->get('referer'));
    }

    private function logRejection(Submission $submission, ?UserInterface $user): void
    {
        if (!$user) {
            return;
        }
        $url = $this->adminURLGenerator->backendAdminSubmissionURL($submission);
        $this->logger->info('User '.$user->getUserIdentifier().' has rejected submission n°'.$submission->getId().' ('.$submission->getName().' - '.$submission->getDomain().'.frama.space)');
        $this->bus->dispatch(new AdminLoggingAction('Submission for space '.$submission->getName().' has been rejected.', 'User '.$user->getUserIdentifier().' has rejected submission for space '.$submission->getName().' ('.$submission->getDomain().').'."\nAccess the submission details: ".$url."\nThe submission data will be deleted in ".$this->params->get('app.submission.days_before_purge').' days.', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }

    private function logApproval(Submission $submission, ?UserInterface $user): void
    {
        if (!$user) {
            return;
        }
        $url = $this->adminURLGenerator->backendAdminSubmissionURL($submission);
        $this->logger->info('User '.$user->getUserIdentifier().' has approved submission n°'.$submission->getId().' ('.$submission->getName().' - '.$submission->getDomain().'.frama.space)');
        $this->bus->dispatch(new AdminLoggingAction('Submission for space '.$submission->getName().' has been approved.', 'User '.$user->getUserIdentifier().' has approved submission for space '.$submission->getName().' ('.$submission->getDomain().').'."\nThe space should be created shortly.\nAccess the submission details: ".$url, ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }

    private function logDiscourseDiscussionCreation(?Submission $submission): void
    {
        if (!$submission) {
            return;
        }
        $this->logger->info('Creating discussion on Discourse for submission n°'.$submission->getId().' ('.$submission->getName().' - '.$submission->getDomain().'.frama.space)');
    }

    public function nextEntity(AdminContext $context): RedirectResponse
    {
        /** @var Submission $submission */
        $submission = $context->getEntity()->getInstance();
        $next = $this->repository->next($submission);
        if ($next) {
            $url = $this->adminURLGenerator->backendAdminSubmissionURL($next);
        } else {
            $this->addFlash('info', new TranslatableMessage('You reached the end of submissions'));
            $url = $this->adminURLGenerator->backendAdminSubmissionIndex();
        }

        return $this->redirect($url);
    }

    public function prevEntity(AdminContext $context): RedirectResponse
    {
        /** @var Submission $submission */
        $submission = $context->getEntity()->getInstance();
        $prev = $this->repository->previous($submission);
        if ($prev) {
            $url = $this->adminURLGenerator->backendAdminSubmissionURL($prev);
        } else {
            $this->addFlash('info', new TranslatableMessage('You reached the end of submissions'));
            $url = $this->adminURLGenerator->backendAdminSubmissionIndex();
        }

        return $this->redirect($url);
    }
}
