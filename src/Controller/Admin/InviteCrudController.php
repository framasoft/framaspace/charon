<?php

namespace App\Controller\Admin;

use App\Entity\Invite;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use Symfony\Component\Translation\TranslatableMessage;

class InviteCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPaginatorPageSize(50)
        ;
    }

    public static function getEntityFqcn(): string
    {
        return Invite::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email')->setLabel(new TranslatableMessage('Email of the organisation to invite')),
            DateTimeField::new('usedAt')->setLabel(new TranslatableMessage('Used at'))->hideOnForm(),
            AssociationField::new('submission')->setLabel(new TranslatableMessage('Submission'))->hideOnForm(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at'))->hideOnForm(),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->hideOnForm(),
        ];
    }
}
