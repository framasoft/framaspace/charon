<?php

namespace App\Controller\Admin;

use App\Admin\Field\ArrayDetailsField;
use App\Admin\Field\TextLinkField;
use App\Entity\Job;
use App\Form\Type\JsonCodeEditorType;
use App\Message\AdminLoggingAction;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\AdminNotifierInterface;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class JobCrudController extends AbstractCrudController
{
    private AdminUrlGenerator $adminURLGenerator;
    private MessageBusInterface $bus;
    private LoggerInterface $logger;

    public function __construct(AdminUrlGenerator $adminUrlGenerator, MessageBusInterface $bus, LoggerInterface $logger)
    {
        $this->adminURLGenerator = $adminUrlGenerator;
        $this->bus = $bus;
        $this->logger = $logger;
    }

    public static function getEntityFqcn(): string
    {
        return Job::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(['status' => 'ASC', 'updatedAt' => 'DESC'])
            ->setPaginatorPageSize(50)
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $retryAction = Action::new('retryJob', new TranslatableMessage('Retry job'), 'fa-solid fa-arrow-rotate-right')
            ->linkToCrudAction('retryJob')
            ->setCssClass('menu-item-custom menu-item-info')
            ->displayIf(static function (Job $entity) {
                return Job::JOB_STATUS_ERROR === $entity->getStatus();
            })
        ;

        $markAsDone = Action::new('markAsDone', new TranslatableMessage('Mark as done'), 'fa-solid fa-check')
            ->linkToCrudAction('markAsDone')
            ->setCssClass('menu-item-custom menu-item-success')
            ->displayIf(static function (Job $entity) {
                return Job::JOB_STATUS_DONE !== $entity->getStatus();
            })
        ;

        $retryMultipleAction = Action::new('retryMultipleJobs', new TranslatableMessage('Retry jobs'), 'fa-solid fa-arrow-rotate-right')
            ->linkToCrudAction('retryMultipleJobs')
            ->setCssClass('btn btn-info')
        ;

        $markAsDoneMultiple = Action::new('markAsDoneMultiple', new TranslatableMessage('Mark jobs as done'), 'fa-solid fa-check')
            ->linkToCrudAction('markAsDoneMultiple')
            ->setCssClass('btn btn-success')
        ;

        return $actions->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->addBatchAction($retryMultipleAction)
            ->addBatchAction($markAsDoneMultiple)
            ->add(Crud::PAGE_DETAIL, $retryAction)
            ->add(Crud::PAGE_INDEX, $retryAction)
            ->add(Crud::PAGE_DETAIL, $markAsDone)
            ->add(Crud::PAGE_INDEX, $markAsDone)
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(ChoiceFilter::new('type')->setChoices([
                'Create space' => Job::JOB_TYPE_CREATE_SPACE,
                'Update space' => Job::JOB_TYPE_UPDATE_SPACE,
                'Change office suite' => Job::JOB_TYPE_CHANGE_OFFICE,
                'Disable space' => Job::JOB_TYPE_DISABLE_SPACE,
                'Reenable space' => Job::JOB_TYPE_REENABLE_SPACE,
                'Delete space' => Job::JOB_TYPE_DELETE_SPACE,
                'Post Install Admin User Configuration' => Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION,
                'Post Install Finalization' => Job::JOB_TYPE_POST_INSTALL_LAST_STEP,
                'Delete shared office server' => Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER,
                'Deploy Paheko for a space' => Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO,
            ]))
            ->add(ChoiceFilter::new('status')->setChoices([
                'Pending' => Job::JOB_STATUS_PENDING,
                'Ongoing' => Job::JOB_STATUS_ONGOING,
                'Error' => Job::JOB_STATUS_ERROR,
                'Success' => Job::JOB_STATUS_DONE,
            ]))
            ->add(EntityFilter::new('space'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextLinkField::new('id')->setLabel(new TranslatableMessage('ID'))->hideOnForm(),
            ChoiceField::new('type')->setLabel(new TranslatableMessage('Type'))
                ->renderAsBadges([
                    Job::JOB_TYPE_CREATE_SPACE => 'success',
                    Job::JOB_TYPE_UPDATE_SPACE => 'info',
                    Job::JOB_TYPE_CHANGE_OFFICE => 'info',
                    Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION => 'info',
                    Job::JOB_TYPE_POST_INSTALL_LAST_STEP => 'info',
                    Job::JOB_TYPE_DISABLE_SPACE => 'warning',
                    Job::JOB_TYPE_REENABLE_SPACE => 'success',
                    Job::JOB_TYPE_DELETE_SPACE => 'danger',
                    Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER => 'info',
                    Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO => 'info',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Job::JOB_TYPE_CREATE_SPACE => t('Create space'),
                    Job::JOB_TYPE_UPDATE_SPACE => t('Update space'),
                    Job::JOB_TYPE_CHANGE_OFFICE => t('Change office suite'),
                    Job::JOB_TYPE_DISABLE_SPACE => t('Disable space'),
                    Job::JOB_TYPE_REENABLE_SPACE => t('Reenable space'),
                    Job::JOB_TYPE_DELETE_SPACE => t('Delete space'),
                    Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION => t('Post Install Admin User Configuration'),
                    Job::JOB_TYPE_POST_INSTALL_LAST_STEP => t('Post Install Finalization'),
                    Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER => t('Delete shared office server'),
                    Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO => t('Deploy paheko for a space'),
                ]),
            ChoiceField::new('status')
                ->setLabel(new TranslatableMessage('Status'))
                ->renderAsBadges([
                    Job::JOB_STATUS_PENDING => 'info',
                    Job::JOB_STATUS_ONGOING => 'info',
                    Job::JOB_STATUS_ERROR => 'danger',
                    Job::JOB_STATUS_DONE => 'success',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Job::JOB_STATUS_PENDING => t('Pending'),
                    Job::JOB_STATUS_ONGOING => t('Ongoing'),
                    Job::JOB_STATUS_ERROR => t('Error'),
                    Job::JOB_STATUS_DONE => t('Success'),
                ]),
            AssociationField::new('space')->setLabel(new TranslatableMessage('Space')),
            CodeEditorField::new('data')->setLabel(new TranslatableMessage('JSON-formatted data'))->onlyOnForms()->setFormType(JsonCodeEditorType::class)->setLanguage('js'),
            ArrayDetailsField::new('data')->setLabel(new TranslatableMessage('Data'))->onlyOnDetail(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at')),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at')),
            TextareaField::new('body')->setLabel(new TranslatableMessage('Output'))->hideOnIndex(),
        ];
    }

    public function retryJob(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Job $job */
        $job = $entity->getInstance();

        $job->setStatus(Job::JOB_STATUS_PENDING);

        $entityManager->persist($job);
        $entityManager->flush();

        $jobUrl = $this->adminURLGenerator->backendAdminJobUrl($job);
        $this->logRetry($job, $context->getUser(), $jobUrl);

        return $this->redirect($jobUrl);
    }

    public function retryMultipleJobs(AdminContext $context, BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Job $job */
            $job = $entityManager->find($className, $id);
            if (Job::JOB_STATUS_ERROR === $job->getStatus()) {
                $job->setStatus(Job::JOB_STATUS_PENDING);
                $entityManager->persist($job);
                $jobUrl = $this->adminURLGenerator->backendAdminJobUrl($job);
                $this->logRetry($job, $context->getUser(), $jobUrl);
            }
        }

        $entityManager->flush();

        return $this->redirect($context->getRequest()->headers->get('referer'));
    }

    public function markAsDone(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Job $job */
        $job = $entity->getInstance();

        $job->setStatus(Job::JOB_STATUS_DONE);

        $entityManager->persist($job);
        $entityManager->flush();

        $jobUrl = $this->adminURLGenerator->backendAdminJobUrl($job);
        $this->logMarkAsDone($job, $context->getUser(), $jobUrl);

        return $this->redirect($jobUrl);
    }

    public function markAsDoneMultiple(AdminContext $context, BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Job $job */
            $job = $entityManager->find($className, $id);
            if (Job::JOB_STATUS_DONE !== $job->getStatus()) {
                $job->setStatus(Job::JOB_STATUS_DONE);
                $entityManager->persist($job);
                $jobUrl = $this->adminURLGenerator->backendAdminJobUrl($job);
                $this->logMarkAsDone($job, $context->getUser(), $jobUrl);
            }
        }

        $entityManager->flush();

        return $this->redirect($context->getRequest()->headers->get('referer'));
    }

    private function logRetry(Job $job, UserInterface $user, string $jobUrl): void
    {
        if ($job->getSpace()) {
            $this->logger->info('User '.$user->getUserIdentifier().' has retried job n°'.$job->getId().' (for space '.$job->getSpace()->getOrganizationName().' - '.$job->getSpace()->getDomain().'.frama.space)');
            $this->bus->dispatch(new AdminLoggingAction('Job n° '.$job->getId().' has been retried.', 'User '.$user->getUserIdentifier().' has retried job for space '.$job->getSpace()->getOrganizationName().' ('.$job->getSpace()->getDomain().').'."\nAccess the job details: ".$jobUrl, ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_DEFAULT]));
        } else {
            $this->logger->info('User '.$user->getUserIdentifier().' has retried job n°'.$job->getId());
            $this->bus->dispatch(new AdminLoggingAction('Job n° '.$job->getId().' has been retried.', 'User '.$user->getUserIdentifier().' has retried a job.'."\nAccess the job details: ".$jobUrl, ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_DEFAULT]));
        }
    }

    private function logMarkAsDone(Job $job, UserInterface $user, string $jobUrl): void
    {
        if ($job->getSpace()) {
            $this->logger->info('User '.$user->getUserIdentifier().' marked job n°'.$job->getId().' as done (for space '.$job->getSpace()->getOrganizationName().' - '.$job->getSpace()->getDomain().'.frama.space)');
            $this->bus->dispatch(new AdminLoggingAction('Job n° '.$job->getId().' has been marked as done.', 'User '.$user->getUserIdentifier().' has marked job as done for space '.$job->getSpace()->getOrganizationName().' ('.$job->getSpace()->getDomain().').'."\nAccess the job details: ".$jobUrl, ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        } else {
            $this->logger->info('User '.$user->getUserIdentifier().' marked job n°'.$job->getId().' as done');
            $this->bus->dispatch(new AdminLoggingAction('Job n° '.$job->getId().' has been marked as done.', 'User '.$user->getUserIdentifier()." has marked job as done.\nAccess the job details: ".$jobUrl, ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        }
    }
}
