<?php

namespace App\Controller\Admin;

use App\Entity\Announcement;
use App\Entity\Invite;
use App\Entity\Job;
use App\Entity\MonitoringReport;
use App\Entity\OfficeServer;
use App\Entity\PahekoServer;
use App\Entity\Space;
use App\Entity\Submission;
use App\Repository\SpaceRepository;
use App\Services\ConfigService;
use App\Services\Statistics;
use Doctrine\ORM\AbstractQuery;
use EasyCorp\Bundle\EasyAdminBundle\Attribute\AdminDashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\SortOrder;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\ComparisonType;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Ldap\Security\LdapUser;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[AdminDashboard(routePath: '/admin', routeName: 'admin')]
class DashboardController extends AbstractDashboardController
{
    public function __construct(private readonly AdminUrlGenerator $adminUrlGenerator, private readonly SpaceRepository $spaceRepository, private readonly ConfigService $configService, private readonly ChartBuilderInterface $chartBuilder, private readonly TranslatorInterface $translator, private readonly Statistics $statistics)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $submissionDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(SubmissionCrudController::class)
            ->set('filters[status][comparison]', ComparisonType::EQ)
            ->set('filters[status][value]', Submission::SUBMISSION_STATUS_PENDING)
            ->generateUrl();

        $monitoringReportDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(MonitoringReportCrudController::class)
            ->setAction(Action::INDEX)
            ->set('filters[returnStatusCode][comparison]', ComparisonType::NEQ)
            ->set('filters[returnStatusCode][value]', 200)
            ->set('filters[createdAt][comparison]', ComparisonType::GTE)
            ->set('filters[createdAt][value]', (new \DateTimeImmutable())->sub(new \DateInterval('P1D'))->format(\DateTimeInterface::ATOM))
            ->generateUrl();

        $tooMuchUsedSpaceMonitoringReportDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(MonitoringReportCrudController::class)
            ->setAction(Action::INDEX)
            ->set('sort[usedSpace]', SortOrder::DESC)
            ->set('filters[returnStatusCode][comparison]', ComparisonType::EQ)
            ->set('filters[returnStatusCode][value]', 200)
            ->set('filters[createdAt][comparison]', ComparisonType::GTE)
            ->set('filters[createdAt][value]', (new \DateTimeImmutable())->sub(new \DateInterval('P1D'))->format(\DateTimeInterface::ATOM))
            ->generateUrl();

        $tooMuchUsersMonitoringReportDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(MonitoringReportCrudController::class)
            ->setAction(Action::INDEX)
            ->set('filters[returnStatusCode][comparison]', ComparisonType::NEQ)
            ->set('filters[returnStatusCode][value]', 200)
            ->set('filters[createdAt][comparison]', ComparisonType::GTE)
            ->set('filters[createdAt][value]', (new \DateTimeImmutable())->sub(new \DateInterval('P1D'))->format(\DateTimeInterface::ATOM))
            ->generateUrl();

        $yearlyInactiveUsersMonitoringReportDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(MonitoringReportCrudController::class)
            ->setAction(Action::INDEX)
            ->set('filters[usersLastActiveLastYearNumber][comparison]', ComparisonType::EQ)
            ->set('filters[usersLastActiveLastYearNumber][value]', 0)
            ->generateUrl();

        $threeMonthsInactiveUsersMonitoringReportDashboardAdminUrl = $this->adminUrlGenerator
            ->setController(MonitoringReportCrudController::class)
            ->setAction(Action::INDEX)
            ->set('filters[usersLastActive3MonthNumber][comparison]', ComparisonType::EQ)
            ->set('filters[usersLastActive3MonthNumber][value]', 0)
            ->generateUrl();

        $spaceDashboardAdminUrl = $this->adminUrlGenerator->setController(SpaceCrudController::class)->generateUrl();

        return $this->render('admin/index.html.twig', [
            'pending_submission_count' => $this->statistics->getPendingSubmissionCount(),
            'submission_dashboard_admin_url' => $submissionDashboardAdminUrl,
            'failed_monitoring_reports_count' => $this->statistics->getFailedMonitoringReportsCount(),
            'failed_monitoring_reports_dashboard_admin_url' => $monitoringReportDashboardAdminUrl,
            'spaces_over_quota_count' => $this->statistics->getSpacesOverQuotaCount(),
            'spaces_with_too_much_used_space_count' => $this->statistics->getSpacesWithTooMuchUsedSpaceCount(),
            'too_much_used_space_monitoring_reports_dashboard_admin_url' => $tooMuchUsedSpaceMonitoringReportDashboardAdminUrl,
            'spaces_with_too_much_users_count' => $this->statistics->getSpacesWithTooMuchUsersCount(),
            'too_much_users_monitoring_reports_dashboard_admin_url' => $tooMuchUsersMonitoringReportDashboardAdminUrl,
            'spaces_with_yearly_inactive_users' => $this->statistics->getSpacesWithYearlyInactiveUsers(),
            'yearly_inactive_users_monitoring_reports_dashboard_admin_url' => $yearlyInactiveUsersMonitoringReportDashboardAdminUrl,
            'spaces_with_3_months_inactive_users' => $this->statistics->getSpacesWith3MonthsInactiveUsers(),
            'three_months_inactive_users_monitoring_reports_dashboard_admin_url' => $threeMonthsInactiveUsersMonitoringReportDashboardAdminUrl,
            'spaces_number' => $this->statistics->getActiveSpacesCount(),
            'space_dashboard_admin_url' => $spaceDashboardAdminUrl,
            'users_repartition_chart' => $this->generateSpaceUsersChart(),
            'used_space_repartition_chart' => $this->generateDiskSpaceUsedChart(),
            'approved_submissions_by_type' => $this->generateSpaceByStructureTypeChart(),
            'new_spaces_by_month' => $this->generateNewSpacesByMonth(),
            'spaces_by_creation_year' => $this->generateSpacesByCreationYear(),
            'spaces_by_topics' => $this->generateSpacesByTopics(),
            'space_office_type' => $this->generateSpaceOfficeTypeChart(),
            'deployment_disabled' => $this->configService->isDeploymentDisabled(),
            'general_upgrade_ongoing' => $this->configService->isGeneralUpgradeOngoing(),
        ]);
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        /** @var LdapUser $user */
        $displayName = $user->getEntry()->getAttribute('displayName')[0] ?? $user->getUserIdentifier();
        $email = $user->getEntry()->getAttribute('mail')[0] ?? '';
        $userMenu = parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($displayName);

        $avatarData = $user->getEntry()->getAttribute('jpegPhoto')[0] ?? false;
        if ($avatarData) {
            $userMenu->setAvatarUrl('data:image/jpeg;base64,'.base64_encode($avatarData));
        } else {
            $userMenu->setGravatarEmail($email);
        }

        return $userMenu;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Charon');
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud(new TranslatableMessage('Submissions'), 'fa-solid fa-envelope', Submission::class)
            ->setQueryParameter('filters[status][comparison]', '=')
            ->setQueryParameter('filters[status][value]', '0');
        yield MenuItem::linkToCrud(new TranslatableMessage('Spaces'), 'fa-solid fa-cloud', Space::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Jobs'), 'fa-solid fa-right-left', Job::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Monitoring reports'), 'fa-solid fa-check-double', MonitoringReport::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Office servers'), 'fa-solid fa-file', OfficeServer::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Paheko servers'), 'fa-solid fa-file-invoice-dollar', PahekoServer::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Invites'), 'fa-solid fa-paper-plane', Invite::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Announcements'), 'fa-solid fa-bullhorn', Announcement::class);

        yield MenuItem::section(new TranslatableMessage('Links'));
        yield MenuItem::linkToUrl(new TranslatableMessage('Documentation'), 'fa-solid fa-book', 'https://asso.framasoft.org/wiki/doku.php/contribuer-a-framasoft/framaspace')->setLinkTarget('_blank');
        yield MenuItem::linkToUrl(new TranslatableMessage('Technical'), 'fa-solid fa-wrench', 'https://tech.framasoft.org/doku.php/services:framaspace:charon')->setLinkTarget('_blank');
    }

    private function generateSpaceUsersChart(): Chart
    {
        $spaces_users = $this->spaceRepository->getUsersForSpaces();
        $ranges = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
        $range_users = $this->createRanges(array_column($spaces_users, 'usersNumber'), $ranges);

        $usersChart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
        $usersChart->setData([
            'labels' => ['0 - 5', '5 - 10', '10 - 15', '15 - 20', '20 - 25', '25 - 30', '30 - 35', '35 - 40', '40 - 45', '45 - 50'],
            'datasets' => [
                [
                    'label' => $this->translator->trans('Spaces'),
                    'backgroundColor' => ['#977ac2'],
                    'data' => $range_users,
                ],
            ],
        ]);

        return $usersChart;
    }

    private const MO = 1_048_576;
    private const GO = 1_073_741_824;

    private function generateDiskSpaceUsedChart(): Chart
    {
        $spaces_users = $this->spaceRepository->getDiskSpaceUsedSpaces();
        $ranges = [0, 5 * self::MO, 10 * self::MO, 20 * self::MO, 50 * self::MO, 100 * self::MO, 250 * self::MO, 500 * self::MO, self::GO, 5 * self::GO, 20 * self::GO, 50 * self::GO];
        $range_space = $this->createRanges(array_column($spaces_users, 'usedSpace'), $ranges);

        $usersChart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
        $usersChart->setData([
            'labels' => ['0 - 5MB', '5MB - 10MB', '10MB - 20MB', '20MB - 50MB', '50MB - 100MB', '100MB - 250MB', '250MB - 500MB', '500MB - 1GB', '1GB - 5GB', '5GB - 20GB', '20GB - 50GB'],
            'datasets' => [
                [
                    'label' => $this->translator->trans('Spaces'),
                    'backgroundColor' => ['#977ac2'],
                    'data' => $range_space,
                ],
            ],
        ]);

        return $usersChart;
    }

    private function createRanges(array $data, array $ranges): array
    {
        return array_reduce($data, function ($acc, $item) use ($ranges) {
            for ($i = 0; $i < count($ranges) - 1; ++$i) {
                /* @psalm-suppress InvalidArrayOffset */
                if ($item >= $ranges[$i] && $item < $ranges[$i + 1]) {
                    $acc[$i] = ($acc[$i] ?? 0) + 1;
                    break;
                }
            }

            return $acc;
        }, array_fill(0, count($ranges) - 1, 0));
    }

    private function generateSpaceByStructureTypeChart(): Chart
    {
        /** @var array<string,int> $data */
        $data = array_reduce($this->spaceRepository->countSpacesByOrganisationType(), function ($acc, $result) {
            $acc[$result['organisationType']] = $result['spaceCount'];

            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_DOUGHNUT);

        $chart->setData([
            'labels' => array_map(fn ($elem): string => Submission::submissionTypesToNames()[$elem] ?? 'Unknown', array_keys($data)),
            'datasets' => [
                [
                    'backgroundColor' => self::FRAMA_COLORS,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'plugins' => [
                'legend' => [
                    'position' => 'right',
                ],
            ],
        ]);

        return $chart;
    }

    private function generateNewSpacesByMonth(): Chart
    {
        $data = array_reduce($this->spaceRepository->countNewSpacesByMonths(), function ($acc, $result) {
            $acc[$result['createdAtMonth']] = $result['spaceCount'];

            return $acc;
        }, []);
        ksort($data);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $monthFormatter = new \IntlDateFormatter('fr_FR', \IntlDateFormatter::FULL, \IntlDateFormatter::FULL, 'Europe/Paris', \IntlDateFormatter::GREGORIAN, 'LLLL y');

        $chart->setData([
            'labels' => array_map(fn ($elem) => $monthFormatter->format(new \DateTime($elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Spaces'),
                    'backgroundColor' => self::FRAMA_COLORS[0],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        return $chart;
    }

    private function generateSpacesByTopics(): Chart
    {
        $data = $this->spaceRepository->countSpacesByTopics();
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);

        $data = array_reduce(array_values($data), function ($acc, $item) {
            $acc[$item['topic']] = $item['topicCount'];

            return $acc;
        }, []);

        $chart->setOptions([
            'scaleShowValues' => true,
            'scales' => [
                'x' => [
                    'ticks' => ['autoSkip' => false],
                ],
            ],
        ]);

        $chart->setData([
            'labels' => array_keys($data),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Spaces'),
                    'backgroundColor' => self::FRAMA_COLORS,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        return $chart;
    }

    private function generateSpacesByCreationYear(): Chart
    {
        $data = array_reduce($this->spaceRepository->countSpacesByCreationYear(), function ($acc, $result) {
            $acc[$result['creationYear']] = $result['spaceCount'];

            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);

        $chart->setData([
            'labels' => array_keys($data),
            'datasets' => [
                [
                    'label' => $this->translator->trans('Organization'),
                    'backgroundColor' => self::FRAMA_COLORS,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        return $chart;
    }

    public const FRAMA_COLORS = [
        '#977ac2',
        '#487982',
        '#338650',
        '#dd6418',
        '#e65740',
        '#677490',
    ];

    private function generateSpaceOfficeTypeChart(): Chart
    {
        /** @var array<int,int> $data */
        $data = array_reduce($this->spaceRepository->mesureOfficeTypeDistribution(), function ($acc, $result) {
            $acc[$result['office']] = $result['spaceCount'];

            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_PIE);

        $chart->setData([
            'labels' => array_map(fn ($elem) => match ($elem) {
                Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'OnlyOffice',
                Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'Collabora Office',
                Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => 'Shared Collabora Office',
                Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE => 'Shared OnlyOffice',
                default => 'Unknown',
            }, array_keys($data)),
            'datasets' => [
                [
                    'backgroundColor' => self::FRAMA_COLORS,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);
        $chart->setOptions([
            'plugins' => [
                'legend' => [
                    'position' => 'right',
                ],
            ],
        ]);

        return $chart;
    }

    #[Route('/admin/stats/spaces/export', name: 'admin_stats_spaces_export')]
    public function exportSpaces(Request $request): Response
    {
        $qb = $this->spaceRepository->exportCreatedSpaces();
        $response = new StreamedResponse(function () use ($qb) {
            $data = $qb->getQuery()->toIterable([], AbstractQuery::HYDRATE_ARRAY);
            $handle = fopen('php://output', 'w+');
            fputcsv($handle, $this->getExportableHead(), ';');
            foreach ($data as $result) {
                $row = $this->getExportableRow($result);
                fputcsv($handle, $row, ';');
            }
            fclose($handle);
        });

        $formatter = new \IntlDateFormatter($request->getLocale(), \IntlDateFormatter::SHORT, \IntlDateFormatter::SHORT, 'Europe/Paris');
        $filename = $this->translator->trans('Spaces Export - '.$formatter->format(new \DateTime()));
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'.csv"');

        return $response;
    }

    private function getExportableHead(): array
    {
        return [
            $this->translator->trans('ID'),
            $this->translator->trans('Domain'),
            $this->translator->trans('Contact email'),
            $this->translator->trans('Contact name'),
            $this->translator->trans('Organization name'),
            $this->translator->trans('Office solution type'),
            $this->translator->trans('Object'),
            $this->translator->trans('Topics'),
            $this->translator->trans('Main actions'),
            $this->translator->trans('Creation year'),
            $this->translator->trans('Website'),
            $this->translator->trans('Number of employees'),
            $this->translator->trans('Number of members'),
            $this->translator->trans('Number of beneficiaries'),
            $this->translator->trans('Budget'),
            $this->translator->trans('Accounts needed'),
            $this->translator->trans('Country of the organisation'),
            $this->translator->trans('Main language of the organisation'),
            $this->translator->trans('Organization type'),
            $this->translator->trans('Reasons for needing a Frama.space account'),
            $this->translator->trans('Identification number (SIREN / RNA)'),
            $this->translator->trans('Postal code'),
            $this->translator->trans('Enabled'),
        ];
    }

    public const COLUMN_NAMES = ['id', 'domain', 'contactEmail', 'contactName', 'organizationName', 'office', 'object', 'topics', 'mainActions', 'creationYear', 'website', 'nbEmployees', 'nbMembers', 'nbBeneficiaries', 'budget', 'accountsNeeded', 'country', 'organisationLanguage', 'organisationType', 'reasons', 'externalIdentifier', 'postalCode', 'enabled'];

    private function getExportableRow(array $row): array
    {
        return array_map(fn ($column) => $row[$column] ? $this->convertData($row[$column]) : null, self::COLUMN_NAMES);
    }

    private function convertData($data): string
    {
        if (is_array($data)) {
            return implode(',', $data);
        }

        return (string) $data;
    }
}
