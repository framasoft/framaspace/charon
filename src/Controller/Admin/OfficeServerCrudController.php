<?php

namespace App\Controller\Admin;

use App\Entity\OfficeServer;
use App\Entity\Space;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

class OfficeServerCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPaginatorPageSize(50)
            ->setPageTitle('index', new TranslatableMessage('Office servers'))
            ->setEntityLabelInSingular(new TranslatableMessage('office server'))
            ->setEntityLabelInPlural(new TranslatableMessage('office servers'))
        ;
    }

    public static function getEntityFqcn(): string
    {
        return OfficeServer::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->remove(Crud::PAGE_DETAIL, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('hostname')->setLabel(new TranslatableMessage('Hostname')),
            IntegerField::new('minimalPort')->setLabel(new TranslatableMessage('Minimal port')),
            IntegerField::new('maximalPort')->setLabel(new TranslatableMessage('Maximal port')),
            IntegerField::new('maxServers')->setLabel(new TranslatableMessage('Maximum servers')),
            ChoiceField::new('type')->setLabel(new TranslatableMessage('Office solution'))->renderAsBadges([
                OfficeServer::OFFICE_TYPE_STANDALONE => 'info',
                Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => 'success',
                Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE => 'success',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setChoices([
                    'Standalone' => OfficeServer::OFFICE_TYPE_STANDALONE,
                    'Shared Collabora Office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE,
                    'Shared OnlyOffice' => Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE,
                ]),
            BooleanField::new('isFull')->setLabel(new TranslatableMessage('Full')),
        ];
    }
}
