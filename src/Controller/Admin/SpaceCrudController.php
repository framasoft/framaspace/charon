<?php

namespace App\Controller\Admin;

use App\Admin\Field\ArrayDetailsField;
use App\Admin\Field\DomainLinkField;
use App\Admin\Field\IdentifierLinkField;
use App\Entity\Job;
use App\Entity\Space;
use App\Entity\Submission;
use App\Form\Type\JsonCodeEditorType;
use App\Services\AdminUrlGenerator;
use App\SpaceManager;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LocaleField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class SpaceCrudController extends AbstractCrudController
{
    private SpaceManager $spaceManager;
    private AdminUrlGenerator $adminUrlGenerator;

    public function __construct(SpaceManager $spaceManager, AdminUrlGenerator $adminUrlGenerator)
    {
        $this->spaceManager = $spaceManager;
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Space::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(['status' => 'ASC', 'enabled' => 'DESC', 'updatedAt' => 'DESC'])
            ->setPaginatorPageSize(50)
            ->overrideTemplate('crud/detail', 'admin/space/detail.html.twig');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('domain')
            ->add('organizationName')
            ->add('contactEmail')
            ->add('contactName')
            ->add(ChoiceFilter::new('status')->setChoices([
                'Pending' => Space::SPACE_STATUS_PENDING,
                'Created' => Space::SPACE_STATUS_CREATED,
            ]))
            ->add('enabled')
            ->add(ChoiceFilter::new('office')->setChoices([
                'Collabora Office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE,
                'Shared Collabora Office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE,
                'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                'Shared OnlyOffice' => Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE,
            ]))
            ->add(EntityFilter::new('submission'));
    }

    public function configureActions(Actions $actions): Actions
    {
        $disableAction = Action::new('disableSpace', new TranslatableMessage('Disable space'), 'fa-solid fa-stop')
            ->linkToCrudAction('disableSpace')
            ->setCssClass('menu-item-custom menu-item-warning')
            ->displayIf(function (Space $entity) {
                return $entity->isEnabled() && Space::SPACE_STATUS_CREATED == $entity->getStatus() && !$this->hasActiveJobs($entity);
            })
            ->addCssClass('confirm-action')
            ->setHtmlAttributes([
                'data-bs-toggle' => 'modal',
                'data-bs-target' => '#modal-confirm',
            ]);

        $reenableAction = Action::new('reenableSpace', new TranslatableMessage('Reenable space'), 'fa-solid fa-play')
            ->linkToCrudAction('reenableSpace')
            ->setCssClass('menu-item-custom menu-item-success')
            ->displayIf(function (Space $entity) {
                return !$entity->isEnabled() && Space::SPACE_STATUS_CREATED == $entity->getStatus() && !$this->hasActiveJobs($entity);
            })
            ->addCssClass('confirm-action')
            ->setHtmlAttributes([
                'data-bs-toggle' => 'modal',
                'data-bs-target' => '#modal-confirm',
            ]);

        $extraDetailAction = Action::new('extraDetail', new TranslatableMessage('Details'), 'fa-solid fa-chart-bar')
            ->linkToRoute('admin_space_show', fn (Space $space) => ['id' => $space->getId()])
            ->setCssClass('menu-item-custom menu-item-info');

        $removeAction = Action::new('removeSpace', new TranslatableMessage('Remove space'), 'fa-solid fa-trash')
            ->linkToCrudAction('removeSpace')
            ->setCssClass('menu-item-custom menu-item-danger')
            ->displayIf(function (Space $entity) {
                return !$entity->isEnabled() && Space::SPACE_STATUS_CREATED == $entity->getStatus() && !$this->hasActiveJobs($entity);
            })
            ->addCssClass('confirm-action')
            ->setHtmlAttributes([
                'data-bs-toggle' => 'modal',
                'data-bs-target' => '#modal-confirm',
            ]);

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_DETAIL, $removeAction)
            ->add(Crud::PAGE_DETAIL, $disableAction)
            ->add(Crud::PAGE_DETAIL, $reenableAction)
            ->add(Crud::PAGE_DETAIL, $extraDetailAction)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_DETAIL, Action::DELETE);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addFieldset(new TranslatableMessage('Metadata')),
            DomainLinkField::new('domain')->setLabel(new TranslatableMessage('Domain')),
            ChoiceField::new('status')->setLabel(new TranslatableMessage('Status'))->renderAsBadges([
                Space::SPACE_STATUS_PENDING => 'info',
                Space::SPACE_STATUS_CREATED => 'success',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Space::SPACE_STATUS_PENDING => t('Pending'),
                    Space::SPACE_STATUS_CREATED => t('Created'),
                ]),
            BooleanField::new('enabled')->setLabel(new TranslatableMessage('Enabled'))->renderAsSwitch(false)->hideOnForm(),

            FormField::addFieldset(new TranslatableMessage('Identity')),
            TextField::new('organizationName')->setLabel(new TranslatableMessage('Organization name')),
            EmailField::new('contactEmail')->setLabel(new TranslatableMessage('Contact email')),
            TextField::new('contactName')->setLabel(new TranslatableMessage('Contact name')),

            FormField::addFieldset(new TranslatableMessage('Infrastructure')),
            ChoiceField::new('office')->setLabel(new TranslatableMessage('Office solution'))->renderAsBadges([
                Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'info',
                Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'info',
                Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => 'success',
                Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE => 'success',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setChoices([
                    'Collabora Office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE,
                    'Shared Collabora Office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE,
                    'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                    'Shared OnlyOffice' => Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE,
                ])->hideOnForm(),
            ChoiceField::new('futureOfficeType')->setLabel(new TranslatableMessage('Office solution'))->renderAsBadges([
                Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'info',
                Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'info',
                Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => 'success',
                Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE => 'success',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setChoices([
                    'Collabora Office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE,
                    'Shared Collabora Office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE,
                    'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                    'Shared OnlyOffice' => Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE,
                ])
                ->onlyOnForms(),
            CodeEditorField::new('details')->setLabel(new TranslatableMessage('JSON-formatted data'))->onlyOnForms()->setFormType(JsonCodeEditorType::class)->setLanguage('js'),
            ArrayDetailsField::new('details')->setLabel(new TranslatableMessage('Data'))->onlyOnDetail(),
            AssociationField::new('jobs')->setLabel(new TranslatableMessage('Jobs'))->onlyOnDetail(),

            FormField::addFieldset(new TranslatableMessage('Structure')),
            IdentifierLinkField::new('externalIdentifier')->hideOnIndex()->setLabel(new TranslatableMessage('Identification number (SIREN / RNA)')),
            ChoiceField::new('organisationType')->setLabel(new TranslatableMessage('Organization type'))
                ->renderAsBadges([
                    Submission::SUBMISSION_TYPE_COLLECTIVE => 'info',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1901 => 'success',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1907 => 'info',
                    Submission::SUBMISSION_TYPE_SYNDICATE => 'info',
                    Submission::SUBMISSION_TYPE_SCOP => 'warning',
                    Submission::SUBMISSION_TYPE_SCIC => 'warning',
                    Submission::SUBMISSION_TYPE_ENTREPRISE => 'danger',
                    Submission::SUBMISSION_TYPE_PUBLIC => 'danger',
                    Submission::SUBMISSION_TYPE_OTHER => 'info',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices(Submission::submissionTypesToNames()),

            IntegerField::new('creationYear')->hideOnIndex()->setLabel(new TranslatableMessage('Creation year')),
            LanguageField::new('organisationLanguage')->setLabel(new TranslatableMessage('Main language of the organisation'))->hideOnIndex(),
            TextEditorField::new('object')->setLabel(new TranslatableMessage('Purpose or mission'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),
            TextEditorField::new('mainActions')->setLabel(new TranslatableMessage('Main actions'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),

            FormField::addFieldset(new TranslatableMessage('Details')),
            ArrayField::new('topics')->setLabel(new TranslatableMessage('Fields of intervention'))->setRequired(false)->hideOnIndex(),
            TextEditorField::new('reasons')->setLabel(new TranslatableMessage('Reasons for needing a Frama.space account'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),
            UrlField::new('website')->setLabel(new TranslatableMessage('Website'))->hideOnIndex(),
            IntegerField::new('nbBeneficiaries')->hideOnIndex()->setLabel(new TranslatableMessage('Number of beneficiaries')),
            IntegerField::new('nbEmployees')->hideOnIndex()->setLabel(new TranslatableMessage('Number of employees')),
            IntegerField::new('nbMembers')->hideOnIndex()->setLabel(new TranslatableMessage('Number of members')),
            MoneyField::new('budget')->setLabel(new TranslatableMessage('Annual budget'))->setCurrency('EUR')->setNumDecimals(0)->setStoredAsCents(false)->hideOnIndex(),
            ChoiceField::new('responsible')->setLabel(new TranslatableMessage('Responsible'))->hideOnIndex()
                ->renderAsBadges([
                    Submission::SUBMISSION_NOT_RESPONSIBLE => 'warning',
                    Submission::SUBMISSION_RESPONSIBLE => 'success',
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => 'warning',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Submission::SUBMISSION_RESPONSIBLE => new TranslatableMessage('Yes'),
                    Submission::SUBMISSION_NOT_RESPONSIBLE => new TranslatableMessage('No'),
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => new TranslatableMessage("I'm not sure"),
                ]),
            ChoiceField::new('accountsNeeded')->setLabel(new TranslatableMessage('Accounts needed'))
                ->renderAsBadges([
                    '1-5' => 'success',
                    '6-10' => 'success',
                    '11-20' => 'info',
                    '21-30' => 'info',
                    '31-40' => 'info',
                    '41-50' => 'warning',
                    '51-100' => 'danger',
                    '101-200' => 'danger',
                    '201-500' => 'danger',
                    '+500' => 'danger',
                ])
                ->setChoices([
                    '- Aucun -' => '',
                    '1 à 5' => '1-5',
                    '6 à 10' => '6-10',
                    '11 à 20' => '11-20',
                    '21 à 30' => '21-30',
                    '31 à 40' => '31-40',
                    '41 à 50' => '41-50',
                    '51 à 100' => '51-100',
                    '101 à 200' => '101-200',
                    '201 à 500' => '201-500',
                    '+ de 500' => '+500',
                ])->hideOnIndex(),
            LocaleField::new('locale')->setLabel(new TranslatableMessage('Locale'))->hideOnIndex(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at')),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->hideOnIndex(),
        ];
    }

    public function disableSpace(AdminContext $context): RedirectResponse
    {
        /** @var Space $space */
        $space = $context->getEntity()->getInstance();
        $user = $context->getUser();

        $this->spaceManager->preDisableSpace($space, $user);
        $this->spaceManager->sendDisableSpaceJobs($space);

        $url = $this->adminUrlGenerator->backendAdminSpaceURL($space);

        return $this->redirect($url);
    }

    public function reenableSpace(AdminContext $context): RedirectResponse
    {
        /** @var Space $space */
        $space = $context->getEntity()->getInstance();
        $user = $context->getUser();

        $this->spaceManager->preReenableSpace($space, $user);
        $this->spaceManager->sendReenableSpaceJobs($space);
        $url = $this->adminUrlGenerator->backendAdminSpaceURL($space);

        return $this->redirect($url);
    }

    public function removeSpace(AdminContext $context): RedirectResponse
    {
        /** @var Space $space */
        $space = $context->getEntity()->getInstance();
        $user = $context->getUser();

        $this->spaceManager->preDeleteSpace($space, $user);
        $this->spaceManager->sendDeleteSpaceJob($space);
        $url = $this->adminUrlGenerator->backendAdminSpaceURL($space);

        return $this->redirect($url);
    }

    public function hasActiveJobs(Space $space): bool
    {
        return count(array_filter($space->getJobs()->toArray(), fn (Job $job) => Job::JOB_STATUS_DONE !== $job->getStatus())) > 0;
    }
}
