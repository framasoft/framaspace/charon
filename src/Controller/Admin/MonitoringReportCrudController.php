<?php

namespace App\Controller\Admin;

use App\Admin\Field\TextLinkField;
use App\Admin\Field\TextStatusField;
use App\Entity\MonitoringReport;
use App\Twig\FormatBytesExtension;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\NumericFilter;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Translation\TranslatableMessage;

class MonitoringReportCrudController extends AbstractCrudController
{
    public function __construct(private readonly FormatBytesExtension $formatBytesExtension, private readonly EntityRepository $entityRepository, #[Autowire(param: 'app.nextcloud.storage.quota_gb')] private readonly string $quota)
    {
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPaginatorPageSize(50)
            ->setPageTitle('index', new TranslatableMessage('Monitoring reports'))
            ->setEntityLabelInSingular(new TranslatableMessage('Monitoring report'))
            ->setEntityLabelInPlural(new TranslatableMessage('Monitoring reports'))
            ->setDefaultSort(['updatedAt' => 'DESC'])
        ;
    }

    public static function getEntityFqcn(): string
    {
        return MonitoringReport::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, Action::DETAIL)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextLinkField::new('id'),
            AssociationField::new('space')->setLabel(new TranslatableMessage('Space')),
            TextStatusField::new('usedSpace')->setLabel(new TranslatableMessage('Used space'))->formatValue(fn ($value) => $value ? $this->formatBytesExtension->formatBytes($value) : null)->setMaximumValue((float) $this->quota * 1_073_741_824)->showPercentage(),
            TextStatusField::new('usersNumber')->setLabel(new TranslatableMessage('Number of users'))->setMaximumValue(50),
            NumberField::new('usersLastActive5MinutesNumber')->setLabel(new TranslatableMessage('Number of users in the last 5 minutes'))->hideOnIndex(),
            NumberField::new('usersLastActive1HourNumber')->setLabel(new TranslatableMessage('Number of users in the last hour'))->hideOnIndex(),
            NumberField::new('usersLastActive24HNumber')->setLabel(new TranslatableMessage('Number of users in the last 24 hours')),
            NumberField::new('usersLastActive7DaysNumber')->setLabel(new TranslatableMessage('Number of users in the last 7 days'))->hideOnIndex(),
            NumberField::new('usersLastActive1MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last month')),
            NumberField::new('usersLastActive3MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last 3 months'))->hideOnIndex(),
            NumberField::new('usersLastActive6MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last 6 months'))->hideOnIndex(),
            NumberField::new('usersLastActiveLastYearNumber')->setLabel(new TranslatableMessage('Number of users in the last year')),
            NumberField::new('filesNumber')->setLabel(new TranslatableMessage('Number of files')),
            NumberField::new('sharesNumber')->setLabel(new TranslatableMessage('Number of shares'))->hideOnIndex(),
            ChoiceField::new('returnStatusCode')->setLabel(new TranslatableMessage('Returned status code'))->setChoices([200 => 200, 401 => 401, 404 => 404])->renderAsBadges([
                200 => 'success',
                401 => 'danger',
                404 => 'danger',
            ]),
            TextareaField::new('returnErrorDetails')->hideOnIndex()->setLabel('Returned error details')->stripTags(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at'))->hideOnForm(),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->hideOnForm(),
            DateTimeField::new('nextRun')->setLabel(new TranslatableMessage('Next run at'))->hideOnForm(),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('space')->setLabel(new TranslatableMessage('Space')))
            ->add(NumericFilter::new('usedSpace')->setLabel(new TranslatableMessage('Used space')))
            ->add(NumericFilter::new('usersNumber')->setLabel(new TranslatableMessage('Number of users')))
            ->add(NumericFilter::new('usersLastActive5MinutesNumber')->setLabel(new TranslatableMessage('Number of users in the last 5 minutes')))
            ->add(NumericFilter::new('usersLastActive1HourNumber')->setLabel(new TranslatableMessage('Number of users in the last hour')))
            ->add(NumericFilter::new('usersLastActive24HNumber')->setLabel(new TranslatableMessage('Number of users in the last 24 hours')))
            ->add(NumericFilter::new('usersLastActive7DaysNumber')->setLabel(new TranslatableMessage('Number of users in the last 7 days')))
            ->add(NumericFilter::new('usersLastActive1MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last month')))
            ->add(NumericFilter::new('usersLastActive3MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last 3 months')))
            ->add(NumericFilter::new('usersLastActive6MonthNumber')->setLabel(new TranslatableMessage('Number of users in the last 6 months')))
            ->add(NumericFilter::new('usersLastActiveLastYearNumber')->setLabel(new TranslatableMessage('Number of users in the last year')))
            ->add(NumericFilter::new('filesNumber')->setLabel(new TranslatableMessage('Number of files')))
            ->add(NumericFilter::new('sharesNumber')->setLabel(new TranslatableMessage('Number of shares')))
            ->add(ChoiceFilter::new('returnStatusCode')->setChoices([
                '200' => 200,
                '404' => 404,
                '401' => 401,
            ])->setLabel(new TranslatableMessage('Returned error code')))
            ->add(DateTimeFilter::new('createdAt')->setLabel(new TranslatableMessage('Created at')))
            ->add(DateTimeFilter::new('updatedAt')->setLabel(new TranslatableMessage('Updated at')))
            ->add(DateTimeFilter::new('nextRun')->setLabel(new TranslatableMessage('Next run at')))
        ;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityRepository->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->andWhere(
            $qb->expr()->eq(
                'entity.updatedAt',
                '('.
                $qb->getEntityManager()->createQueryBuilder()
                    ->select($qb->expr()->max('mr2.updatedAt'))
                    ->from(MonitoringReport::class, 'mr2')
                    ->where($qb->expr()->eq('mr2.space', 'entity.space'))
                    ->getDQL()
                .')'
            )
        );

        return $qb;
    }
}
