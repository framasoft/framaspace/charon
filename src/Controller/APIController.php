<?php

namespace App\Controller;

use App\Entity\Config;
use App\Entity\Job;
use App\Entity\MonitoringReport;
use App\Entity\Space;
use App\Form\Type\SpaceType;
use App\Repository\ConfigRepository;
use App\Repository\JobRepository;
use App\Repository\SpaceRepository;
use App\Repository\SubmissionRepository;
use App\Services\OrganizationLookup\CheckRNAService;
use App\Services\OrganizationLookup\CheckSIRETService;
use App\Services\Statistics;
use App\Validator\ReservedDomains;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Nelmio\ApiDocBundle\Attribute\Model;
use Nelmio\ApiDocBundle\Attribute\Security;
use OpenApi\Attributes as OA;
use Pagerfanta\Doctrine\Collections\CollectionAdapter;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class APIController extends AbstractController
{
    private SerializerInterface $serializer;
    private LoggerInterface $logger;
    private ObjectManager $em;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger, ManagerRegistry $doctrine)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->em = $doctrine->getManager();
    }

    #[Route('/api/v1/searchOrganization', methods: ['POST'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns organization results in INSEE and DataGouv datasets matching search text', content: new OA\JsonContent(type: 'array', items: new OA\Items()))]
    #[OA\Response(response: Response::HTTP_BAD_REQUEST, description: 'Returns 400 if the search term is missing')]
    #[OA\Response(response: Response::HTTP_NOT_FOUND, description: 'Returns 409 conflict if no results were found')]
    #[OA\RequestBody(description: 'The text to search', required: true, content: [new OA\MediaType(mediaType: 'application/x-www-form-urlencoded', schema: new OA\Schema(required: ['search'], properties: [new OA\Property(property: 'search', type: 'string')], type: 'object'))])]
    public function searchOrganization(Request $request, CheckRNAService $rnaService, CheckSIRETService $siretService, SerializerInterface $serializer, RateLimiterFactory $anonymousApiLimiter): JsonResponse
    {
        $limiter = $anonymousApiLimiter->create($request->getClientIp());
        $limit = $limiter->consume(1);

        $headers = [
            'X-RateLimit-Remaining' => $limit->getRemainingTokens(),
            'X-RateLimit-Retry-After' => $limit->getRetryAfter()->getTimestamp(),
            'X-RateLimit-Limit' => $limit->getLimit(),
        ];

        if (false === $limit->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $text = trim((string) $request->request->get('search'));
        if (!$text) {
            $this->logger->debug('Called searchOrganization without search request input');

            return new JsonResponse(['error' => 'No search term passed'], Response::HTTP_BAD_REQUEST);
        }
        try {
            if (preg_match('/^W[0-9]{9}$/', $text)) {
                $elements = $rnaService->getInfosFromText($text);
            } else {
                $elements = $siretService->getInfosFromText($text);
            }
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Unable to get details from data providers', 'details' => $e->getMessage()], Response::HTTP_NOT_FOUND);
        }
        if (empty($elements)) {
            return new JsonResponse(['error' => 'No results found'], 404);
        }

        return new JsonResponse($serializer->serialize($elements, 'json'), Response::HTTP_OK, $headers, true);
    }

    #[Route('/api/v1/isAvailable', methods: ['POST'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns 200 if the subdomain is available')]
    #[OA\Response(response: Response::HTTP_BAD_REQUEST, description: 'Returns 400 if the search term is missing')]
    #[OA\Response(response: Response::HTTP_CONFLICT, description: 'Returns 409 conflict if the subdomain is NOT available')]
    #[OA\RequestBody(description: 'The subdomain to search availability for', required: true, content: [new OA\MediaType(mediaType: 'application/x-www-form-urlencoded', schema: new OA\Schema(required: ['subdomain'], properties: [new OA\Property(property: 'subdomain', type: 'string')], type: 'object'))])]
    public function isAvailable(Request $request, SubmissionRepository $submissionRepository, SpaceRepository $spaceRepository, RateLimiterFactory $spaceAvailableLimiter, ValidatorInterface $validator): Response
    {
        $limiter = $spaceAvailableLimiter->create($request->getClientIp());
        $limit = $limiter->consume(1);

        $headers = [
            'X-RateLimit-Remaining' => $limit->getRemainingTokens(),
            'X-RateLimit-Retry-After' => $limit->getRetryAfter()->getTimestamp(),
            'X-RateLimit-Limit' => $limit->getLimit(),
        ];

        if (false === $limit->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $text = trim((string) $request->request->get('subdomain'));

        $errors = $validator->validate($text, new ReservedDomains());

        if ($errors->count()) {
            return new Response(null, Response::HTTP_CONFLICT, $headers);
        }

        if (!$text) {
            $this->logger->debug('Called isAvailable without subdomain request input');

            return new Response(null, Response::HTTP_BAD_REQUEST, $headers);
        }
        if (null === $submissionRepository->findOneBy(['domain' => $text]) && null === $spaceRepository->findOneBy(['domain' => $text])) {
            return new Response(null, Response::HTTP_OK, $headers);
        } else {
            return new Response(null, Response::HTTP_CONFLICT, $headers);
        }
    }

    #[Route('/api/v1/jobs', methods: ['GET'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the list of jobs', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: Job::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[OA\Parameter(parameter: 'status', name: 'status', description: "Filter by job status\n\nStatuses:\n * `0`: Status pending\n * `10`: Status ongoing\n * `50`: Status error\n * `100`: Status done", in: 'query', schema: new OA\Schema(type: 'integer', enum: [0, 10, 50, 100]))]
    #[OA\Parameter(ref: '#/components/parameters/page')]
    #[OA\Parameter(ref: '#/components/parameters/perPage')]
    #[Security(name: 'httpAuth')]
    public function getJobs(Request $request, JobRepository $jobRepository): JsonResponse
    {
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $query = $jobRepository->findAllQueryWithFilterQueryBuilder((int) $request->query->get('status'));

        $pagerFanta = new Pagerfanta(new QueryAdapter($query));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/jobs/{job}', methods: ['PUT'])]
    #[OA\Parameter(name: 'job', in: 'path', schema: new OA\Schema(type: 'integer'))]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the job that was updated (status is now ongoing)', content: new OA\JsonContent(ref: new Model(type: Job::class)))]
    #[OA\Response(response: Response::HTTP_NOT_FOUND, description: 'Returns 404 if not found')]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function updateJob(Job $job): JsonResponse
    {
        $job->setStatus(Job::JOB_STATUS_ONGOING);
        $this->em->persist($job);
        $this->em->flush();

        return new JsonResponse($this->serializer->serialize($job, 'json'), Response::HTTP_OK, [], true);
    }

    #[Route('/api/v1/jobs/{job}', methods: ['POST'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the job that was finalized (status is now done or error)', content: new OA\JsonContent(ref: new Model(type: Job::class)))]
    #[OA\Parameter(name: 'job', in: 'path', schema: new OA\Schema(type: 'integer'))]
    #[OA\RequestBody(description: 'The job result', required: true, content: new OA\JsonContent(required: ['success', 'payload'], properties: [new OA\Property(property: 'success', type: 'boolean'), new OA\Property(property: 'payload', type: 'string')]))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function finalizeJob(Request $request, Job $job): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $this->logger->debug('Received finalize job with payload', ['payload' => $data]);
            $status = ($data['success'] ?? false) === true ? Job::JOB_STATUS_DONE : Job::JOB_STATUS_ERROR;
            $job->setStatus($status);
            $job->setBody(json_encode($data['payload'], JSON_THROW_ON_ERROR) ?? null);
            $this->em->persist($job);
            $this->em->flush();

            return new JsonResponse($this->serializer->serialize($job, 'json'), Response::HTTP_OK, [], true);
        } catch (\JsonException) {
            return new Response('Failed to parse JSON request payload', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/api/v1/spaces', methods: ['GET'])]
    #[OA\Parameter(name: 'enabled', description: 'Filter by enabled status', in: 'query', schema: new OA\Schema(type: 'string', enum: ['true', 'false']))]
    #[OA\Parameter(name: 'status', description: "Filter by status.\n\nStatuses:\n * `0`: Status pending\n * `10`: Status created", in: 'query', schema: new OA\Schema(type: 'integer', enum: [0, 10]))]
    #[OA\Parameter(ref: '#/components/parameters/page')]
    #[OA\Parameter(ref: '#/components/parameters/perPage')]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the list of spaces', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: Space::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function listSpaces(Request $request, SpaceRepository $spaceRepository): JsonResponse
    {
        $enabled = $request->query->get('enabled');
        $status = $request->query->get('status');
        $query = $spaceRepository->findAllQueryWithFilterQueryBuilder(null !== $enabled ? filter_var($enabled, FILTER_VALIDATE_BOOLEAN) : null, null !== $status ? (int) $status : null);
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $pagerFanta = new Pagerfanta(new QueryAdapter($query));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/spaces/{domain:space}', methods: ['GET'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns a space', content: new OA\JsonContent(ref: new Model(type: Space::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function getSpace(Space $space): JsonResponse
    {
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('space')
            ->toArray();

        return new JsonResponse($this->serializer->serialize($space, 'json', $context), 200, [], true);
    }

    #[Route('/api/v1/spaces/{domain:space}', methods: ['PATCH'])]
    #[OA\Parameter(name: 'domain', in: 'path', schema: new OA\Schema(type: 'string'))]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the updated space', content: new OA\JsonContent(ref: new Model(type: Space::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[OA\RequestBody(description: 'The updated space', required: true, content: new OA\JsonContent(properties: [new OA\Property(property: 'contactEmail', type: 'string'), new OA\Property(property: 'contactName', type: 'string'), new OA\Property(property: 'locale', type: 'string'), new OA\Property(property: 'organizationName', type: 'string'), new OA\Property(property: 'office', type: 'integer', enum: [Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, Space::SPACE_OFFICE_TYPE_ONLYOFFICE])]))]
    #[Security(name: 'httpAuth')]
    public function updateSpace(Request $request, Space $space): Response
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(SpaceType::class, $space, ['csrf_protection' => false]);
        $form->submit($data, false);
        if ($form->isValid()) {
            $this->em->persist($space);
            $this->em->flush();

            return new JsonResponse($this->serializer->serialize($space, 'json'), Response::HTTP_OK, [], true);
        }

        return new Response(null, Response::HTTP_BAD_REQUEST);
    }

    #[Route('/api/v1/spaces/{domain:space}/maintenance', methods: ['PUT'])]
    #[OA\Parameter(name: 'domain', in: 'path', schema: new OA\Schema(type: 'string'))]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the updated space', content: new OA\JsonContent(ref: new Model(type: Space::class)))]
    #[OA\RequestBody(description: 'The space maintenance status', required: true, content: new OA\JsonContent(required: ['activated'], properties: [new OA\Property(property: 'activated', type: 'boolean'), new OA\Property(property: 'message', type: 'string')]))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function setSpaceMaintenance(Request $request, Space $space): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $available = (bool) $data['activated'];
        $message = $data['message'] ?? null;
        $space->setMaintenanceActivated($available);
        $space->setMaintenanceMessage($message);
        $this->em->persist($space);
        $this->em->flush();

        return new JsonResponse($this->serializer->serialize($space, 'json'), Response::HTTP_OK, [], true);
    }

    #[Route('/api/v1/spaces/{domain:space}/monitoring', methods: ['GET'])]
    #[OA\Parameter(ref: '#/components/parameters/page', example: 2)]
    #[OA\Parameter(ref: '#/components/parameters/perPage', example: 5)]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns space monitoring reports.', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: MonitoringReport::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    #[Cache(expires: '+600 seconds')]
    public function getSpaceMonitoringReports(Request $request, Space $space): JsonResponse
    {
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $pagerFanta = new Pagerfanta(new CollectionAdapter($space->getMonitoringReports()));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/config', methods: ['GET'])]
    #[OA\Parameter(ref: '#/components/parameters/page')]
    #[OA\Parameter(ref: '#/components/parameters/perPage')]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns the list of config values', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: Config::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function listConfigValues(Request $request, ConfigRepository $configRepository): JsonResponse
    {
        $query = $configRepository->findAllQb();
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $pagerFanta = new Pagerfanta(new QueryAdapter($query));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/config/{key:config}', methods: ['GET'])]
    #[OA\Parameter(name: 'key', in: 'path', schema: new OA\Schema(type: 'string'))]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns a config value', content: new OA\JsonContent(ref: new Model(type: Config::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function getConfig(Config $config): JsonResponse
    {
        return new JsonResponse($this->serializer->serialize($config, 'json'), Response::HTTP_OK, [], true);
    }

    #[Route('/api/v1/config/{key:config}', methods: ['DELETE'])]
    #[OA\Parameter(name: 'key', in: 'path', schema: new OA\Schema(type: 'string'))]
    #[OA\Response(response: Response::HTTP_NO_CONTENT, description: 'Returns nothing')]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function deleteConfig(Config $config): Response
    {
        $this->em->remove($config);
        $this->em->flush();

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/v1/config', methods: ['PUT'])]
    #[OA\RequestBody(description: 'The config entry', required: true, content: new OA\JsonContent(required: ['key', 'value'], properties: [new OA\Property(property: 'key', type: 'string', example: 'number_of_candidates_max'), new OA\Property(property: 'value', type: 'string', example: '57'), new OA\Property(property: 'type', type: 'integer', default: 1, example: '2')]))]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns an updated config value', content: new OA\JsonContent(ref: new Model(type: Config::class)))]
    #[OA\Response(response: Response::HTTP_CREATED, description: 'Returns the created config value', content: new OA\JsonContent(ref: new Model(type: Config::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function putConfig(Request $request, ConfigRepository $configRepository, ValidatorInterface $validator): JsonResponse
    {
        $new = true;
        $configData = $this->serializer->deserialize($request->getContent(), Config::class, 'json');
        if ($config = $configRepository->find($configData->getKey())) {
            $config->setValue($configData->getValue());
            $config->setType($configData->getType());
            $new = false;
        } else {
            $config = $configData;
        }
        $validator->validate($config);
        $this->em->persist($config);
        $this->em->flush();

        return new JsonResponse($this->serializer->serialize($config, 'json'), $new ? Response::HTTP_CREATED : Response::HTTP_OK, [], true);
    }

    #[Route('/api/v1/statistics', methods: ['GET'])]
    #[OA\Response(response: Response::HTTP_OK, description: 'Returns various statistics on the submissions and spaces. Has a one hour cache on statistics.', content: new OA\JsonContent(type: 'array', items: new OA\Items(properties: [new OA\Property(property: 'active_spaces_count', type: 'int'), new OA\Property(property: 'pending_submission_count', type: 'int'), new OA\Property(property: 'failed_monitoring_reports_count', type: 'int'), new OA\Property(property: 'spaces_over_quota_count', type: 'int'), new OA\Property(property: 'spaces_with_too_much_used_space_count', type: 'int'), new OA\Property(property: 'spaces_with_too_much_users_count', type: 'int')])))]
    #[OA\Response(ref: '#/components/responses/401', response: Response::HTTP_UNAUTHORIZED)]
    #[Security(name: 'httpAuth')]
    public function getStatistics(Statistics $statistics, CacheInterface $cache): JsonResponse
    {
        $data = $cache->get('statistics', function (ItemInterface $item) use ($statistics) {
            $item->expiresAfter(3555);

            return [
                'active_spaces_count' => $statistics->getActiveSpacesCount(),
                'pending_submission_count' => $statistics->getPendingSubmissionCount(),
                'failed_monitoring_reports_count' => $statistics->getFailedMonitoringReportsCount(),
                'spaces_over_quota_count' => $statistics->getSpacesOverQuotaCount(),
                'spaces_with_too_much_used_space_count' => $statistics->getSpacesWithTooMuchUsedSpaceCount(),
                'spaces_with_too_much_users_count' => $statistics->getSpacesWithTooMuchUsersCount(),
            ];
        });

        return new JsonResponse($data);
    }
}
