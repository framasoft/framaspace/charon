<?php

namespace App\Exceptions\Nextcloud;

class NextcloudRemoteXMLException extends NextcloudRemoteException
{
    public function __construct(string $nextcloudUserId, string $spaceDomain, string $response)
    {
        parent::__construct($nextcloudUserId, $spaceDomain, $response, 'An error occurred while decoding returned XML from user creation');
    }
}
