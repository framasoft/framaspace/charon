<?php

namespace App\Exceptions\Nextcloud;

abstract class NextcloudRemoteException extends \Exception
{
    private string $nextcloudUserId;
    private string $spaceDomain;
    private string $response;

    public function __construct(string $nextcloudUserId, string $spaceDomain, string $response, string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        $this->nextcloudUserId = $nextcloudUserId;
        $this->spaceDomain = $spaceDomain;
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }

    public function getNextcloudUserId(): string
    {
        return $this->nextcloudUserId;
    }

    public function getSpaceDomain(): string
    {
        return $this->spaceDomain;
    }

    public function getResponse(): string
    {
        return $this->response;
    }
}
