<?php

namespace App\Exceptions\Nextcloud;

class NextcloudRemoteOCSException extends NextcloudRemoteException
{
    private string $ocsStatusCode;
    private string $ocsMessage;

    public function __construct(string $ocsStatusCode, string $ocsMessage, string $nextcloudUserId, string $spaceDomain, string $response)
    {
        $this->ocsStatusCode = $ocsStatusCode;
        $this->ocsMessage = $ocsMessage;
        parent::__construct($nextcloudUserId, $spaceDomain, $response, 'An error occurred while performing an OCS request');
    }

    public function getOcsStatusCode(): string
    {
        return $this->ocsStatusCode;
    }

    public function getOcsMessage(): string
    {
        return $this->ocsMessage;
    }
}
