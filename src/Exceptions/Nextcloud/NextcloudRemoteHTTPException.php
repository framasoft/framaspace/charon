<?php

namespace App\Exceptions\Nextcloud;

class NextcloudRemoteHTTPException extends NextcloudRemoteException
{
    private int $httpStatusCode;

    public function __construct(int $httpStatusCode, string $nextcloudUserId, string $spaceDomain, string $response)
    {
        $this->httpStatusCode = $httpStatusCode;
        parent::__construct($nextcloudUserId, $spaceDomain, $response, 'An HTTP error occurred while performing an OCS request');
    }

    public function getHttpStatusCode(): int
    {
        return $this->httpStatusCode;
    }
}
