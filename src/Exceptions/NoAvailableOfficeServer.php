<?php

namespace App\Exceptions;

class NoAvailableOfficeServer extends \Exception
{
    private int $officeType;

    public function __construct(int $officeType, string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        $this->officeType = $officeType;
        parent::__construct($message, $code, $previous);
    }

    public function getOfficeType(): int
    {
        return $this->officeType;
    }
}
