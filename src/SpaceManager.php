<?php

namespace App;

use App\Entity\Job;
use App\Entity\Space;
use App\Entity\Submission;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudSpaceCreationFinalizeAction;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SpaceManager
{
    private EntityManagerInterface $entityManager;
    private Mailer $mailer;
    private LoggerInterface $logger;
    private MessageBusInterface $bus;

    public const DEFAULT_ADMIN_USERNAME = 'admin';
    public const DEFAULT_REMOTE_ADMIN_USERNAME = 'framadmin';
    public const DEFAULT_REMOTE_ADMIN_EMAIL = 'tech-framaspace@framalistes.org';

    public function __construct(EntityManagerInterface $entityManager, Mailer $mailer, LoggerInterface $logger, MessageBusInterface $bus)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->bus = $bus;
    }

    public function createSpaceFromSubmission(Submission $submission): Space
    {
        $space = Space::fromSubmission($submission);
        $this->entityManager->persist($space);
        $this->entityManager->remove($submission);
        $this->entityManager->flush();

        return $space;
    }

    public function postCreateSpace(Space $space): void
    {
        $space->setStatus(Space::SPACE_STATUS_CREATED);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
        $this->bus->dispatch(new NextcloudSpaceCreationFinalizeAction($space->getId()));
    }

    public function sendChangeOfficeJobs(Space $space): void
    {
        $spaceDetails = $space->getDetails();
        $this->logger->debug("Space office type has been changed, let's create a new Job to announce changes", ['details' => $spaceDetails]);
        $this->bus->dispatch(new AdminLoggingAction('Changing space office type for '.$space->getDomain().' to '.$this->officeTypeToName($space->getFutureOfficeType()), 'The space '.$space->getDomain().' office type is going to be changed from '.$this->officeTypeToName($space->getOffice()).' to '.$this->officeTypeToName($space->getFutureOfficeType()), ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));

        $data = [...$spaceDetails, 'domain' => $space->getDomain(), 'office_type' => $space->getFutureOfficeType(), 'old_office_type' => $space->getOffice()];
        // override office port and office server if shared office
        if (OfficeServerManager::isSharedOfficeType($space->getFutureOfficeType()) && $this->hasSharedOfficeDetails($spaceDetails)) {
            $this->logger->debug("We've set office type to a shared one, so office_port and office_server are in reality shared ones");
            $data['office_port'] = $data['shared_office_port'];
            $data['office_server'] = $data['shared_office_server'];
        }

        $job = (new Job())
            ->setSpace($space)
            ->setType(Job::JOB_TYPE_CHANGE_OFFICE)
            ->setStatus(Job::JOB_STATUS_PENDING)
            ->setCreatedAt(new \DateTimeImmutable())
            ->setData([...$data, 'organisation_name' => $space->getOrganizationName()]);
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    public function postChangeOfficeType(Space $space): void
    {
        $this->logger->info('Changed space office type for '.$space->getDomain().' from '.$this->officeTypeToName($space->getOffice()).' to '.$this->officeTypeToName($space->getFutureOfficeType()));
        $this->bus->dispatch(new AdminLoggingAction('Changed space office type for '.$space->getDomain().' to '.$this->officeTypeToName($space->getFutureOfficeType()), 'The space '.$space->getDomain().' office type has been changed from '.$this->officeTypeToName($space->getOffice()).' to '.$this->officeTypeToName($space->getFutureOfficeType()), ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));

        $space->setOffice($space->getFutureOfficeType());
        $space->setFutureOfficeType(null);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
    }

    public function preDisableSpace(Space $space, ?UserInterface $user = null): void
    {
        $who = 'The space admin';
        if ($user) {
            $who = 'Meta-admin user '.$user->getUserIdentifier();
        }

        $this->logger->info('Disabling space '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Disabling space '.$space->getDomain(), $who.' has requested the space to be disabled.', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        $space->setEnabled(false);
        $space->setDeletedAt(new \DateTimeImmutable());
        $space->setDisabledReason(Space::SPACE_DISABLING_REASON_BY_SPACE_ADMIN);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
    }

    public function sendDisableSpaceJobs(Space $space): void
    {
        $details = $space->getDetails();

        $job = (new Job())
            ->setType(Job::JOB_TYPE_DISABLE_SPACE)
            ->setSpace($space)
            ->setData([...$details, 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'disabling_reason' => $space->getDisabledReason(), 'office_server' => $details['old_office_server'], 'office_port' => $details['old_office_port'], 'organisation_name' => $space->getOrganizationName()]);

        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    public function postDisableSpace(Space $space): void
    {
        $this->logger->info('Disabled space '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Disabled space '.$space->getDomain(), 'The space deactivation has been completed', ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        $this->mailer->sendDeleteConfirmation($space);
    }

    public function preReenableSpace(Space $space, ?UserInterface $user = null): void
    {
        $who = 'The space admin';
        if ($user) {
            $who = 'Meta-admin user '.$user->getUserIdentifier();
        }

        $this->logger->info('Re-enabling space '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Re-enabling space '.$space->getDomain(), $who.' has requested the space to be re-enabled.', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        $space->setEnabled(true);
        $space->setDeletedAt(null);
        $space->setDisabledReason(null);
        $this->entityManager->persist($space);
        $this->entityManager->flush();
    }

    public function sendReenableSpaceJobs(Space $space): void
    {
        $details = $space->getDetails();

        $data = [...$details, 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'organisation_name' => $space->getOrganizationName()];
        // override office port and office server if shared office
        // otherwise office_port and office_server are already populated by handlePreUpdateReactivation
        if (OfficeServerManager::isSharedOfficeType($space->getOffice()) && $this->hasSharedOfficeDetails($details)) {
            $data['office_port'] = $data['shared_office_port'];
            $data['office_server'] = $data['shared_office_server'];
        }

        $job = (new Job())
            ->setType(Job::JOB_TYPE_REENABLE_SPACE)
            ->setSpace($space)
            ->setData($data);

        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    public function postReenableSpace(Space $space): void
    {
        $this->logger->info('Re-enabled space '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Re-enabled space '.$space->getDomain(), 'The space re-enabling task has been completed', ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }

    public function preDeleteSpace(Space $space, ?UserInterface $user = null): void
    {
        $this->logger->info('Completely deleting space '.$space->getDomain());
        $who = 'The space admin';
        if ($user) {
            $who = 'Meta-admin user '.$user->getUserIdentifier();
        }
        $this->bus->dispatch(new AdminLoggingAction('Deleting space '.$space->getDomain(), $who.' has requested the space to be completely deleted.', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }

    public function sendDeleteSpaceJob(Space $space): void
    {
        $details = $space->getDetails();

        $job = (new Job())
            ->setType(Job::JOB_TYPE_DELETE_SPACE)
            ->setSpace($space)
            ->setData([...$details, 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'office_server' => $details['old_office_server'], 'office_port' => $details['old_office_port'], 'organisation_name' => $space->getOrganizationName()]);
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    public function deleteSpace(Space $space): void
    {
        $this->logger->info('Deleting space '.$space->getDomain());
        $this->bus->dispatch(new AdminLoggingAction('Deleted space '.$space->getDomain(), 'The space deletion task has been completed', ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        $this->entityManager->remove($space);
        $this->entityManager->flush();
        // Send mails to user ?
    }

    public static function officeTypeToName(?int $officeType): string
    {
        return match ($officeType) {
            Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'Collabora Office',
            Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'OnlyOffice',
            Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE => 'Shared Collabora Office',
            Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE => 'Shared OnlyOffice',
            default => 'Unknown',
        };
    }

    private function hasSharedOfficeDetails(array $details): bool
    {
        return ($details['shared_office_port'] ?? false) && ($details['shared_office_server'] ?? false);
    }
}
