<?php

namespace App\EventListener;

use App\Entity\Announcement;
use App\Message\NextcloudBatchAnnouncementAction;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;

class AnnouncementCreatedNotifier
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function postPersist(LifecycleEventArgs $lifecycleEventArgs): void
    {
        if (($announcement = $lifecycleEventArgs->getObject()) instanceof Announcement) {
            /* @var Announcement $announcement */

            $this->bus->dispatch(new NextcloudBatchAnnouncementAction($announcement->getId()));
        }
    }
}
