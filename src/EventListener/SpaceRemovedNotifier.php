<?php

namespace App\EventListener;

use App\Entity\Space;
use App\OfficeServerManager;
use Doctrine\ORM\Event\PostRemoveEventArgs;

class SpaceRemovedNotifier
{
    private OfficeServerManager $officeServerManager;

    public function __construct(OfficeServerManager $officeServerManager)
    {
        $this->officeServerManager = $officeServerManager;
    }

    public function postRemove(PostRemoveEventArgs $lifecycleEventArgs): void
    {
        $space = $lifecycleEventArgs->getObject();
        if ($space instanceof Space) {
            $sharedOfficePort = $space->getDetails()['shared_office_port'] ?? false;
            $sharedOfficeServer = $space->getDetails()['shared_office_server'] ?? false;

            if ($sharedOfficeServer && $sharedOfficePort) {
                $this->officeServerManager->maybeSendDeleteOfficeServer($sharedOfficeServer, $sharedOfficePort);
            }
        }
    }
}
