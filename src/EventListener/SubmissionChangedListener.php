<?php

namespace App\EventListener;

use App\Events\SubmissionCreatedEvent;
use App\Message\AdminLoggingAction;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsEventListener(event: SubmissionCreatedEvent::class, method: 'handleSubmissionCreated')]
final class SubmissionChangedListener
{
    private Mailer $mailer;
    private AdminUrlGenerator $adminUrlGenerator;
    private MessageBusInterface $bus;

    public function __construct(Mailer $mailer, AdminUrlGenerator $adminUrlGenerator, MessageBusInterface $bus)
    {
        $this->mailer = $mailer;
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->bus = $bus;
    }

    public function handleSubmissionCreated(SubmissionCreatedEvent $event): void
    {
        $submission = $event->getSubmission();
        $this->mailer->sendPendingEmail($submission);
        $adminBackendURL = $this->adminUrlGenerator->backendAdminSubmissionURL($submission);
        $this->bus->dispatch(new AdminLoggingAction('Submission for '.$submission->getName().' was created', $submission->getIdentity().' created submission request n°'.$submission->getId().' for organisation '.$submission->getName()."\n\nDetails of the submission can be found on ".$adminBackendURL."\n\nCheers,\n\nCharon, for the Frama.space team.", ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
    }
}
