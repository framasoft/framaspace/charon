<?php

namespace App\EventListener;

use App\Entity\Space;
use App\OfficeServerManager;
use App\PahekoServerManager;
use App\SpaceManager;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Psr\Log\LoggerInterface;

readonly class SpaceChangedNotifier
{
    public function __construct(private SpaceManager $spaceManager, private OfficeServerManager $officeServerManager, private PahekoServerManager $pahekoServerManager, private LoggerInterface $logger)
    {
    }

    public function postUpdate(PostUpdateEventArgs $eventArgs): void
    {
        if ($eventArgs->getObject() instanceof Space) {
            /** @var Space $space */
            $space = $eventArgs->getObject();
            $futureOfficeType = $space->getFutureOfficeType();
            if ($futureOfficeType !== $space->getOffice() && null !== $futureOfficeType) {
                $this->handlePostUpdateOfficeTypeChange($eventArgs, $space);
            }
            $changeset = $eventArgs->getObjectManager()->getUnitOfWork()->getEntityChangeset($space);
            if (isset($changeset['enabled']) && !$space->isEnabled()) {
                $this->handlePostUpdateDeactivation($space);
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $space = $eventArgs->getObject();
        if ($space instanceof Space) {
            if ($eventArgs->hasChangedField('futureOfficeType') && null !== $eventArgs->getNewValue('futureOfficeType')) {
                $this->handlePreUpdateOfficeTypeChange($eventArgs, $space);
            }
            if ($eventArgs->hasChangedField('enabled')) {
                if (true === $eventArgs->getNewValue('enabled')) {
                    $this->handlePreUpdateReactivation($eventArgs, $space);
                } elseif (false === $eventArgs->getNewValue('enabled')) {
                    $this->handlePreUpdateDeactivation($eventArgs, $space);
                }
            }
        }
    }

    private function handlePreUpdateOfficeTypeChange(PreUpdateEventArgs $eventArgs, Space $space): void
    {
        $this->logger->debug("Space office type is going to be changed, let's see if we need to change port information");
        $futureOfficeType = $eventArgs->getNewValue('futureOfficeType');
        $spaceDetails = $space->getDetails();

        // Backup old office ports and office servers
        $oldOfficePort = $spaceDetails['office_port'] ?? false;
        $oldOfficeServer = $spaceDetails['office_server'] ?? false;
        if (OfficeServerManager::isSharedOfficeType($space->getOffice()) && $this->hasSharedOfficeDetails($spaceDetails)) {
            $oldOfficePort = $spaceDetails['shared_office_port'];
            $oldOfficeServer = $spaceDetails['shared_office_server'];
        }

        // Always remove shared office details
        if ($this->hasSharedOfficeDetails($spaceDetails)) {
            $spaceDetails = $this->removeSharedOfficeDetails($spaceDetails);
        }

        // If we're going for a standalone
        if (!OfficeServerManager::isSharedOfficeType($futureOfficeType) && (!$this->hasStandaloneOfficeDetails($spaceDetails) || $this->isOfficeServerFull($spaceDetails))) {
            $this->logger->debug("The space office type is being set to standalone and we don't already have such details, or the previous office server is full, so let's set them");
            // If we have no standalone office details set
            [$officeServerName, $officePort] = $this->officeServerManager->getNewStandaloneOfficeDetails();
            $spaceDetails = [...$spaceDetails, 'office_port' => $officePort, 'office_server' => $officeServerName];
        } elseif (OfficeServerManager::isSharedOfficeType($futureOfficeType)) {
            $this->logger->debug("The space office type is being set to shared, let's reset new details for office");
            [$sharedOfficeServerName, $sharedOfficePort] = $this->officeServerManager->getNewSharedOfficeDetails($futureOfficeType);
            $spaceDetails = [...$spaceDetails, 'shared_office_port' => $sharedOfficePort, 'shared_office_server' => $sharedOfficeServerName];
        } else {
            $this->logger->debug('We changed from one standalone office type to another ('.$space->getOffice().' to '.$futureOfficeType.'), nothing to do');
        }

        if ($oldOfficeServer && $oldOfficePort) {
            $space->setDetails([...$spaceDetails, 'old_office_port' => $oldOfficePort, 'old_office_server' => $oldOfficeServer]);
        }

        $this->recomputeChanges($eventArgs, $space);
    }

    private function handlePostUpdateOfficeTypeChange(PostUpdateEventArgs $eventArgs, Space $space): void
    {
        $this->spaceManager->sendChangeOfficeJobs($space);

        $spaceDetails = $space->getDetails();
        // If we're not going to use a shared office type anymore
        if (!OfficeServerManager::isSharedOfficeType($space->getFutureOfficeType()) && OfficeServerManager::isSharedOfficeType($space->getOffice()) && $this->hasPreviousOfficeDetails($spaceDetails)) {
            $this->logger->debug("We're not using a shared office type anymore and we have shared office details, so let's check if we need to send a delete office server type");
            $this->officeServerManager->maybeSendDeleteOfficeServer($spaceDetails['old_office_server'], $spaceDetails['old_office_port']);
        }
    }

    private function handlePreUpdateReactivation(PreUpdateEventArgs $eventArgs, Space $space): void
    {
        $this->logger->debug("Space is going to be reactivated, let's recreate office details");
        $officeType = $space->getOffice();
        if (OfficeServerManager::isSharedOfficeType($officeType)) {
            [$sharedOfficeServerName, $sharedOfficePort] = $this->officeServerManager->getNewSharedOfficeDetails($officeType);
            $space->setDetails([...$space->getDetails(), 'shared_office_port' => $sharedOfficePort, 'shared_office_server' => $sharedOfficeServerName]);
        } else {
            [$officeServerName, $officePort] = $this->officeServerManager->getNewStandaloneOfficeDetails();
            $space->setDetails([...$space->getDetails(), 'office_port' => $officePort, 'office_server' => $officeServerName]);
        }
        $this->recomputeChanges($eventArgs, $space);
    }

    private function handlePreUpdateDeactivation(PreUpdateEventArgs $eventArgs, Space $space): void
    {
        $this->logger->debug("Space is going to be deactivated, let's move office details to old");
        $space->setDetails($this->moveOfficeDetailsToOld($space->getDetails(), $space->getOffice()));
        $this->recomputeChanges($eventArgs, $space);
    }

    private function handlePostUpdateDeactivation(Space $space): void
    {
        $details = $space->getDetails();
        if ($this->hasSharedOfficeDetails($details)) {
            $this->officeServerManager->maybeSendDeleteOfficeServer($details['shared_office_server'], $details['shared_office_port']);
        }
        // No need to delete Paheko servers
    }

    private function hasStandaloneOfficeDetails(array $details): bool
    {
        return ($details['office_port'] ?? false) && ($details['office_server'] ?? false);
    }

    private function hasSharedOfficeDetails(array $details): bool
    {
        return ($details['shared_office_port'] ?? false) && ($details['shared_office_server'] ?? false);
    }

    private function hasPreviousOfficeDetails(array $details): bool
    {
        return ($details['old_office_port'] ?? false) && ($details['old_office_server'] ?? false);
    }

    private function removeSharedOfficeDetails(array $spaceDetails): array
    {
        unset($spaceDetails['shared_office_server']);
        unset($spaceDetails['shared_office_port']);

        return $spaceDetails;
    }

    private function removeStandaloneOfficeDetails(array $spaceDetails): array
    {
        unset($spaceDetails['office_server']);
        unset($spaceDetails['office_port']);

        return $spaceDetails;
    }

    private function moveOfficeDetailsToOld(array $spaceDetails, ?int $officeType): array
    {
        if (OfficeServerManager::isSharedOfficeType($officeType) && $this->hasSharedOfficeDetails($spaceDetails)) {
            $spaceDetails['old_office_server'] = $spaceDetails['shared_office_server'];
            $spaceDetails['old_office_port'] = $spaceDetails['shared_office_port'];
        } else {
            $spaceDetails['old_office_server'] = $spaceDetails['office_server'];
            $spaceDetails['old_office_port'] = $spaceDetails['office_port'];
        }

        return $this->removeSharedOfficeDetails($this->removeStandaloneOfficeDetails($spaceDetails));
    }

    private function recomputeChanges(PreUpdateEventArgs $eventArgs, Space $space): void
    {
        $em = $eventArgs->getObjectManager();
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($space));
        $uow->recomputeSingleEntityChangeSet($meta, $space);
    }

    private function isOfficeServerFull(array $spaceDetails): bool
    {
        return $spaceDetails['office_server'] && $this->officeServerManager->isOfficeServerFull($spaceDetails['office_server']);
    }

    private function hasPahekoDetails(array $details): bool
    {
        return $details['paheko_shared_server'] ?? false;
    }
}
