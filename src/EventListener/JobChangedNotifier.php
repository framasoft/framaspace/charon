<?php

namespace App\EventListener;

use App\Entity\Job;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallAdminUserConfigurationAction;
use App\Message\NextcloudPostInstallLastStepAction;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\AdminNotifierInterface;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class JobChangedNotifier
{
    protected SpaceManager $spaceManager;
    private MessageBusInterface $bus;
    private AdminUrlGenerator $adminUrlGenerator;
    private LoggerInterface $logger;

    public function __construct(SpaceManager $spaceManager, MessageBusInterface $bus, AdminUrlGenerator $adminUrlGenerator, LoggerInterface $logger)
    {
        $this->spaceManager = $spaceManager;
        $this->bus = $bus;
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->logger = $logger;
    }

    public function postUpdate(Job $job): void
    {
        switch ($job->getStatus()) {
            case Job::JOB_STATUS_DONE:
                $this->handleJobDone($job);
                break;
            case Job::JOB_STATUS_ERROR:
                $this->handleJobError($job);
                break;
        }
    }

    private function handleJobDone(Job $job): void
    {
        $space = $job->getSpace();
        if (!$space) {
            $this->logger->error('Error handing job n°'.$job->getId().': Space not found');

            return;
        }
        switch ($job->getType()) {
            case Job::JOB_TYPE_CREATE_SPACE:
                $this->spaceManager->postCreateSpace($space);
                break;
            case Job::JOB_TYPE_DELETE_SPACE:
                $this->spaceManager->deleteSpace($space);
                break;
            case Job::JOB_TYPE_DISABLE_SPACE:
                $this->spaceManager->postDisableSpace($space);
                break;
            case Job::JOB_TYPE_REENABLE_SPACE:
                $this->spaceManager->postReenableSpace($space);
                break;
            case Job::JOB_TYPE_CHANGE_OFFICE:
                $this->spaceManager->postChangeOfficeType($space);
                break;
            case Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION:
                $jobBody = $job->getBody();
                try {
                    if ($jobBody) {
                        $jobData = json_decode($jobBody, true, 512, JSON_THROW_ON_ERROR);
                        if (isset($jobData['app_password'])) {
                            $this->bus->dispatch(new NextcloudPostInstallAdminUserConfigurationAction($space->getId(), $jobData['app_password']));
                            break;
                        }
                    }
                    $this->bus->dispatch(new AdminLoggingAction('Job n°'.$job->getId()." didn't return with an app password as body", 'Body: '.$jobBody, ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
                } catch (\JsonException) {
                    $this->bus->dispatch(new AdminLoggingAction('Job n°'.$job->getId()." didn't return with a valid JSON body", 'Body: '.$jobBody, ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
                }
                break;
            case Job::JOB_TYPE_POST_INSTALL_LAST_STEP:
                $this->bus->dispatch(new NextcloudPostInstallLastStepAction($space->getId()));
                break;
            case Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER:
                $this->bus->dispatch(new AdminLoggingAction('Shared office server successfully deleted', 'Body: '.$job->getBody(), ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
                break;
            case Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO:
                $this->bus->dispatch(new AdminLoggingAction('Paheko server successfully deployed for space '.$job->getSpace()->getDomain(), 'Body: '.$job->getBody(), ['type' => AdminNotifierInterface::TYPE_SUCCESS, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
                break;
        }
    }

    private function handleJobError(Job $job): void
    {
        $adminBackendURL = $this->adminUrlGenerator->backendAdminJobUrl($job);
        $jobType = match ($job->getType()) {
            Job::JOB_TYPE_CREATE_SPACE => 'creating a space',
            Job::JOB_TYPE_DELETE_SPACE => 'deleting a space',
            Job::JOB_TYPE_DISABLE_SPACE => 'disabling a space',
            Job::JOB_TYPE_REENABLE_SPACE => 'renabling a space',
            Job::JOB_TYPE_CHANGE_OFFICE => 'changing a space office type',
            Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER => 'deleting a shared office server',
            Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO => 'deploying Paheko for a space',
            default => 'performing an unknown action',
        };
        $this->bus->dispatch(new AdminLoggingAction('Job n°'.$job->getId().' returned an error while '.$jobType, "The returned body is shown below:\n\n".$job->getBody()."\n\nAccess the job page here: ".$adminBackendURL, ['priority' => AdminNotifierInterface::PRIORITY_ERROR, 'type' => AdminNotifierInterface::TYPE_ERROR]));
    }
}
