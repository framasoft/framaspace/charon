<?php

namespace App\EventListener;

use App\Entity\Job;
use App\Entity\Space;
use App\Exceptions\NoAvailablePahekoServer;
use App\Message\AdminLoggingAction;
use App\OfficeServerManager;
use App\Repository\SpaceRepository;
use App\Services\Nextcloud\Generator;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\PahekoDetailsService;
use App\SpaceManager;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class SpaceCreatedNotifier
{
    public function __construct(private SpaceRepository $spaceRepository, private OfficeServerManager $officeServerManager, private PahekoDetailsService $pahekoDetailsService, private Generator $generator, private ContainerBagInterface $params, private LoggerInterface $logger, private MessageBusInterface $bus)
    {
    }

    public function postPersist(PostPersistEventArgs $lifecycleEventArgs): void
    {
        if ($lifecycleEventArgs->getObject() instanceof Space) {
            /** @var Space $space */
            $space = $lifecycleEventArgs->getObject();
            $objectManager = $lifecycleEventArgs->getObjectManager();

            $spaceDetails = $space->getDetails();

            if (OfficeServerManager::isSharedOfficeType($space->getFutureOfficeType()) && $this->hasSharedOfficeDetails($spaceDetails)) {
                $this->logger->debug("We've set office type to a shared one, so office_port and office_server are in reality shared ones");
                $spaceDetails['office_port'] = $spaceDetails['shared_office_port'];
                $spaceDetails['office_server'] = $spaceDetails['shared_office_server'];
            }

            $job = (new Job())
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setSpace($space)
                ->setType(Job::JOB_TYPE_CREATE_SPACE)
                ->setData([...$spaceDetails, 'domain' => $space->getDomain(), 'office_type' => $space->getOffice(), 'organisation_name' => $space->getOrganizationName()]);

            $objectManager->persist($job);
            $objectManager->flush();

            $this->bus->dispatch(new AdminLoggingAction('Creating space '.$space->getDomain(), 'The space '.$space->getDomain().' has been approved and is scheduled for creation', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        }
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        if ($eventArgs->getObject() instanceof Space) {
            /** @var Space $space */
            $space = $eventArgs->getObject();
            $stunServer = $this->params->get('app.talk.stun_server');
            $turnServer = $this->params->get('app.talk.turn_server');
            $signalingServer = $this->params->get('app.talk.signaling_server');

            $details = [
                // Nextcloud details
                'nc_instanceid' => $this->generator->generateInstanceId(),
                'nc_admin_username' => SpaceManager::DEFAULT_ADMIN_USERNAME,
                'nc_admin_email' => $space->getContactEmail(),
                'nc_remote_admin_username' => SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME,
                'nc_remote_admin_password' => $this->generator->generateNextcloudPassword(),
                'nc_remote_admin_email' => SpaceManager::DEFAULT_REMOTE_ADMIN_EMAIL,
                'nc_passwordsalt' => $this->generator->generatePasswordSalt(),
                'nc_secret' => $this->generator->generateSecret(),

                // OnlyOffice details
                'oo_secret' => $this->generator->generatePasswordSaltForOnlyOffice(),

                // Talk
                'talk_stun_server' => $stunServer,
                'talk_turn_server' => $turnServer,
                'talk_signaling_server' => $signalingServer,

                // High performance notify push server
                'nc_hp_backend' => $this->spaceRepository->findBiggestNcHpBackendPort() + 1,
            ];

            // Paheko
            if (true === $this->params->get('app.office.paheko.enabled')) {
                try {
                    $details = array_merge($details, $this->pahekoDetailsService->getDetails($space->getDomain()));
                } catch (NoAvailablePahekoServer) {
                    $this->logger->error('Paheko enabled but no Paheko server available for deployment, skipping adding Paheko details to space');
                }
            }

            // Shared office details
            if (OfficeServerManager::isSharedOfficeType($space->getOffice())) {
                [$sharedOfficeServerName, $sharedOfficePort] = $this->officeServerManager->getNewSharedOfficeDetails($space->getOffice());
                $details['shared_office_port'] = $sharedOfficePort;
                $details['shared_office_server'] = $sharedOfficeServerName;
            } else {
                // Common office details
                [$officeServerName, $officePort] = $this->officeServerManager->getNewStandaloneOfficeDetails();
                $details['office_port'] = $officePort;
                $details['office_server'] = $officeServerName;
            }

            $this->logger->info('Creating space '.$space->getDomain());

            $space->setDetails($details);
        }
    }

    private function hasSharedOfficeDetails(array $details): bool
    {
        return ($details['shared_office_port'] ?? false) && ($details['shared_office_server'] ?? false);
    }
}
