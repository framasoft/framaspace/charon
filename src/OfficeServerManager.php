<?php

namespace App;

use App\Entity\Job;
use App\Entity\OfficeServer;
use App\Entity\Space;
use App\Exceptions\NoAvailableOfficeServer;
use App\Message\AdminLoggingAction;
use App\Repository\OfficeServerRepository;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class OfficeServerManager
{
    private SpaceRepository $spaceRepository;
    private OfficeServerRepository $officeServerRepository;
    private EntityManagerInterface $entityManager;
    private MessageBusInterface $bus;

    public function __construct(SpaceRepository $spaceRepository, OfficeServerRepository $officeServerRepository, EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        $this->spaceRepository = $spaceRepository;
        $this->officeServerRepository = $officeServerRepository;
        $this->entityManager = $entityManager;
        $this->bus = $bus;
    }

    public function getNewStandaloneOfficeDetails(): array
    {
        $officeServer = $this->officeServerRepository->getAvailableForNewSpacesOfficeServer(OfficeServer::OFFICE_TYPE_STANDALONE);
        $port = $this->spaceRepository->findBiggestPortForOfficeServer($officeServer->getHostname(), $officeServer->getMinimalPort(), $officeServer->getMaximalPort()) + 1;

        return [$officeServer->getHostname(), $port];
    }

    /**
     * @throws NoAvailableOfficeServer
     */
    public function getNewSharedOfficeDetails(int $officeType): array
    {
        $officeServer = $this->officeServerRepository->getAvailableForNewSpacesOfficeServer($officeType);
        if (!$officeServer) {
            throw new NoAvailableOfficeServer($officeType);
        }
        $port = $this->spaceRepository->findAvailablePortForSharedOfficeServer($officeServer);

        return [$officeServer->getHostname(), $port];
    }

    public static function isSharedOfficeType(?int $officeType): bool
    {
        return in_array($officeType, Space::SPACE_OFFICE_SHARED_TYPES, true);
    }

    public function maybeSendDeleteOfficeServer(string $sharedOfficeServer, int $sharedOfficePort): void
    {
        if ($this->spaceRepository->countSharedOfficeSpacesBySharedOfficeNameAndPort($sharedOfficeServer, $sharedOfficePort) <= 0) {
            $job = (new Job())
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setType(Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER)
                ->setData(['shared_office_port' => $sharedOfficePort, 'shared_office_server' => $sharedOfficeServer]);

            $this->entityManager->persist($job);
            $this->entityManager->flush();

            $this->bus->dispatch(new AdminLoggingAction('Removing shared office server '.$sharedOfficeServer.':'.$sharedOfficePort, 'When removing or disabling a space, we found out no space still uses shared office server '.$sharedOfficeServer.':'.$sharedOfficePort.", so we're removing it", ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]));
        }
    }

    public function isOfficeServerFull(string $officeServer): bool
    {
        return null !== $this->officeServerRepository->findOneBy(['hostname' => $officeServer, 'isFull' => true]);
    }
}
