<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\SpaceManager;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ServerInfoClient extends OCSClient
{
    private const ENDPOINT = '/ocs/v2.php/apps/serverinfo/api/v1/info?format=json';

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws \JsonException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NextcloudRemoteOCSException
     */
    public function getStatistics(Space $space): array
    {
        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT;

        $remoteAdminUser = $space->getDetails()['nc_remote_admin_username'] ?? SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME;
        $remoteAdminPassword = $space->getDetails()['nc_remote_admin_password'];
        $basicAuth = [$remoteAdminUser, $remoteAdminPassword];

        return $this->doJSONGetRequest($space, $endpoint, $basicAuth, $remoteAdminUser)['ocs']['data'];
    }
}
