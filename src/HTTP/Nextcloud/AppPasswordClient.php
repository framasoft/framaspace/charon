<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AppPasswordClient extends OCSClient
{
    private const ENDPOINT = '/ocs/v2.php/core/apppassword';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        parent::__construct($nextcloudProvisioningAPI, $logger);
    }

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws NextcloudRemoteXMLException
     * @throws NextcloudRemoteOCSException
     */
    public function deleteAppPassword(Space $space, array $basicAuth): void
    {
        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT;

        $adminUser = $space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME;

        [$statusCode, $message] = $this->doRequest($space, $endpoint, $basicAuth, $adminUser, null, null, 'DELETE');

        $title = 'Successfully deleted app password for '.$adminUser.' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $message,
        ]);
    }
}
