<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class OCSClient
{
    protected HttpClientInterface $client;
    protected LoggerInterface $logger;

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        $this->client = $nextcloudProvisioningAPI;
        $this->logger = $logger;
    }

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws NextcloudRemoteXMLException
     * @throws NextcloudRemoteOCSException
     */
    protected function doRequest(Space $space, string $endpoint, array $basicAuth, string $userId, ?array $body, ?array $json = null, string $method = 'POST'): array
    {
        $this->logger->debug('Sending OCS API request to '.$method.' '.$endpoint);
        $this->logger->debug('Request body: ', ['body' => $body]);
        $this->logger->debug('Basic auth details: ', $basicAuth);
        try {
            $response = $this->client->request($method, $endpoint, [
                'body' => $body,
                'json' => $json,
                'auth_basic' => $basicAuth,
                'headers' => [
                    'OCS-APIRequest' => 'true',
                ],
            ]);
            $statusCode = $response->getStatusCode();
            $content = $response->getContent();
        } catch (ClientException $e) {
            throw new NextcloudRemoteHTTPException($e->getResponse()->getStatusCode(), $userId, $space->getDomain(), $e->getResponse()->getContent(false));
        }
        if (Response::HTTP_OK !== $statusCode) {
            throw new NextcloudRemoteHTTPException($statusCode, $userId, $space->getDomain(), $content);
        }
        $data = simplexml_load_string($content);
        if (false === $data) {
            throw new NextcloudRemoteXMLException($userId, $space->getDomain(), $content);
        }
        $statusCode = $data->meta->statuscode->__toString();
        $message = $data->meta->message->__toString();
        if (!in_array((int) $statusCode, [100, 200])) {
            throw new NextcloudRemoteOCSException($statusCode, $message, $userId, $space->getDomain(), $content);
        }

        return [$statusCode, $message];
    }

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws \JsonException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NextcloudRemoteOCSException
     */
    protected function doJSONGetRequest(Space $space, string $endpoint, array $basicAuth, string $userId): array
    {
        $this->logger->debug('Sending OCS API GET request to '.$endpoint);
        $this->logger->debug('Basic auth details: ', $basicAuth);

        try {
            $response = $this->client->request('GET', $endpoint, [
                'auth_basic' => $basicAuth,
                'headers' => [
                    'OCS-APIRequest' => 'true',
                ],
            ]);
            $statusCode = $response->getStatusCode();
            $content = $response->getContent();
        } catch (ClientException $e) {
            throw new NextcloudRemoteHTTPException($e->getResponse()->getStatusCode(), $userId, $space->getDomain(), $e->getResponse()->getContent(false));
        }
        if (Response::HTTP_OK !== $statusCode) {
            throw new NextcloudRemoteHTTPException($statusCode, $userId, $space->getDomain(), $content);
        }
        $data = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $statusCode = $data['ocs']['meta']['statuscode'];
        $message = $data['ocs']['meta']['message'];
        if (!in_array((int) $statusCode, [100, 200])) {
            throw new NextcloudRemoteOCSException($statusCode, $message, $userId, $space->getDomain(), $content);
        }

        return $data;
    }
}
