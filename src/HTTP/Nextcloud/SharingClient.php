<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SharingClient extends OCSClient
{
    private const ENDPOINT = '/ocs/v2.php/apps/files_sharing/api/v1/shares';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        parent::__construct($nextcloudProvisioningAPI, $logger);
    }

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws NextcloudRemoteXMLException
     * @throws NextcloudRemoteOCSException
     */
    public function share(Space $space, array $basicAuth, string $path, string $recipient, int $shareType, int $permissions): void
    {
        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT;

        $adminUser = $space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME;

        $body = [
            'path' => $path,
            'shareType' => $shareType,
            'shareWith' => $recipient,
            'permissions' => $permissions,
        ];

        [$statusCode, $message] = $this->doRequest($space, $endpoint, $basicAuth, $adminUser, $body);

        $title = 'Successfully sent share for  on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $message,
        ]);
    }
}
