<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Space;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebDAVClient
{
    protected HttpClientInterface $client;
    protected LoggerInterface $logger;

    public const ENDPOINT = '/remote.php/dav/files/';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        $this->client = $nextcloudProvisioningAPI;
        $this->logger = $logger;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function createFolder(Space $space, string $appPassword, string $folderName): void
    {
        $adminUser = $space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME;
        $basicAuth = [$adminUser, $appPassword];

        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT.($space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME).'/'.$folderName;

        $response = $this->client->request('MKCOL', $endpoint, [
            'auth_basic' => $basicAuth,
        ]);
        $statusCode = $response->getStatusCode();
        $content = $response->getContent();
        $title = 'Successfully created folder '.$folderName.' for user '.$adminUser.' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $content,
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function createFile(Space $space, string $appPassword, string $path, string $content): void
    {
        $adminUser = $space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME;
        $basicAuth = [$adminUser, $appPassword];

        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT.($space->getDetails()['nc_admin_username'] ?? SpaceManager::DEFAULT_ADMIN_USERNAME).'/'.$path;

        $response = $this->client->request('PUT', $endpoint, [
            'auth_basic' => $basicAuth,
            'body' => $content,
        ]);
        $statusCode = $response->getStatusCode();
        $content = $response->getContent();
        $title = 'Successfully created folder '.$path.' for user '.$adminUser.' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $content,
        ]);
    }
}
