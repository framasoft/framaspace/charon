<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Announcement;
use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AnnouncementClient extends OCSClient
{
    private const ENDPOINT = '/ocs/v2.php/apps/announcementcenter/api/v1/announcements';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        parent::__construct($nextcloudProvisioningAPI, $logger);
    }

    /**
     * @throws NextcloudRemoteHTTPException|NextcloudRemoteXMLException|NextcloudRemoteOCSException
     */
    public function addAnnouncement(Space $space, Announcement $announcement): void
    {
        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT;

        $remoteAdminUser = $space->getDetails()['nc_remote_admin_username'] ?? SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME;
        $remoteAdminPassword = $space->getDetails()['nc_remote_admin_password'];
        $basicAuth = [$remoteAdminUser, $remoteAdminPassword];

        $announcementMessage = $announcement->getMessage();
        if (!$announcementMessage) {
            $this->logger->warning('Announcement '.$announcement->getId().' does not have any message, skipping sending announcement.');

            return;
        }

        $body = [
            'subject' => $announcement->getSubject(),
            'message' => $announcementMessage,
            'groups' => $announcement->isOnlyAdmins() ? ['admin'] : [],
            'activities' => $announcement->isCreateActivities(),
            'notifications' => $announcement->isCreateNotifications(),
            'emails' => $announcement->isSendEmails(),
            'comments' => $announcement->isAllowComments(),
        ];

        [$statusCode, $message] = $this->doRequest($space, $endpoint, $basicAuth, $remoteAdminUser, null, $body);

        $title = 'Successfully sent announcement '.$announcement->getSubject().' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $message,
        ]);
    }
}
