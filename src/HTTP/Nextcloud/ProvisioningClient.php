<?php

namespace App\HTTP\Nextcloud;

use App\Entity\Nextcloud\Group;
use App\Entity\Nextcloud\User;
use App\Entity\Space;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Exceptions\Nextcloud\NextcloudRemoteXMLException;
use App\SpaceManager;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ProvisioningClient extends OCSClient
{
    private const ENDPOINT = '/ocs/v1.php/cloud';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI, LoggerInterface $logger)
    {
        parent::__construct($nextcloudProvisioningAPI, $logger);
    }

    /**
     * @throws NextcloudRemoteOCSException
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NextcloudRemoteXMLException
     * @throws NextcloudRemoteHTTPException
     */
    public function addUser(Space $space, User $nextcloudUser): void
    {
        $remoteAdminUser = $space->getDetails()['nc_remote_admin_username'] ?? SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME;
        $remoteAdminPassword = $space->getDetails()['nc_remote_admin_password'];
        $basicAuth = [$remoteAdminUser, $remoteAdminPassword];

        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT.'/users';
        $body = [
            'userid' => $nextcloudUser->getUserId(),
            'displayName' => $nextcloudUser->getDisplayName(),
            'email' => $nextcloudUser->getEmail(),
            'groups' => $nextcloudUser->getGroupIds(),
            'subadmin' => $nextcloudUser->getSubadmin(),
            'language' => $nextcloudUser->getLanguage(),
            'quota' => $nextcloudUser->getQuota(),
        ];
        [$statusCode, $message] = $this->doRequest($space, $endpoint, $basicAuth, $nextcloudUser->getUserId(), $body);

        $title = 'Successfully created user '.$nextcloudUser->getUserId().' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $message,
        ]);
    }

    /**
     * @throws NextcloudRemoteHTTPException
     * @throws NextcloudRemoteXMLException
     * @throws NextcloudRemoteOCSException
     */
    public function addGroup(Space $space, Group $nextcloudGroup): void
    {
        $remoteAdminUser = $space->getDetails()['nc_remote_admin_username'] ?? SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME;
        $remoteAdminPassword = $space->getDetails()['nc_remote_admin_password'];
        $basicAuth = [$remoteAdminUser, $remoteAdminPassword];
        $endpoint = 'https://'.$space->getDomain().'.frama.space'.self::ENDPOINT.'/groups';

        [$statusCode, $message] = $this->doRequest($space, $endpoint, $basicAuth, $remoteAdminUser, [
            'groupid' => $nextcloudGroup->getGroupId(),
        ]);

        $title = 'Successfully created group '.$nextcloudGroup->getGroupId().' on '.$space->getDomain();
        $this->logger->info($title, [
            'statuscode' => $statusCode,
            'message' => $message,
        ]);
    }
}
