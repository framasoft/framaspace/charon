<?php

namespace App\Components\Admin;

use Symfony\UX\Chartjs\Model\Chart;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(template: 'admin/dashboard/statistics_card.html.twig')]
class StatisticsCardComponent
{
    public string $title;
    public string $status = 'primary';
    public Chart $chart;
}
