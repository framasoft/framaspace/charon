# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices.html#use-parameters-for-application-configuration
parameters:
    app.ldap.host: '%env(LDAP_HOST)%'
    app.ldap.port: '%env(LDAP_PORT)%'
    app.ldap.dn_string: '%env(LDAP_DN_STRING)%'
    app.ldap.base_dn: '%env(LDAP_BASE_DN)%'
    app.ldap.search_dn: '%env(LDAP_SEARCH_DN)%'
    app.ldap.search_password: '%env(LDAP_SEARCH_PASSWORD)%'
    app.ldap.filter: '%env(LDAP_FILTER)%'
    app.siret.insee_key: '%env(INSEE_API_KEY)%'
    app.siret.insee_secret: '%env(INSEE_API_SECRET)%'
    app.logging.email_address: '%env(LOGGING_EMAIL_ADDRESS)%'
    app.login.rate_limiter_secret: '%env(LOGIN_RATE_LIMITER_SECRET)%'
    app.discourse.endpoint: '%env(DISCOURSE_API_ENDPOINT)%'
    app.discourse.username: '%env(DISCOURSE_API_USERNAME)%'
    app.discourse.api_key: '%env(DISCOURSE_API_KEY)%'
    app.discourse.category: '%env(DISCOURSE_API_CATEGORY)%'
    app.email.users: '%env(bool:EMAIL_USERS)%'
    app.submissions.opened: '%env(bool:SUBMISSIONS_OPENED)%'
    app.office.default_server: '%env(OFFICE_DEFAULT_SERVER)%'
    app.office.allow_change_type: '%env(bool:ALLOW_CHANGE_OFFICE_TYPE)%'
    app.office.paheko.enabled: '%env(bool:PAHEKO_ENABLED)%'
    app.talk.stun_server: '%env(TALK_STUN_SERVER)%'
    app.talk.turn_server: '%env(TALK_TURN_SERVER)%'
    app.talk.signaling_server: '%env(TALK_SIGNALING_SERVER)%'
    app.space.default_days_before_purge: 30
    app.space.days_before_purge: '%env(int:default:app.space.default_days_before_purge:SPACE_DAYS_BEFORE_PURGE)%'
    app.submission.default_days_before_purge: 7
    app.submission.days_before_purge: '%env(int:default:app.submission.default_days_before_purge:SUBMISSION_DAYS_BEFORE_PURGE)%'
    app.submission.default_days_before_automatic_rejection: 30
    app.submission.days_before_automatic_rejection: '%env(int:default:app.submission.default_days_before_purge:SUBMISSION_DAYS_BEFORE_AUTOMATIC_REJECTION)%'
    app.email.from.email: '%env(MAIL_FROM_EMAIL)%'
    app.email.from.name: '%env(MAIL_FROM_NAME)%'
    app.notifications.gotify.server: '%env(NOTIFICATIONS_GOTIFY_SERVER)%'
    app.notifications.gotify.tokens: '%env(json:NOTIFICATIONS_GOTIFY_TOKENS)%'
    app.notifications.gotify.min_level: '%env(int:NOTIFICATIONS_GOTIFY_MIN_LEVEL)%'
    app.notifications.mailer.min_level: '%env(int:NOTIFICATIONS_MAILER_MIN_LEVEL)%'
    app.nextcloud.common_folder_name: '%env(NEXTCLOUD_COMMON_FOLDER_NAME)%'
    app.nextcloud.everyone_group_name: '%env(NEXTCLOUD_EVERYONE_GROUP_NAME)%'
    app.nextcloud.common_folder_readme_path: '%env(NEXTCLOUD_COMMON_FOLDER_README_PATH)%'
    app.nextcloud.storage.default_quota_gb: 40
    app.nextcloud.storage.quota_gb: '%env(int:default:app.nextcloud.storage.default_quota_gb:NEXTCLOUD_STORAGE_QUOTA_GB)%'

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/'
        exclude:
            - '../src/DependencyInjection/'
            - '../src/Entity/'
            - '../src/Entity/Nextcloud/'
            - '../src/Message/'
            - '../src/Kernel.php'
            - '../src/Exceptions/'
            - '../src/Doctrine/'

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones
    Symfony\Component\Ldap\Ldap:
        arguments: [ '@Symfony\Component\Ldap\Adapter\ExtLdap\Adapter' ]
        tags:
            - ldap
    Symfony\Component\Ldap\Adapter\ExtLdap\Adapter:
        arguments:
            - host: '%app.ldap.host%'
              port: '%app.ldap.port%'
              encryption: ssl
#              options:
#                  protocol_version: 3
#                  referrals: false

    App\EventListener\JobChangedNotifier:
        tags:
            -
              name: 'doctrine.orm.entity_listener'
              event: 'postUpdate'
              entity: 'App\Entity\Job'
#
    App\EventListener\SpaceChangedNotifier:
        tags:
            - name: doctrine.event_listener
              event: postUpdate
            - name: doctrine.event_listener
              event: preUpdate
#
    App\EventListener\SpaceCreatedNotifier:
        tags:
            - name: doctrine.event_listener
              event: postPersist
            - name: doctrine.event_listener
              event: prePersist

    App\EventListener\SpaceRemovedNotifier:
        tags:
            - name: doctrine.event_listener
              event: postRemove

    App\EventListener\AnnouncementCreatedNotifier:
        tags:
            - name: doctrine.event_listener
              event: postPersist

    App\Services\Notifications\Gotify:
        tags: [ 'app.notification' ]

    App\Services\Notifications\Mailer:
        tags: ['app.notification']

    App\MessageHandler\AdminLoggingHandler:
        arguments:
            - !tagged_iterator app.notification

    app.login_rate_limiter:
        class: Symfony\Component\Security\Http\RateLimiter\DefaultLoginRateLimiter
        arguments:
            # globalFactory is the limiter for IP
            $globalFactory: '@limiter.ip_login'
            # localFactory is the limiter for username+IP
            $localFactory: '@limiter.username_ip_login'
            $secret: '%app.login.rate_limiter_secret%'
