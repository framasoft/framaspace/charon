<?php

namespace App\Tests\Form\Type;

use App\Form\Type\UserProvisioningImportType;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validation;
use Zenstruck\Foundry\Test\Factories;

class UserProvisioningImportTypeTest extends TypeTestCase
{
    use Factories;

    protected function getExtensions(): array
    {
        // create a type instance with the mocked dependencies
        $type = new UserProvisioningImportType();

        return [
            // register the type instances with the PreloadedExtension
            new PreloadedExtension([$type], []),
        ];
    }

    #[DataProvider(methodName: 'dataForTestSubmitValidData')]
    public function testSubmitValidData(array $formData, bool $valid)
    {
        $validator = Validation::createValidator();

        $formFactory = Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->addExtensions($this->getExtensions())
        ->getFormFactory();

        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $formFactory->create(UserProvisioningImportType::class, []);

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        $this->assertTrue($form->isSubmitted());
        $this->assertEquals($valid, $form->isValid());
    }

    public static function dataForTestSubmitValidData(): array
    {
        file_put_contents('/tmp/user-provisioning-import-file.csv', 'some data');

        return [
            [
                [
                    'file' => new UploadedFile('/tmp/user-provisioning-import-file.csv', 'user-provisioning-import-file.csv'),
                ],
                true,
            ],
        ];
    }
}
