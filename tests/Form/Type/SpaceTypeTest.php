<?php

namespace App\Tests\Form\Type;

use App\Entity\Space;
use App\Form\Type\SpaceType;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;
use Twig\Environment;
use Zenstruck\Foundry\Test\Factories;

class SpaceTypeTest extends TypeTestCase
{
    use Factories;

    private Environment $twig;

    protected function setUp(): void
    {
        // mock any dependencies
        $this->twig = $this->createMock(Environment::class);

        parent::setUp();
    }

    protected function getExtensions(): array
    {
        // create a type instance with the mocked dependencies
        $type = new SpaceType($this->twig, true);

        return [
            // register the type instances with the PreloadedExtension
            new PreloadedExtension([$type], []),
        ];
    }

    #[DataProvider(methodName: 'dataForTestSubmitValidData')]
    public function testSubmitValidData(array $formData, bool $valid)
    {
        $validator = Validation::createValidator();

        $formFactory = Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->addExtensions($this->getExtensions())
        ->getFormFactory();

        $now = new \DateTimeImmutable();

        $model = new Space();
        $model->setCreatedAt($now);
        $model->setUpdatedAt($now);

        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $formFactory->create(SpaceType::class, $model);

        $expected = new Space();
        $expected->setContactName($formData['contactName']);
        $expected->setContactEmail($formData['contactEmail']);
        $expected->setOrganizationName($formData['organizationName']);
        $expected->setFutureOfficeType($formData['futureOfficeType']);
        $expected->setCreatedAt($now);
        $expected->setUpdatedAt($now);

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        $this->assertTrue($form->isSubmitted());
        $this->assertEquals($valid, $form->isValid());
        if ($valid) {
            $this->assertEquals($expected, $model);
        } else {
            $this->assertNotEquals($expected, $model);
        }
    }

    public static function dataForTestSubmitValidData(): array
    {
        return [
            [
                [
                    'contactName' => 'hello',
                    'contactEmail' => 'someone@somewhere.com',
                    'organizationName' => 'some org',
                    'futureOfficeType' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                ],
                true,
            ],
            // We don't test invalid emails as constraints can't be tested here
            // https://symfony.com/doc/current/form/unit_testing.html
            [
                [
                    'contactName' => 'hello',
                    'contactEmail' => 'someone@somewhere.com',
                    'organizationName' => 'some org',
                    'futureOfficeType' => 256,
                ],
                false,
            ],
        ];
    }
}
