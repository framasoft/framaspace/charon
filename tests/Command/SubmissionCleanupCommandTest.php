<?php

namespace App\Tests\Command;

use App\Entity\Submission;
use App\Factory\SubmissionFactory;
use App\Repository\SubmissionRepository;
use App\Services\EntityManagerInterfaceProxy;
use App\Services\Notifications\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SubmissionCleanupCommandTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testCommandWithIncorrectNumberOfDays(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:submission:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--days' => '-1',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] The number of days must be positive or zero.', $output);
    }

    public function testCommandWithDryRun(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $SubmissionRepository = $this->createMock(SubmissionRepository::class);
        $SubmissionRepository->expects($this->once())->method('countRejectedSubmissionsToRemove')->with(5)->willReturn(7);
        $SubmissionRepository->expects($this->never())->method('getRejectedSubmissionsToRemove');
        $container->set(SubmissionRepository::class, $SubmissionRepository);

        $command = $application->find('app:submission:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--days' => '5',
            '--dry-run' => '',
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Using custom days after deactivation value', $output);
        $this->assertStringContainsString('[OK] Removed 7 submissions.', $output);
    }

    public function testCommand(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $submissions = SubmissionFactory::createMany(15, ['status' => Submission::SUBMISSION_STATUS_REJECTED, 'updatedAt' => new \DateTimeImmutable()]);

        $SubmissionRepository = $this->createMock(SubmissionRepository::class);
        $SubmissionRepository->expects($this->never())->method('countRejectedSubmissionsToRemove')->with(5);
        $SubmissionRepository->expects($this->once())->method('getRejectedSubmissionsToRemove')->with(7, 10)->willReturn(array_slice(array_map(fn ($submission) => $submission->_real(), $submissions), 0, 10));
        $container->set(SubmissionRepository::class, $SubmissionRepository);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->exactly(10))->method('remove')->with($this->isInstanceOf(Submission::class));
        $entityManager->expects($this->once())->method('flush');

        $entityManagerProxy = $this->createMock(EntityManagerInterfaceProxy::class);
        $entityManagerProxy->expects($this->once())->method('getEntityManager')->willReturn($entityManager);
        $container->set(EntityManagerInterfaceProxy::class, $entityManagerProxy);

        $mailer = $this->createMock(Mailer::class);
        $mailer->expects($this->exactly(10))->method('sendSubmissionDeletionConfirmation')->with($this->isInstanceOf(Submission::class));
        $container->set(Mailer::class, $mailer);

        $command = $application->find('app:submission:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--max' => 10,
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[OK] Removed 10 submissions.', $output);
    }
}
