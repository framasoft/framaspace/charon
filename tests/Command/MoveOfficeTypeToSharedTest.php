<?php

namespace App\Tests\Command;

use App\Entity\Space;
use App\Factory\SpaceFactory;
use App\Repository\SpaceRepository;
use App\Services\EntityManagerInterfaceProxy;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class MoveOfficeTypeToSharedTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testCommandWithDryRun(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $spaces = SpaceFactory::createMany(12, ['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'status' => Space::SPACE_STATUS_CREATED]);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $spaceRepository = $this->createMock(SpaceRepository::class);
        $spaceRepository->expects($this->once())->method('findBy')->with(['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'futureOfficeType' => null], null, 50)->willReturn(array_map(fn ($space) => $space->_real(), $spaces));
        $entityManager->expects($this->never())->method('persist');
        $entityManager->expects($this->never())->method('remove');
        $entityManager->expects($this->never())->method('flush');

        $container->set(SpaceRepository::class, $spaceRepository);

        $command = $application->find('app:maintenance:move-office-type-to-shared');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--dry-run' => '',
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Moved 12 spaces to collabora office', $output);
    }

    public function testCommand(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $spaces = SpaceFactory::createMany(12, ['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'status' => Space::SPACE_STATUS_CREATED]);

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $spaceRepository->expects($this->once())->method('findBy')->with(['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'futureOfficeType' => null], null, 50)->willReturn(array_map(fn ($space) => $space->_real(), $spaces));
        $container->set(SpaceRepository::class, $spaceRepository);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->exactly(12))->method('persist')->with($this->isInstanceOf(Space::class));
        $entityManager->expects($this->exactly(12))->method('flush')->with();

        $entityManagerProxy = $this->createMock(EntityManagerInterfaceProxy::class);
        $entityManagerProxy->expects($this->once())->method('getEntityManager')->willReturn($entityManager);
        $container->set(EntityManagerInterfaceProxy::class, $entityManagerProxy);

        $command = $application->find('app:maintenance:move-office-type-to-shared');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[OK] Moved 12 spaces to collabora office.', $output);
    }
}
