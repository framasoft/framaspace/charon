<?php

namespace App\Tests\Command;

use App\Entity\Job;
use App\Entity\Space;
use App\Factory\SpaceFactory;
use App\Repository\SpaceRepository;
use App\Services\EntityManagerInterfaceProxy;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceCleanupCommandTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testCommandWithIncorrectNumberOfDays(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:space:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--days' => '-1',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] The number of days must be positive or zero.', $output);
    }

    public function testCommandWithDryRun(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $spaceRepository->expects($this->once())->method('countSpacesToClean')->with(5)->willReturn(7);
        $spaceRepository->expects($this->never())->method('getSpacesToClean');
        $container->set(SpaceRepository::class, $spaceRepository);

        $command = $application->find('app:space:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--days' => '5',
            '--dry-run' => '',
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Using custom days after deactivation value', $output);
        $this->assertStringContainsString('[OK] Started "7" jobs to completely delete spaces.', $output);
    }

    public function testCommand(): void
    {
        $kernel = self::bootKernel();
        $container = static::getContainer();
        $application = new Application($kernel);

        $spaces = SpaceFactory::createMany(15, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => false, 'deletedAt' => new \DateTimeImmutable()]);

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $spaceRepository->expects($this->never())->method('countSpacesToClean')->with(5);
        $spaceRepository->expects($this->once())->method('getSpacesToClean')->with(30, 10)->willReturn(array_slice(array_map(fn ($space) => $space->_real(), $spaces), 0, 10));
        $container->set(SpaceRepository::class, $spaceRepository);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->exactly(10))->method('persist')->with($this->isInstanceOf(Job::class));
        $entityManager->expects($this->exactly(10))->method('flush')->with();

        $entityManagerProxy = $this->createMock(EntityManagerInterfaceProxy::class);
        $entityManagerProxy->expects($this->once())->method('getEntityManager')->willReturn($entityManager);
        $container->set(EntityManagerInterfaceProxy::class, $entityManagerProxy);

        $command = $application->find('app:space:cleanup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--max' => 10,
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[OK] Started "10" jobs to completely delete spaces.', $output);
    }
}
