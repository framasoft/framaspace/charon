<?php

namespace App\Tests\Repository;

use App\Repository\JobRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class JobRepositoryTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testFindAllQueryWithFilterQueryBuilder(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $jobRepository = $container->get(JobRepository::class);

        $this->assertInstanceOf(QueryBuilder::class, $jobRepository->findAllQueryWithFilterQueryBuilder());
    }
}
