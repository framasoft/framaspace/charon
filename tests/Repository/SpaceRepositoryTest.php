<?php

namespace App\Tests\Repository;

use App\Entity\Space;
use App\Factory\SpaceFactory;
use App\Repository\SpaceRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceRepositoryTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testFindBiggestNcHpBackendPort(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEquals(10000, $spaceRepository->findBiggestNcHpBackendPort(), 'Biggest NCHPBackend port is not what expected');

        SpaceFactory::createOne(['details' => ['nc_hp_backend' => 42000, 'office_port' => 12500, 'office_server' => 'office-2']]);

        $this->assertEquals(42000, $spaceRepository->findBiggestNcHpBackendPort(), 'Biggest NCHPBackend port is not what expected');
    }

    public function testFindBiggestOfficePort(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEquals(10000, $spaceRepository->findBiggestPortForOfficeServer('office-1', 10000, 19999), 'Biggest NCHPBackend port is not what expected');

        SpaceFactory::createOne(['details' => ['nc_hp_backend' => 42000, 'office_port' => 12500, 'office_server' => 'office-2']]);

        $this->assertEquals(10000, $spaceRepository->findBiggestPortForOfficeServer('office-1', 10000, 19999), 'Biggest NCHPBackend port is not what expected');

        $this->assertEquals(12500, $spaceRepository->findBiggestPortForOfficeServer('office-2', 10000, 19999), 'Biggest NCHPBackend port is not what expected');
    }

    public function testCountSpacesToClean(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);
        $this->assertEquals(0, $spaceRepository->countSpacesToClean(5));

        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => false, 'deletedAt' => (new \DateTimeImmutable())->sub(new \DateInterval('P40D'))]);
        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => false, 'deletedAt' => (new \DateTimeImmutable())]);
        $this->assertEquals(5, $spaceRepository->countSpacesToClean(5));
    }

    public function testGetSpacesToClean(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);
        $this->assertEmpty($spaceRepository->getSpacesToClean(5));

        $oldObsoleteSpaces = SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => false, 'deletedAt' => (new \DateTimeImmutable())->sub(new \DateInterval('P40D'))]);
        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => false, 'deletedAt' => (new \DateTimeImmutable())]);
        $this->assertCount(5, $spaceRepository->getSpacesToClean(5));
        $this->assertSameSize($oldObsoleteSpaces, $spaceRepository->getSpacesToClean(5));
        $oldObsoleteSpacesDomains = array_map(fn ($obsoleteSpace) => $obsoleteSpace->getDomain(), $oldObsoleteSpaces);
        $spacesToCleanDomains = array_map(fn (Space $spaceToClean) => $spaceToClean->getDomain(), $spaceRepository->getSpacesToClean(5));
        sort($oldObsoleteSpacesDomains);
        sort($spacesToCleanDomains);
        $this->assertEquals($oldObsoleteSpacesDomains, $spacesToCleanDomains);
    }

    public function testCountNewSpacesByMonth(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEmpty($spaceRepository->countNewSpacesByMonths());

        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true]);
        SpaceFactory::new(['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true])
        ->with(function () {
            return ['createdAt' => \DateTimeImmutable::createFromMutable(new \DateTime('2020-07-06'))];
        })
        /*
         * createdAt is always instantiated to current date, so we need to override it
         */
        ->afterPersist(function (Space $space, array $attributes) {
            $space->setCreatedAt($attributes['createdAt']);
        })
        ->create();
        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_PENDING, 'enabled' => false]);

        $results = $spaceRepository->countNewSpacesByMonths();

        $this->assertEquals(1, array_values(array_filter($results, fn ($result) => '2020-07-01 00:00:00' === $result['createdAtMonth']))[0]['spaceCount']);
        $this->assertEquals(5, array_values(array_filter($results, fn ($result) => $result['createdAtMonth'] === date('Y-m-01 00:00:00')))[0]['spaceCount']);
    }

    public function testMesureOfficeTypeDistribution(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEmpty($spaceRepository->mesureOfficeTypeDistribution());

        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true, 'office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE]);
        SpaceFactory::createMany(3, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true, 'office' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE]);

        $this->assertEquals([['spaceCount' => 5, 'office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE], ['spaceCount' => 3, 'office' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE]], $spaceRepository->mesureOfficeTypeDistribution());
    }
}
