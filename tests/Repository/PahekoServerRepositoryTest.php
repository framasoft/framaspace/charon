<?php

namespace App\Tests\Repository;

use App\Factory\PahekoServerFactory;
use App\Factory\SpaceFactory;
use App\Repository\PahekoServerRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class PahekoServerRepositoryTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testProvideAvailablePahekoServersForNewSpaces(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $pahekoServerRepository = $container->get(PahekoServerRepository::class);

        PahekoServerFactory::createOne(['hostname' => 'simple', 'maxServers' => 10]);
        PahekoServerFactory::createOne(['hostname' => 'simple2', 'maxServers' => 10]);
        PahekoServerFactory::createOne(['hostname' => 'tiny', 'maxServers' => 1]);
        PahekoServerFactory::createOne(['hostname' => 'full', 'isFull' => true]);

        $this->assertContains($pahekoServerRepository->getAvailableForNewSpacesPahekoServer()->getHostname(), ['simple', 'simple2', 'tiny']);

        SpaceFactory::createOne(['details' => ['paheko_shared_server' => 'tiny']]);
        SpaceFactory::createOne(['details' => ['paheko_shared_server' => 'simple2']]);
        SpaceFactory::createOne(['details' => ['paheko_shared_server' => 'simple2']]);

        $this->assertEquals('simple', $pahekoServerRepository->getAvailableForNewSpacesPahekoServer()->getHostname());
    }

    public function testProvideAvailablePahekoServersForNewSpacesWithoutAvailable(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        /** @var PahekoServerRepository $pahekoServerRepository */
        $pahekoServerRepository = $container->get(PahekoServerRepository::class);

        PahekoServerFactory::createOne(['hostname' => 'tiny', 'maxServers' => 1]);
        PahekoServerFactory::createOne(['hostname' => 'full', 'isFull' => true]);

        SpaceFactory::createOne(['details' => ['paheko_shared_server' => 'tiny']]);

        $this->assertEquals(null, $pahekoServerRepository->getAvailableForNewSpacesPahekoServer());
    }
}
