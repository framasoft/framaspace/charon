<?php

namespace App\Tests\MessageHandler;

use App\Entity\Job;
use App\Factory\SpaceFactory;
use App\HTTP\Nextcloud\AppPasswordClient;
use App\HTTP\Nextcloud\NotificationClient;
use App\HTTP\Nextcloud\SharingClient;
use App\HTTP\Nextcloud\WebDAVClient;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallAdminUserConfigurationAction;
use App\MessageHandler\NextcloudPostInstallAdminUserConfigurationHandler;
use App\Repository\SpaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class NextcloudPostInstallAdminUserConfigurationHandlerTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    private SpaceRepository|MockObject $spaceRepository;
    private WebDAVClient|MockObject $webdavClient;
    private NotificationClient|MockObject $notificationClient;
    private SharingClient|MockObject $sharingClient;
    private AppPasswordClient|MockObject $appPasswordClient;
    private LoggerInterface|MockObject $logger;
    private MessageBusInterface|MockObject $messageBus;
    private EntityManagerInterface|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->spaceRepository = $this->createMock(SpaceRepository::class);
        $this->webdavClient = $this->createMock(WebDAVClient::class);
        $this->sharingClient = $this->createMock(SharingClient::class);
        $this->notificationClient = $this->createMock(NotificationClient::class);
        $this->appPasswordClient = $this->createMock(AppPasswordClient::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->messageBus = $this->createMock(MessageBusInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);

        parent::setUp();
    }

    public function testHandleNextcloudPostInstallAdminUserConfigurationAction(): void
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->spaceRepository->expects($this->once())->method('find')->with($space->_real()->getId())->willReturn($space->_real());

        $this->webdavClient->expects($this->once())->method('createFolder')->with($space->_real(), 'hello', 'Dossier partagé');
        $this->webdavClient->expects($this->once())->method('createFile')->with($space->_real(), 'hello', 'Dossier partagé/README.md');
        $this->sharingClient->expects($this->once())->method('share')->with($space->_real(), $this->anything(), 'Dossier partagé', 'Everyone', 1, 15);
        $this->notificationClient->expects($this->once())->method('deleteAll')->with($space->_real(), $this->anything());
        $this->appPasswordClient->expects($this->once())->method('deleteAppPassword')->with($space->_real(), $this->anything());
        $this->entityManager->expects($this->once())->method('persist')->with($this->isInstanceOf(Job::class));
        $this->entityManager->expects($this->once())->method('flush');

        $postInstallAdminUserConfigHandler = new NextcloudPostInstallAdminUserConfigurationHandler($this->spaceRepository, $this->webdavClient, $this->sharingClient, $this->notificationClient, $this->appPasswordClient, $this->logger, $this->messageBus, $this->entityManager, 'Dossier partagé', 'Everyone', 'public/Shared_Readme.md');
        $postInstallAdminUserConfigHandler(new NextcloudPostInstallAdminUserConfigurationAction($space->_real()->getId(), 'hello'));
    }

    #[DataProvider(methodName: 'dataTestHandleNextcloudPostInstallAdminUserConfigurationActionWebDAVClientFailure')]
    public function testHandleNextcloudPostInstallAdminUserConfigurationActionWebDAVClientFailure(int $httpReturnCode, string $exceptionClass, bool $continue): void
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->spaceRepository->expects($this->once())->method('find')->with($space->_real()->getId())->willReturn($space->_real());

        $response = new MockResponse('error', ['http_code' => $httpReturnCode]);
        $this->webdavClient->expects($this->once())->method('createFolder')->with($space->_real(), 'hello', 'Dossier partagé')->willThrowException(new $exceptionClass($response));
        $this->webdavClient->expects($continue ? $this->once() : $this->never())->method('createFile')->with($space->_real(), 'hello', 'Dossier partagé/README.md');
        $this->logger->expects($this->once())->method('error');
        $this->messageBus->expects($this->once())->method('dispatch')->with($this->isInstanceOf(AdminLoggingAction::class))->willReturn(new Envelope($this->isInstanceOf(AdminLoggingAction::class)));
        if (!$continue) {
            $this->expectException($exceptionClass);
        }

        $postInstallAdminUserConfigHandler = new NextcloudPostInstallAdminUserConfigurationHandler($this->spaceRepository, $this->webdavClient, $this->sharingClient, $this->notificationClient, $this->appPasswordClient, $this->logger, $this->messageBus, $this->entityManager, 'Dossier partagé', 'Everyone', 'public/Shared_Readme.md');
        $postInstallAdminUserConfigHandler(new NextcloudPostInstallAdminUserConfigurationAction($space->_real()->getId(), 'hello'));
    }

    public static function dataTestHandleNextcloudPostInstallAdminUserConfigurationActionWebDAVClientFailure(): array
    {
        return [
            [405, ClientException::class, true],
            [401, ClientException::class, false],
            [500, ServerException::class, false],
        ];
    }
}
