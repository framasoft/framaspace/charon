<?php

namespace App\Tests\MessageHandler;

use App\Factory\SpaceFactory;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallLastStepAction;
use App\MessageHandler\NextcloudPostInstallLastStepHandler;
use App\Repository\SpaceRepository;
use App\Services\Notifications\Mailer;
use App\Services\RefreshSpaceMonitoring;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class NextcloudPostInstallLastStepHandlerTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    private SpaceRepository|MockObject $spaceRepository;
    private LoggerInterface|MockObject $logger;
    private Mailer|MockObject $mailer;
    private MessageBusInterface|MockObject $messageBus;
    private RefreshSpaceMonitoring|MockObject $refreshSpaceMonitoring;
    private NextcloudPostInstallLastStepHandler $postInstallLastStepHandler;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->spaceRepository = $this->createMock(SpaceRepository::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->mailer = $this->createMock(Mailer::class);
        $this->messageBus = $this->createMock(MessageBusInterface::class);
        $this->refreshSpaceMonitoring = $this->createMock(RefreshSpaceMonitoring::class);
        $this->postInstallLastStepHandler = new NextcloudPostInstallLastStepHandler($this->spaceRepository, $this->logger, $this->mailer, $this->messageBus, $this->refreshSpaceMonitoring);

        parent::setUp();
    }

    public function testHandleNextcloudPostInstallLastStepAction(): void
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->spaceRepository->expects($this->once())->method('find')->with($space->_real()->getId())->willReturn($space);
        $this->mailer->expects($this->once())->method('sendCreationSuccessEmail')->with($space);
        $this->refreshSpaceMonitoring->expects($this->once())->method('refreshSpaceMonitoring')->with($space);
        $this->logger->expects($this->once())->method('info');
        $this->messageBus->expects($this->once())->method('dispatch')->with($this->isInstanceOf(AdminLoggingAction::class))->willReturn(new Envelope($this->isInstanceOf(AdminLoggingAction::class)));

        $this->postInstallLastStepHandler->__invoke(new NextcloudPostInstallLastStepAction($space->_real()->getId()));
    }
}
