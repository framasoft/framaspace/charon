<?php

namespace App\Tests\MessageHandler;

use App\Entity\Nextcloud\Group;
use App\Entity\Nextcloud\User;
use App\Entity\Nextcloud\UserImport;
use App\Factory\SpaceFactory;
use App\HTTP\Nextcloud\ProvisioningClient;
use App\Message\NextcloudImportUsersAction;
use App\MessageHandler\NextcloudImportUsersActionHandler;
use App\Repository\SpaceRepository;
use Faker\Factory as FakerFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class NextcloudImportUsersActionHandlerTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testHandleNextcloudImportUsersAction(): void
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $spaceRepository->expects($this->once())->method('find')->with($space->_real()->getId())->willReturn($space->_real());
        $provisioningClient = $this->createMock(ProvisioningClient::class);
        $messageBus = $this->createMock(MessageBusInterface::class);

        $users = [];
        $faker = FakerFactory::create();
        for ($i = 0; $i < 3; ++$i) {
            $users[] = (new User())->setUserId($faker->unique()->userName())->setDisplayName($faker->name())->setEmail($faker->email())->setGroupIds($faker->words());
        }
        $group = (new Group())->setGroupId('mygroup');
        $userImport = new UserImport($users, [$group]);

        $provisioningClient->expects($this->exactly(3))->method('addUser')->with($space->_real(), $this->isInstanceOf(User::class));
        $provisioningClient->expects($this->once())->method('addGroup')->with($space->_real(), $group);

        $discoursePostActionHandler = new NextcloudImportUsersActionHandler($spaceRepository, $provisioningClient, $messageBus);
        $discoursePostActionHandler(new NextcloudImportUsersAction($space->_real()->getId(), $userImport));
    }
}
