<?php

namespace App\Tests\MessageHandler;

use App\Factory\SubmissionFactory;
use App\Message\DiscoursePostAction;
use App\MessageHandler\DiscoursePostActionHandler;
use App\Repository\SubmissionRepository;
use App\Services\DiscourseService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class DiscoursePostActionHandlerTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testHandleDiscoursePostAction(): void
    {
        self::bootKernel();

        $submission = SubmissionFactory::createOne();

        $submissionRepository = $this->createMock(SubmissionRepository::class);
        $submissionRepository->expects($this->once())->method('find')->with($submission->_real()->getId())->willReturn($submission->_real());
        $discourseService = $this->createMock(DiscourseService::class);
        $discourseService->expects($this->once())->method('postNewSubmission')->with($submission->_real())->willReturn(true);
        $logger = $this->createMock(LoggerInterface::class);
        $discoursePostActionHandler = new DiscoursePostActionHandler($submissionRepository, $discourseService, $logger);
        $discoursePostActionHandler(new DiscoursePostAction($submission->_real()->getId()));
    }
}
