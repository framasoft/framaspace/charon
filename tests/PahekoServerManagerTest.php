<?php

namespace App\Tests;

use App\Entity\PahekoServer;
use App\Exceptions\NoAvailablePahekoServer;
use App\PahekoServerManager;
use App\Repository\PahekoServerRepository;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PahekoServerManagerTest extends KernelTestCase
{
    private MockObject|PahekoServerRepository $pahekoServerRepository;
    private PahekoServerManager $pahekoServerManager;

    protected function setUp(): void
    {
        $this->pahekoServerRepository = $this->createMock(PahekoServerRepository::class);
        $this->pahekoServerManager = new PahekoServerManager($this->pahekoServerRepository);
        parent::setUp();
    }

    #[DataProvider(methodName: 'dataForTestGetNewSharedPahekoServerDetails')]
    public function testGetNewSharedPahekoServerDetails(bool $hasServer, ?string $hostname): void
    {
        $pahekoServer = $this->createMock(PahekoServer::class);
        $pahekoServer->expects($hasServer ? $this->once() : $this->never())->method('getHostname')->willReturn($hostname);

        $this->pahekoServerRepository->expects($this->once())->method('getAvailableForNewSpacesPahekoServer')->willReturn($hasServer ? $pahekoServer : null);
        if (!$hasServer) {
            $this->expectException(NoAvailablePahekoServer::class);
            $this->pahekoServerManager->getNewPahekoServerDetails();
        } else {
            if (!$hostname) {
                $this->expectException(\ValueError::class);
                $this->pahekoServerManager->getNewPahekoServerDetails();
            } else {
                $this->assertEquals('toto', $this->pahekoServerManager->getNewPahekoServerDetails());
            }
        }
    }

    public static function dataForTestGetNewSharedPahekoServerDetails(): array
    {
        return [
            [false, null],
            [true, 'toto'],
            [true, null],
        ];
    }

    #[DataProvider(methodName: 'dataForTestIsPahekoServerFull')]
    public function testIsPahekoServerFull(string $serverName, bool $createPahekoServer, bool $isFull)
    {
        $server = null;
        if ($createPahekoServer) {
            $server = $this->createMock(PahekoServer::class);
        }
        $this->pahekoServerRepository->expects($this->once())->method('findOneBy')->with(['hostname' => $serverName, 'isFull' => true])->willReturn($server);

        $this->assertEquals($isFull, $this->pahekoServerManager->isPahekoServerFull($serverName));
    }

    public static function dataForTestIsPahekoServerFull(): array
    {
        return [
            ['server-available', true, true],
            ['server-not-existing', false, false],
            ['server-full', false, false],
        ];
    }
}
