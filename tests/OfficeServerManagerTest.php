<?php

namespace App\Tests;

use App\Entity\Job;
use App\Entity\OfficeServer;
use App\Entity\Space;
use App\Exceptions\NoAvailableOfficeServer;
use App\Message\AdminLoggingAction;
use App\OfficeServerManager;
use App\Repository\OfficeServerRepository;
use App\Repository\SpaceRepository;
use App\Services\Notifications\AdminNotifierInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class OfficeServerManagerTest extends KernelTestCase
{
    private MockObject|SpaceRepository $spaceRepository;
    private MockObject|OfficeServerRepository $officeServerRepository;
    private MockObject|EntityManagerInterface $objectManager;
    private MockObject|MessageBusInterface $bus;
    private OfficeServerManager $officeServerManager;

    protected function setUp(): void
    {
        $this->spaceRepository = $this->createMock(SpaceRepository::class);
        $this->officeServerRepository = $this->createMock(OfficeServerRepository::class);
        $this->objectManager = $this->createMock(EntityManagerInterface::class);
        $this->bus = $this->createMock(MessageBusInterface::class);
        $this->officeServerManager = new OfficeServerManager($this->spaceRepository, $this->officeServerRepository, $this->objectManager, $this->bus);
        parent::setUp();
    }

    #[DataProvider(methodName: 'dataForTestMaybeSendDeleteOfficeServer')]
    public function testMaybeSendDeleteOfficeServer(int $returnedSpaces): void
    {
        self::bootKernel();
        $sharedOfficePort = 9002;
        $sharedOfficeServer = 'office-1';

        $this->spaceRepository->expects($this->once())->method('countSharedOfficeSpacesBySharedOfficeNameAndPort')->with($sharedOfficeServer, $sharedOfficePort)->willReturn($returnedSpaces);

        if ($returnedSpaces <= 0) {
            $this->objectManager->expects($this->once())->method('persist')->with($this->isInstanceOf(Job::class));
            $this->objectManager->expects($this->once())->method('flush');

            $adminLoggingAction = new AdminLoggingAction('Removing shared office server '.$sharedOfficeServer.':'.$sharedOfficePort, 'When removing or disabling a space, we found out no space still uses shared office server '.$sharedOfficeServer.':'.$sharedOfficePort.", so we're removing it", ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]);
            $this->bus->expects($this->once())->method('dispatch')->with($adminLoggingAction)->willReturn(new Envelope($adminLoggingAction));
        } else {
            $this->objectManager->expects($this->never())->method('persist');
            $this->objectManager->expects($this->never())->method('flush');

            $this->bus->expects($this->never())->method('dispatch');
        }
        $this->officeServerManager->maybeSendDeleteOfficeServer($sharedOfficeServer, $sharedOfficePort);
    }

    public static function dataForTestMaybeSendDeleteOfficeServer(): array
    {
        return [
            [0],
            [7],
        ];
    }

    #[DataProvider(methodName: 'dataForTestGetNewSharedOfficeDetails')]
    public function testGetNewSharedOfficeDetails(int $officeType, bool $createOfficeServer, ?int $port): void
    {
        $officeServer = null;
        if ($createOfficeServer) {
            $officeServer = $this->createMock(OfficeServer::class);
            $officeServer->expects($this->once())->method('getHostname')->willReturn('toto');
        }

        $this->officeServerRepository->expects($this->once())->method('getAvailableForNewSpacesOfficeServer')->with($officeType)->willReturn($officeServer);
        if (is_null($officeServer)) {
            $this->expectException(NoAvailableOfficeServer::class);
            $this->officeServerManager->getNewSharedOfficeDetails($officeType);
        } else {
            $this->spaceRepository->expects($this->once())->method('findAvailablePortForSharedOfficeServer')->with($officeServer)->willReturn($port);
            $this->assertEquals(['toto', $port], $this->officeServerManager->getNewSharedOfficeDetails($officeType));
        }
    }

    public static function dataForTestGetNewSharedOfficeDetails(): array
    {
        return [
            [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, false, null],
            [Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE, true, 671],
        ];
    }
}
