<?php

namespace App\Tests\EventListener;

use App\Entity\Job;
use App\EventListener\JobChangedNotifier;
use App\Factory\JobFactory;
use App\Message\AdminLoggingAction;
use App\Message\NextcloudPostInstallAdminUserConfigurationAction;
use App\Message\NextcloudPostInstallLastStepAction;
use App\Services\AdminUrlGenerator;
use App\SpaceManager;
use PHPUnit\Framework\Attributes\DataProvider;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class JobChangedNotifierTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    #[DataProvider(methodName: 'postUpdateData')]
    public function testPostUpdate(JobFactory $jobFactory, ?string $method): void
    {
        self::bootKernel();
        $job = $jobFactory->create();

        $bus = $this->createMock(MessageBusInterface::class);
        $adminUrlGenerator = $this->createMock(AdminUrlGenerator::class);
        $logger = $this->createMock(LoggerInterface::class);

        $spaceManager = $this->createMock(SpaceManager::class);
        if ($method) {
            $spaceManager->expects($this->once())->method($method)->with($job->_real()->getSpace());
            $bus->expects($this->never())->method('dispatch');
            $adminUrlGenerator->expects($this->never())->method('backendAdminJobUrl');
        } else {
            $spaceManager->expects($this->never())->method($this->anything());
            $bus->expects($this->once())->method('dispatch')->with($this->isInstanceOf(AdminLoggingAction::class))->willReturn(new Envelope($this->isInstanceOf(AdminLoggingAction::class)));
            $adminUrlGenerator->expects($this->once())->method('backendAdminJobUrl')->with($job->_real());
        }

        $jobChangedNotifier = new JobChangedNotifier($spaceManager, $bus, $adminUrlGenerator, $logger);
        $jobChangedNotifier->postUpdate($job->_real());
    }

    public static function postUpdateData(): iterable
    {
        return [
            yield [JobFactory::new(['type' => Job::JOB_TYPE_CREATE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'postCreateSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_DELETE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'deleteSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_DISABLE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'postDisableSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_REENABLE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'postReenableSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_CHANGE_OFFICE, 'status' => Job::JOB_STATUS_DONE]), 'postChangeOfficeType'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_CHANGE_OFFICE, 'status' => Job::JOB_STATUS_ERROR]), null],
        ];
    }

    #[DataProvider(methodName: 'dataForTestPostUpdateInstallAdminUserConfigurationWithAppPassword')]
    public function testPostUpdateInstallAdminUserConfigurationWithAppPassword(?string $body, bool $success): void
    {
        self::bootKernel();
        $job = JobFactory::createOne(['type' => Job::JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION, 'status' => Job::JOB_STATUS_DONE, 'body' => $body]);

        $bus = $this->createMock(MessageBusInterface::class);
        $adminUrlGenerator = $this->createMock(AdminUrlGenerator::class);
        $adminUrlGenerator->expects($this->never())->method($this->anything());
        $logger = $this->createMock(LoggerInterface::class);

        $spaceManager = $this->createMock(SpaceManager::class);
        $spaceManager->expects($this->never())->method($this->anything());

        if ($success) {
            $bus->expects($this->once())->method('dispatch')->with($this->isInstanceOf(NextcloudPostInstallAdminUserConfigurationAction::class))->willReturn(new Envelope($this->isInstanceOf(NextcloudPostInstallAdminUserConfigurationAction::class)));
        } else {
            $bus->expects($this->once())->method('dispatch')->with($this->isInstanceOf(AdminLoggingAction::class))->willReturn(new Envelope($this->isInstanceOf(AdminLoggingAction::class)));
        }

        $jobChangedNotifier = new JobChangedNotifier($spaceManager, $bus, $adminUrlGenerator, $logger);
        $jobChangedNotifier->postUpdate($job->_real());
    }

    public static function dataForTestPostUpdateInstallAdminUserConfigurationWithAppPassword(): array
    {
        return [
            [null, false],
            ['not_json', false],
            [json_encode(['app_password' => 'yolo']), true],
        ];
    }

    #[DataProvider(methodName: 'dataForTestPostUpdateDispatchAction')]
    public function testPostUpdateDispatchAction(int $jobType, string $actionClass): void
    {
        self::bootKernel();
        $job = JobFactory::createOne(['type' => $jobType, 'status' => Job::JOB_STATUS_DONE]);

        $bus = $this->createMock(MessageBusInterface::class);
        $adminUrlGenerator = $this->createMock(AdminUrlGenerator::class);
        $adminUrlGenerator->expects($this->never())->method($this->anything());

        $spaceManager = $this->createMock(SpaceManager::class);
        $spaceManager->expects($this->never())->method($this->anything());

        $logger = $this->createMock(LoggerInterface::class);

        $bus->expects($this->once())->method('dispatch')->with($this->isInstanceOf($actionClass))->willReturn(new Envelope($this->isInstanceOf($actionClass)));

        $jobChangedNotifier = new JobChangedNotifier($spaceManager, $bus, $adminUrlGenerator, $logger);
        $jobChangedNotifier->postUpdate($job->_real());
    }

    public static function dataForTestPostUpdateDispatchAction(): array
    {
        return [
            [Job::JOB_TYPE_POST_INSTALL_LAST_STEP, NextcloudPostInstallLastStepAction::class],
            [Job::JOB_TYPE_DELETE_SHARED_OFFICE_SERVER, AdminLoggingAction::class],
            [Job::JOB_TYPE_UNIQUE_DEPLOY_PAHEKO, AdminLoggingAction::class],
        ];
    }

    public function testHandleJobDoneWithoutSpace()
    {
        $job = JobFactory::createOne(['type' => Job::JOB_TYPE_POST_INSTALL_LAST_STEP, 'status' => Job::JOB_STATUS_DONE, 'space' => null]);

        $bus = $this->createMock(MessageBusInterface::class);
        $adminUrlGenerator = $this->createMock(AdminUrlGenerator::class);
        $spaceManager = $this->createMock(SpaceManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('error')->with($this->stringEndsWith('Space not found'));
        $spaceManager->expects($this->never())->method($this->anything());
        $bus->expects($this->never())->method($this->anything());

        $jobChangedNotifier = new JobChangedNotifier($spaceManager, $bus, $adminUrlGenerator, $logger);
        $jobChangedNotifier->postUpdate($job->_real());
    }
}
