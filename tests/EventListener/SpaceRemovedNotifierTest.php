<?php

namespace App\Tests\EventListener;

use App\EventListener\SpaceRemovedNotifier;
use App\Factory\SpaceFactory;
use App\OfficeServerManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostRemoveEventArgs;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceRemovedNotifierTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    #[DataProvider(methodName: 'dataForTestPostRemove')]
    public function testPostRemove(array $details): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['details' => $details])->_enableAutoRefresh();

        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);

        if (count($details) > 0) {
            $officeServerManager->expects($this->once())->method('maybeSendDeleteOfficeServer')->with($details['shared_office_server'], $details['shared_office_port']);
        } else {
            $officeServerManager->expects($this->never())->method('maybeSendDeleteOfficeServer');
        }

        $lifecycleEventArgs = new PostRemoveEventArgs($space->_real(), $objectManager);

        $notifier = new SpaceRemovedNotifier($officeServerManager);
        $notifier->postRemove($lifecycleEventArgs);
    }

    public static function dataForTestPostRemove(): array
    {
        return [
            [[]],
            [['shared_office_port' => 9002, 'shared_office_server' => 'office-1']],
        ];
    }
}
