<?php

namespace App\Tests\EventListener;

use App\Entity\Job;
use App\Entity\Space;
use App\EventListener\SpaceCreatedNotifier;
use App\Factory\SpaceFactory;
use App\Message\AdminLoggingAction;
use App\OfficeServerManager;
use App\Repository\SpaceRepository;
use App\Services\Nextcloud\Generator;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\PahekoDetailsService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use PHPUnit\Framework\Attributes\DataProvider;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceCreatedNotifierTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testPostPersist(): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne();

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoDetailsService = $this->createMock(PahekoDetailsService::class);
        $nextcloudGenerator = $this->createMock(Generator::class);
        $params = $this->createMock(ContainerBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $bus = $this->createMock(MessageBusInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);

        $objectManager->expects($this->once())->method('persist')->with($this->isInstanceOf(Job::class));
        $objectManager->expects($this->once())->method('flush');

        $adminLoggingAction = new AdminLoggingAction('Creating space '.$space->_real()->getDomain(), 'The space '.$space->_real()->getDomain().' has been approved and is scheduled for creation', ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]);
        $bus->expects($this->once())->method('dispatch')->with($adminLoggingAction)->willReturn(new Envelope($adminLoggingAction));

        $lifecycleEventArgs = new PostPersistEventArgs($space->_real(), $objectManager);

        $jobChangedNotifier = new SpaceCreatedNotifier($spaceRepository, $officeServerManager, $pahekoDetailsService, $nextcloudGenerator, $params, $logger, $bus);
        $jobChangedNotifier->postPersist($lifecycleEventArgs);
    }

    #[DataProvider(methodName: 'dataForTestPrePersist')]
    public function testPrePersist(int $officeType): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['office' => $officeType]);

        $spaceRepository = $this->createMock(SpaceRepository::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoDetailsService = $this->createMock(PahekoDetailsService::class);
        $nextcloudGenerator = $this->createMock(Generator::class);
        $params = $this->createMock(ContainerBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $bus = $this->createMock(MessageBusInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);

        $objectManager->expects($this->never())->method('persist');
        $objectManager->expects($this->never())->method('flush');

        $bus->expects($this->never())->method('dispatch');

        $lifecycleEventArgs = new PrePersistEventArgs($space->_real(), $objectManager);

        if (OfficeServerManager::isSharedOfficeType($officeType)) {
            $officeServerManager->expects($this->once())->method('getNewSharedOfficeDetails')->with($officeType)->willReturn(['shared', 9003]);
        } else {
            $officeServerManager->expects($this->once())->method('getNewStandaloneOfficeDetails')->willReturn(['single', 10034]);
        }

        $jobChangedNotifier = new SpaceCreatedNotifier($spaceRepository, $officeServerManager, $pahekoDetailsService, $nextcloudGenerator, $params, $logger, $bus);
        $jobChangedNotifier->prePersist($lifecycleEventArgs);
        $space->_save();

        if (OfficeServerManager::isSharedOfficeType($officeType)) {
            $this->assertEquals(9003, $space->_real()->getDetails()['shared_office_port']);
            $this->assertEquals('shared', $space->_real()->getDetails()['shared_office_server']);
            $this->assertNotContains('office_server', $space->_real()->getDetails());
            $this->assertNotContains('office_port', $space->_real()->getDetails());
        } else {
            $this->assertEquals(10034, $space->_real()->getDetails()['office_port']);
            $this->assertEquals('single', $space->_real()->getDetails()['office_server']);
            $this->assertNotContains('shared_office_port', $space->_real()->getDetails());
            $this->assertNotContains('shared_office_server', $space->_real()->getDetails());
        }
    }

    public static function dataForTestPrePersist(): array
    {
        return [
            [Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE],
            [Space::SPACE_OFFICE_TYPE_ONLYOFFICE],
            [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE],
            [Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE],
        ];
    }
}
