<?php

namespace App\Tests\EventListener;

use App\EventListener\SubmissionChangedListener;
use App\Events\SubmissionCreatedEvent;
use App\Factory\SubmissionFactory;
use App\Message\AdminLoggingAction;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\AdminNotifierInterface;
use App\Services\Notifications\Mailer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SubmissionChangedNotifierTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    public function testPostUpdate(): void
    {
        self::bootKernel();
        $submission = SubmissionFactory::createOne();

        $mailer = $this->createMock(Mailer::class);
        $mailer->expects($this->once())->method('sendPendingEmail')->with($submission->_real());

        $adminBackendURL = 'Some URL to backend';

        $adminURLGenerator = $this->createMock(AdminUrlGenerator::class);
        $adminURLGenerator->expects($this->once())->method('backendAdminSubmissionURL')->with($submission->_real())->willReturn($adminBackendURL);

        $action = new AdminLoggingAction('Submission for '.$submission->getName().' was created', $submission->getIdentity().' created submission request n°'.$submission->getId().' for organisation '.$submission->getName()."\n\nDetails of the submission can be found on ".$adminBackendURL."\n\nCheers,\n\nCharon, for the Frama.space team.", ['type' => AdminNotifierInterface::TYPE_INFO, 'priority' => AdminNotifierInterface::PRIORITY_LOW]);

        $bus = $this->createMock(MessageBusInterface::class);
        $bus->expects($this->once())->method('dispatch')->with($action)->willReturn(new Envelope($action));

        $jobChangedNotifier = new SubmissionChangedListener($mailer, $adminURLGenerator, $bus);
        $jobChangedNotifier->handleSubmissionCreated(new SubmissionCreatedEvent($submission->_real()));
    }
}
