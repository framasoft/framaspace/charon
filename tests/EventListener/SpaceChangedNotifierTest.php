<?php

namespace App\Tests\EventListener;

use App\Entity\Space;
use App\EventListener\SpaceChangedNotifier;
use App\Factory\SpaceFactory;
use App\OfficeServerManager;
use App\PahekoServerManager;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\UnitOfWork;
use PHPUnit\Framework\Attributes\DataProvider;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceChangedNotifierTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    #[DataProvider(methodName: 'dataForTestPostUpdate')]
    public function testPostUpdate(SpaceFactory $factory, bool $newJob): void
    {
        self::bootKernel();
        $space = $factory->create();

        $spaceManager = $this->createMock(SpaceManager::class);
        $spaceManager->expects($newJob ? $this->once() : $this->never())->method('sendChangeOfficeJobs')->with($space->_real());
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $unitOfWork = $this->createMock(UnitOfWork::class);
        $objectManager->expects($this->once())->method('getUnitOfWork')->willReturn($unitOfWork);

        $lifecycleEventArgs = new PostUpdateEventArgs($space->_real(), $objectManager);

        $jobChangedNotifier = new SpaceChangedNotifier($spaceManager, $officeServerManager, $pahekoServerManager, $logger);
        $jobChangedNotifier->postUpdate($lifecycleEventArgs);
    }

    public static function dataForTestPostUpdate(): iterable
    {
        yield [SpaceFactory::new(), false];
        yield [SpaceFactory::new(['office' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE, 'futureOfficeType' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE]), true];
        yield [SpaceFactory::new(['office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'futureOfficeType' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE]), true];
        yield [SpaceFactory::new(['office' => Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE, 'futureOfficeType' => null]), false];
        yield [SpaceFactory::new(['office' => Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, 'futureOfficeType' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, 'details' => [
            'shared_office_server' => 'office-1', 'shared_office_port' => 9007,
        ]]), true];
    }

    #[DataProvider(methodName: 'dataForTestPreUpdateOfficeChange')]
    public function testPreUpdateOfficeChange(int $oldOfficeType, int $newOfficeType, array $initialSettings, bool $existing = false): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['office' => $oldOfficeType, 'futureOfficeType' => null, 'details' => $initialSettings]);

        $spaceManager = $this->createMock(SpaceManager::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $unitOfWork = $this->createMock(UnitOfWork::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $objectManager->expects($this->once())->method('getUnitOfWork')->willReturn($unitOfWork);
        $objectManager->expects($this->once())->method('getClassMetadata')->with(Space::class)->willReturn($classMetadata);
        $logger->expects($this->never())->method('error');

        if (OfficeServerManager::isSharedOfficeType($newOfficeType)) {
            $officeServerManager->expects($this->once())->method('getNewSharedOfficeDetails')->with($newOfficeType)->willReturn(['office-1', 9003]);
            $matcher = $this->exactly(2);
            $logger->expects($matcher)->method('debug')->willReturnCallback(function (string $message) use ($matcher) {
                match ($matcher->numberOfInvocations()) {
                    1 => $this->assertEquals("Space office type is going to be changed, let's see if we need to change port information", $message),
                    2 => $this->assertEquals("The space office type is being set to shared, let's reset new details for office", $message),
                };
            });
        } elseif (!isset($initialSettings['office_port']) && !isset($initialSettings['office_server'])) {
            $officeServerManager->expects($this->once())->method('getNewStandaloneOfficeDetails')->willReturn(['office-1', 10023]);
            $matcher = $this->exactly(2);
            $logger->expects($matcher)->method('debug')->willReturnCallback(function (string $message) use ($matcher) {
                match ($matcher->numberOfInvocations()) {
                    1 => $this->assertEquals("Space office type is going to be changed, let's see if we need to change port information", $message),
                    2 => $this->assertEquals("The space office type is being set to standalone and we don't already have such details, or the previous office server is full, so let's set them", $message),
                };
            });
        } elseif ($existing) {
            $officeServerManager->expects($this->once())->method('isOfficeServerFull')->willReturn(true);
            $officeServerManager->expects($this->once())->method('getNewStandaloneOfficeDetails')->willReturn(['office-1', 10023]);
            $matcher = $this->exactly(2);
            $logger->expects($matcher)->method('debug')->willReturnCallback(function (string $message) use ($matcher) {
                match ($matcher->numberOfInvocations()) {
                    1 => $this->assertEquals("Space office type is going to be changed, let's see if we need to change port information", $message),
                    2 => $this->assertEquals("The space office type is being set to standalone and we don't already have such details, or the previous office server is full, so let's set them", $message),
                };
            });
        } else {
            $matcher = $this->exactly(2);
            $logger->expects($matcher)->method('debug')->willReturnCallback(function (string $message) use ($matcher) {
                match ($matcher->numberOfInvocations()) {
                    1 => $this->assertEquals("Space office type is going to be changed, let's see if we need to change port information", $message),
                    2 => $this->assertEquals('We changed from one standalone office type to another (20 to 10), nothing to do', $message),
                };
            });
        }

        $changes = ['futureOfficeType' => [$oldOfficeType, $newOfficeType]];

        $lifecycleEventArgs = new PreUpdateEventArgs($space->_real(), $objectManager, $changes);

        $jobChangedNotifier = new SpaceChangedNotifier($spaceManager, $officeServerManager, $pahekoServerManager, $logger);
        $jobChangedNotifier->preUpdate($lifecycleEventArgs);
        $space->_save();
    }

    public static function dataForTestPreUpdateOfficeChange(): array
    {
        return [
            [Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, []],
            [Space::SPACE_OFFICE_TYPE_ONLYOFFICE, Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, []],
            [Space::SPACE_OFFICE_TYPE_ONLYOFFICE, Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, ['office_port' => 10007, 'office_server' => 'office-1']],
            [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, ['shared_office_port' => 9002, 'shared_office_server' => 'office-1']],
            [Space::SPACE_OFFICE_TYPE_SHARED_ONLYOFFICE, Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, ['shared_office_port' => 8004, 'shared_office_server' => 'office-1']],
            [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE, Space::SPACE_OFFICE_TYPE_ONLYOFFICE, ['office_port' => 10007, 'office_server' => 'office-1'], true],
        ];
    }

    public function testPreUpdateDeactivation(): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['enabled' => true, 'details' => ['shared_office_port' => 9002, 'shared_office_server' => 'office-1', 'office_port' => 10034, 'office_server' => 'office-1']]);

        $spaceManager = $this->createMock(SpaceManager::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $unitOfWork = $this->createMock(UnitOfWork::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $objectManager->expects($this->once())->method('getUnitOfWork')->willReturn($unitOfWork);
        $objectManager->expects($this->once())->method('getClassMetadata')->with(Space::class)->willReturn($classMetadata);
        $logger->expects($this->once())->method('debug');

        $changes = ['enabled' => [true, false]];

        $lifecycleEventArgs = new PreUpdateEventArgs($space->_real(), $objectManager, $changes);

        $jobChangedNotifier = new SpaceChangedNotifier($spaceManager, $officeServerManager, $pahekoServerManager, $logger);
        $jobChangedNotifier->preUpdate($lifecycleEventArgs);
        $space->_save();

        $details = $space->_real()->getDetails();
        $this->assertArrayNotHasKey('shared_office_port', $details);
        $this->assertArrayNotHasKey('shared_office_server', $details);
        $this->assertArrayNotHasKey('office_port', $details);
        $this->assertArrayNotHasKey('office_server', $details);
    }

    #[DataProvider(methodName: 'dataForTestPreUpdateReactivation')]
    public function testPreUpdateReactivation(int $officeType): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['enabled' => false, 'office' => $officeType, 'details' => []]);

        $spaceManager = $this->createMock(SpaceManager::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $unitOfWork = $this->createMock(UnitOfWork::class);
        $classMetadata = $this->createMock(ClassMetadata::class);
        $objectManager->expects($this->once())->method('getUnitOfWork')->willReturn($unitOfWork);
        $objectManager->expects($this->once())->method('getClassMetadata')->with(Space::class)->willReturn($classMetadata);
        $logger->expects($this->once())->method('debug');

        $changes = ['enabled' => [false, true]];

        if (OfficeServerManager::isSharedOfficeType($officeType)) {
            $officeServerManager->expects($this->once())->method('getNewSharedOfficeDetails')->with($officeType)->willReturn(['office-1', 9003]);
        } else {
            $officeServerManager->expects($this->once())->method('getNewStandaloneOfficeDetails')->willReturn(['office-1', 10023]);
        }

        $lifecycleEventArgs = new PreUpdateEventArgs($space->_real(), $objectManager, $changes);

        $jobChangedNotifier = new SpaceChangedNotifier($spaceManager, $officeServerManager, $pahekoServerManager, $logger);
        $jobChangedNotifier->preUpdate($lifecycleEventArgs);
        $space->_save();

        $details = $space->_real()->getDetails();
        if (OfficeServerManager::isSharedOfficeType($officeType)) {
            $this->assertArrayHasKey('shared_office_port', $details);
            $this->assertArrayHasKey('shared_office_server', $details);
            $this->assertArrayNotHasKey('office_port', $details);
            $this->assertArrayNotHasKey('office_server', $details);
        } else {
            $this->assertArrayHasKey('office_port', $details);
            $this->assertArrayHasKey('office_server', $details);
            $this->assertArrayNotHasKey('shared_office_port', $details);
            $this->assertArrayNotHasKey('shared_office_server', $details);
        }
    }

    public static function dataForTestPreUpdateReactivation(): array
    {
        return [
            [Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE],
            [Space::SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE],
        ];
    }

    #[DataProvider(methodName: 'dataForTestPostUpdateDeactivation')]
    public function testPostUpdateDeactivation(array $params, bool $shouldCheckDelete): void
    {
        self::bootKernel();
        $space = SpaceFactory::createOne(['details' => $params, 'enabled' => false]);

        $spaceManager = $this->createMock(SpaceManager::class);
        $officeServerManager = $this->createMock(OfficeServerManager::class);
        $pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $unitOfWork = $this->createMock(UnitOfWork::class);
        $unitOfWork->expects($this->once())->method('getEntityChangeset')->willReturn(['enabled' => [false, true]]);
        $objectManager->expects($this->once())->method('getUnitOfWork')->willReturn($unitOfWork);

        if ($shouldCheckDelete) {
            $officeServerManager->expects($this->once())->method('maybeSendDeleteOfficeServer')->with($params['shared_office_server'], $params['shared_office_port']);
        }

        $lifecycleEventArgs = new PostUpdateEventArgs($space->_real(), $objectManager);

        $jobChangedNotifier = new SpaceChangedNotifier($spaceManager, $officeServerManager, $pahekoServerManager, $logger);
        $jobChangedNotifier->postUpdate($lifecycleEventArgs);
    }

    public static function dataForTestPostUpdateDeactivation(): array
    {
        return [
            [[], false],
            [['shared_office_port' => 9003, 'shared_office_server' => 'office-1'], true],
        ];
    }
}
