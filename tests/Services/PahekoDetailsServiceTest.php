<?php

namespace App\Tests\Services;

use App\PahekoServerManager;
use App\Services\Nextcloud\Generator;
use App\Services\PahekoDetailsService;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PahekoDetailsServiceTest extends KernelTestCase
{
    private Generator|MockObject $generator;
    private PahekoServerManager|MockObject $pahekoServerManager;
    private PahekoDetailsService $pahekoDetailsService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->generator = $this->createMock(Generator::class);
        $this->pahekoServerManager = $this->createMock(PahekoServerManager::class);
        $this->pahekoDetailsService = new PahekoDetailsService($this->generator, $this->pahekoServerManager);
    }

    public function testGetDetails(): void
    {
        $this->generator->expects($this->once())->method('generatePahekoSharedKey')->willReturn('shared-key');
        $this->generator->expects($this->once())->method('generatePahekoAPIUserName')->with('some-space')->willReturn('somespaceapiuser');
        $this->generator->expects($this->once())->method('generatePahekoAPIUserPassword')->willReturn('somepw');
        $this->pahekoServerManager->expects($this->once())->method('getNewPahekoServerDetails')->willReturn('paheko-1');

        $this->assertEquals([
            'paheko_shared_server' => 'paheko-1',
            'paheko_shared_secret_key' => 'shared-key',
            'paheko_api_user' => 'somespaceapiuser',
            'paheko_api_password' => 'somepw',
        ], $this->pahekoDetailsService->getDetails('some-space'));
    }
}
