<?php

namespace App\Tests\Services;

use App\Factory\SubmissionFactory;
use App\Services\AdminUrlGenerator;
use App\Services\DiscourseService;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use PHPUnit\Framework\Attributes\DataProvider;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class DiscourseServiceTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    #[DataProvider(methodName: 'postSubmissionProvider')]
    public function testPostSubmission(MockResponse $mockResponse, bool $success): void
    {
        self::bootKernel();

        $discourseEndpoint = 'https://forum.somewhere.com';
        $discourseUsername = 'username';
        $discourseAPIKey = 'api_key';
        $discourseCategory = '5';
        $results = [$discourseEndpoint, $discourseUsername, $discourseAPIKey, $discourseCategory];

        $httpClient = new MockHttpClient($mockResponse);
        $params = $this->createMock(ContainerBagInterface::class);
        $matcher = $this->exactly(4);
        $params->expects($matcher)->method('get')->willReturnCallback(function (string $id) use ($matcher, $results) {
            match ($matcher->numberOfInvocations()) {
                1 => $this->assertEquals('app.discourse.endpoint', $id),
                2 => $this->assertEquals('app.discourse.username', $id),
                3 => $this->assertEquals('app.discourse.api_key', $id),
                4 => $this->assertEquals('app.discourse.category', $id),
            };

            return $results[$matcher->numberOfInvocations() - 1];
        });

        $twig = $this->createMock(Environment::class);
        $twig->expects($this->once())->method('render')->with('space/submission/discourse.md.twig', $this->anything());
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method($success ? 'info' : 'warning');
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->once())->method('trans');
        $adminUrlGenerator = $this->createMock(AdminUrlGenerator::class);
        $doctrine = $this->createMock(ManagerRegistry::class);
        $em = $this->createMock(ObjectManager::class);
        if ($success) {
            $doctrine->expects($this->once())->method('getManager')->willReturn($em);
            $em->expects($this->once())->method('persist');
            $em->expects($this->once())->method('flush');
        }

        $discourseService = new DiscourseService($httpClient, $params, $twig, $logger, $translator, $adminUrlGenerator, $doctrine);

        $submission = SubmissionFactory::createOne();

        $this->assertEquals($success, $discourseService->postNewSubmission($submission->_real()));
        $this->assertSame('POST', $mockResponse->getRequestMethod());
        $this->assertStringEndsWith('/posts', $mockResponse->getRequestUrl());
        $this->assertContains('Api-Username: '.$discourseUsername, $mockResponse->getRequestOptions()['headers']);
        $this->assertContains('Api-Key: '.$discourseAPIKey, $mockResponse->getRequestOptions()['headers']);
    }

    public static function postSubmissionProvider(): array
    {
        $json200 = file_get_contents(__DIR__.'/../Fixtures/Services/Discourse/200.json');
        $mockResponseOK = new MockResponse($json200, ['http_code' => 200]);

        $json422 = file_get_contents(__DIR__.'/../Fixtures/Services/Discourse/422.json');
        $mockResponseFailure = new MockResponse($json422, ['http_code' => 422]);

        return [
            [$mockResponseOK, true],
            [$mockResponseFailure, false],
        ];
    }
}
