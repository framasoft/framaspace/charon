<?php

namespace App\Tests\Services;

use App\Entity\MonitoringReport;
use App\Exceptions\Nextcloud\NextcloudRemoteHTTPException;
use App\Exceptions\Nextcloud\NextcloudRemoteOCSException;
use App\Factory\SpaceFactory;
use App\HTTP\Nextcloud\ServerInfoClient;
use App\Repository\MonitoringReportRepository;
use App\Services\RefreshSpaceMonitoring;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class RefreshSpaceMonitoringTest extends KernelTestCase
{
    use ResetDatabase;
    use Factories;

    private MockObject|ServerInfoClient $serverInfoClient;
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|MonitoringReportRepository $monitoringReportRepository;
    private RefreshSpaceMonitoring $refreshSpaceMonitoring;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->serverInfoClient = $this->createMock(ServerInfoClient::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->monitoringReportRepository = $this->createMock(MonitoringReportRepository::class);
        $logger = $this->createMock(LoggerInterface::class);

        $this->refreshSpaceMonitoring = new RefreshSpaceMonitoring($this->serverInfoClient, $this->entityManager, $this->monitoringReportRepository, $logger, 40);
    }

    public function testRefreshSpaceMonitoringOK()
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->serverInfoClient->expects($this->once())->method('getStatistics')->with($space)->willReturn([
            'nextcloud' => [
                'system' => [
                    'freespace' => 10_737_418_240,
                ],
                'storage' => [
                    'num_users' => 26,
                    'num_files' => 28_479,
                ],
                'shares' => [
                    'num_shares' => 129,
                ],
            ],
            'activeUsers' => [
                'last5minutes' => 1,
                'last1hour' => 5,
                'last24hours' => 7,
                'last7days' => 13,
                'last1month' => 15,
                'last3months' => 15,
                'last6months' => 19,
                'lastyear' => 20,
            ],
        ]);

        $this->entityManager->expects($this->once())->method('persist')->with($this->callback(function ($monitoringReport) use ($space) {
            return $monitoringReport instanceof MonitoringReport
                && $monitoringReport->getSpace() == $space
                && $monitoringReport->getUsedSpace() == 40 * 1_073_741_824 - 10_737_418_240
                && 26 == $monitoringReport->getUsersNumber()
                && 7 == $monitoringReport->getUsersLastActive24HNumber()
                && 15 == $monitoringReport->getUsersLastActive3MonthNumber()
                && 28_479 == $monitoringReport->getFilesNumber()
                && 129 == $monitoringReport->getSharesNumber()
                && 200 == $monitoringReport->getReturnStatusCode()
                && null == $monitoringReport->getReturnErrorDetails();
        }))->willReturnCallback(function ($r) {
            if ($r instanceof MonitoringReport) {
                $r->setId(4);
            }
        });
        $this->entityManager->expects($this->once())->method('flush');

        $this->monitoringReportRepository->expects($this->once())->method('removeNextRunForOldEntries')->with($space->getId(), 4);

        $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
    }

    public function testRefreshSpaceMonitoring401()
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->serverInfoClient->expects($this->once())->method('getStatistics')->with($space)->willThrowException(new NextcloudRemoteOCSException(401, 'something bad', 'framadmin', $space->getDomain(), 'something response bad'));

        $this->entityManager->expects($this->once())->method('persist')->with($this->callback(function ($monitoringReport) {
            return $monitoringReport instanceof MonitoringReport && 401 == $monitoringReport->getReturnStatusCode() && 'something bad' == $monitoringReport->getReturnErrorDetails();
        }))->willReturnCallback(function ($r) {
            if ($r instanceof MonitoringReport) {
                $r->setId(4);
            }
        });
        $this->entityManager->expects($this->once())->method('flush');
        $this->monitoringReportRepository->expects($this->once())->method('removeNextRunForOldEntries')->with($space->getId(), 4);

        $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
    }

    public function testRefreshSpaceMonitoring404()
    {
        self::bootKernel();

        $space = SpaceFactory::createOne();

        $this->serverInfoClient->expects($this->once())->method('getStatistics')->with($space)->willThrowException(new NextcloudRemoteHTTPException(404, 'framadmin', $space->getDomain(), 'something response bad'));

        $this->entityManager->expects($this->once())->method('persist')->with($this->callback(function ($monitoringReport) {
            return $monitoringReport instanceof MonitoringReport && 404 == $monitoringReport->getReturnStatusCode() && 'something response bad' == $monitoringReport->getReturnErrorDetails();
        }))->willReturnCallback(function ($r) {
            if ($r instanceof MonitoringReport) {
                $r->setId(4);
            }
        });
        $this->entityManager->expects($this->once())->method('flush');
        $this->monitoringReportRepository->expects($this->once())->method('removeNextRunForOldEntries')->with($space->getId(), 4);

        $this->refreshSpaceMonitoring->refreshSpaceMonitoring($space);
    }
}
