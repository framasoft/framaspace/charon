<?php

namespace App\Tests\Controller;

use App\Entity\Job;
use App\Entity\NormalizedOrganization;
use App\Entity\Space;
use App\Factory\ConfigFactory;
use App\Factory\JobFactory;
use App\Factory\SpaceFactory;
use App\Factory\SubmissionFactory;
use App\Services\OrganizationLookup\CheckRNAService;
use App\Services\OrganizationLookup\CheckSIRETService;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class APIControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    public function testSearchOrganisationWithException(): void
    {
        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Framasoft')->willThrowException(new \Exception('Something went wrong'));
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);
        $this->client->request('POST', '/api/v1/searchOrganization', ['search' => 'Framasoft']);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('Unable to get details from data providers', $content['error']);
        $this->assertEquals('Something went wrong', $content['details']);
    }

    public function testSearchOrganisationWithNoParams(): void
    {
        $this->client->request('POST', '/api/v1/searchOrganization');
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testSearchOrganisationWithNameSearchParam(): void
    {
        $this->client->catchExceptions(false);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Framasoft')->willReturn([
            (new NormalizedOrganization())
            ->setName('FRAMASOFT')
            ->setCreationYear('2003')
            ->setSiren('500715776')
            ->setTrancheEffectifs('11')
            ->setCategorieJuridique('Association déclarée'),
        ]);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $this->client->request('POST', '/api/v1/searchOrganization', ['search' => 'Framasoft']);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('FRAMASOFT', $content[0]['name']);
        $this->assertEquals('2003', $content[0]['creationYear']);
        $this->assertEquals('500715776', $content[0]['siren']);
        $this->assertEquals('11', $content[0]['trancheEffectifs']);
        $this->assertEquals('Association déclarée', $content[0]['categorieJuridique']);
    }

    public function testSearchOrganisationWithBadNameSearchParam(): void
    {
        $this->client->catchExceptions(false);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Something else')->willReturn([]);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $this->client->request('POST', '/api/v1/searchOrganization', ['search' => 'Something else']);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('No results found', $content['error']);
    }

    public function testSearchOrganisationWithSIRENSearchParam(): void
    {
        $this->client->catchExceptions(false);

        $data = json_decode(file_get_contents(__DIR__.'/../Fixtures/Services/SIRET/SIREN_Frama.json'), true);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('500715776')->willReturn($data);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $this->client->request('POST', '/api/v1/searchOrganization', ['search' => '500715776']);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('W751179246', $content['uniteLegale']['identifiantAssociationUniteLegale']);
        $this->assertEquals('2003-12-02', $content['uniteLegale']['dateCreationUniteLegale']);
        $this->assertEquals('2016-01-01', $content['uniteLegale']['periodesUniteLegale'][0]['dateDebut']);
        $this->assertEquals('FRAMASOFT', $content['uniteLegale']['periodesUniteLegale'][0]['denominationUniteLegale']);
        $this->assertEquals('94.99Z', $content['uniteLegale']['periodesUniteLegale'][0]['activitePrincipaleUniteLegale']);
    }

    public function testSearchOrganisationWithRNASearchParam(): void
    {
        $this->client->catchExceptions(false);

        $data = json_decode(json_decode(file_get_contents(__DIR__.'/../Fixtures/Services/RNA/FRAMASOFT.json'), true)['content'], true);

        $rnaServiceMock = $this->createMock(CheckRNAService::class);
        $rnaServiceMock->expects($this->once())->method('getInfosFromText')->with('W751179246')->willReturn($data);
        $container = static::getContainer();
        $container->set(CheckRNAService::class, $rnaServiceMock);

        $this->client->request('POST', '/api/v1/searchOrganization', ['search' => 'W751179246']);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('2003-12-02', $content['association']['date_creation']);
        $this->assertEquals('FRAMASOFT', $content['association']['titre']);
        $this->assertEquals('éducation populaire aux enjeux du numérique et des communes culturels', $content['association']['objet']);
        $this->assertEquals('69007', $content['association']['adresse_code_postal']);
    }

    #[DataProvider(methodName: 'dataForTestIsAvailable')]
    public function testIsAvailable(string $subdomain, int $code): void
    {
        SubmissionFactory::createOne(['domain' => 'taken']);
        SubmissionFactory::createOne(['domain' => 'both']);
        SpaceFactory::createOne(['domain' => 'both']);
        SpaceFactory::createOne(['domain' => 'only-space']);

        $this->client->catchExceptions(false);
        $this->client->request('POST', '/api/v1/isAvailable', ['subdomain' => $subdomain]);
        $this->assertEquals($code, $this->client->getResponse()->getStatusCode());
    }

    public static function dataForTestIsAvailable(): array
    {
        return [
            ['s3', 409],
            ['taken', 409],
            ['', 400],
            ['available', 200],
            ['both', 409],
            ['only-space', 409],
        ];
    }

    public function testGetJobsNotloggedIn(): void
    {
        $this->client->request('GET', '/api/v1/jobs', ['status' => 'done']);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
        $this->assertContains('www-authenticate', array_keys($this->client->getResponse()->headers->all()));
        $this->assertStringContainsString('Basic', $this->client->getResponse()->headers->all()['www-authenticate'][0]);
    }

    #[DataProvider(methodName: 'dataForTestGetJobs')]
    public function testGetJobs(JobFactory $factory): void
    {
        JobFactory::createOne(['status' => Job::JOB_STATUS_ONGOING]);
        $job = $factory->create();
        $this->client->request('GET', '/api/v1/jobs', ['status' => $job->getStatus()], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(Job::JOB_STATUS_ONGOING === $job->_real()->getStatus() ? 2 : 1, $content['items']);
        $this->assertEquals($job->getStatus(), $content['items'][0]['status']);
    }

    public static function dataForTestGetJobs(): iterable
    {
        return [
            yield [JobFactory::new(['status' => Job::JOB_STATUS_PENDING])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_ONGOING])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_ERROR])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_DONE])],
        ];
    }

    public function testGetMultipleJobs(): void
    {
        JobFactory::createMany(15, ['status' => Job::JOB_STATUS_PENDING]);
        $this->client->request('GET', '/api/v1/jobs', ['status' => 'pending'], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(1, $content['pagination']['current_page']);
        $this->assertTrue($content['pagination']['has_next_page']);
        $this->assertEquals(15, $content['pagination']['total_items']);
        $this->assertEquals(2, $content['pagination']['total_pages']);

        $this->client->request('GET', '/api/v1/jobs', ['status' => '0', 'page' => 2], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(5, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);

        $this->client->request('GET', '/api/v1/jobs', ['status' => '0', 'page' => 1, 'perPage' => 30], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(15, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);
    }

    public function testUpdateJob(): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $this->client->request('PUT', '/api/v1/jobs/'.$job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(Job::JOB_STATUS_ONGOING, $content['status']);
    }

    public function testUpdateJobNotFound(): void
    {
        $this->client->request('PUT', '/api/v1/jobs/235', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    #[DataProvider(methodName: 'dataForTestFinalizeJob')]
    public function testFinalizeJob(mixed $success, string $payload, int $status): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $this->client->request('POST', '/api/v1/jobs/'.$job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode(['success' => $success, 'payload' => $payload]));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($status, $content['status']);
        $this->assertEquals('"'.$payload.'"', $content['body']);
    }

    public static function dataForTestFinalizeJob(): array
    {
        return [
            [true, 'whatever done', Job::JOB_STATUS_DONE],
            [false, 'whatever fails', Job::JOB_STATUS_ERROR],
            ['hello', 'whatever unknown', Job::JOB_STATUS_ERROR],
            [true, 'null', Job::JOB_STATUS_DONE],
        ];
    }

    public function testFinalizeJobWithNoPayload(): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $this->client->request('POST', '/api/v1/jobs/'.$job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('Failed to parse JSON request payload', $this->client->getResponse()->getContent());
    }

    public function testGetSpacesNotloggedIn(): void
    {
        $this->client->request('GET', '/api/v1/spaces', ['status' => 'done']);
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
        $this->assertContains('www-authenticate', array_keys($this->client->getResponse()->headers->all()));
        $this->assertStringContainsString('Basic', $this->client->getResponse()->headers->all()['www-authenticate'][0]);
    }

    #[DataProvider(methodName: 'dataForTestGetSpaces')]
    public function testGetSpacesFilterByStatus(SpaceFactory $factory): void
    {
        SpaceFactory::createOne(['status' => Space::SPACE_STATUS_PENDING]);
        $space = $factory->create();
        $this->client->request('GET', '/api/v1/spaces', ['status' => $space->getStatus()], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(Space::SPACE_STATUS_PENDING === $space->_real()->getStatus() ? 2 : 1, $content['items']);
        $this->assertEquals($space->getStatus(), $content['items'][0]['status']);
    }

    public static function dataForTestGetSpaces(): iterable
    {
        return [
            yield [SpaceFactory::new(['status' => Space::SPACE_STATUS_PENDING])],
            yield [SpaceFactory::new(['status' => Space::SPACE_STATUS_CREATED])],
        ];
    }

    public function testGetSpacesFilterByEnabled(): void
    {
        SpaceFactory::createOne(['enabled' => false]);
        $this->client->request('GET', '/api/v1/spaces', ['enabled' => true], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEmpty($content['items']);
    }

    public function testGetMultipleSpaces(): void
    {
        SpaceFactory::createMany(15, ['status' => Space::SPACE_STATUS_PENDING]);
        $this->client->request('GET', '/api/v1/spaces', ['status' => 'pending'], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(1, $content['pagination']['current_page']);
        $this->assertTrue($content['pagination']['has_next_page']);
        $this->assertEquals(15, $content['pagination']['total_items']);
        $this->assertEquals(2, $content['pagination']['total_pages']);

        $this->client->request('GET', '/api/v1/spaces', ['status' => '0', 'page' => 2], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(5, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);

        $this->client->request('GET', '/api/v1/spaces', ['status' => '0', 'page' => 1, 'perPage' => 30], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(15, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);
    }

    public function testUpdateSpace(): void
    {
        $space = SpaceFactory::createOne();
        $this->client->request('PATCH', '/api/v1/spaces/'.$space->_real()->getDomain(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode(['organizationName' => 'updatedOrganizationName']));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('updatedOrganizationName', $content['organizationName']);

        $this->client->request('GET', '/api/v1/spaces/'.$space->_real()->getDomain(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('updatedOrganizationName', $content['organizationName']);

        $this->client->request('PATCH', '/api/v1/spaces/'.$space->_real()->getDomain(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode(['organizationName' => null]));
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
    }

    #[DataProvider(methodName: 'dataForTestSetSpaceMaintenance')]
    public function testSetSpaceMaintenance(array $payload, bool $activated, ?string $message): void
    {
        $space = SpaceFactory::createOne();
        $this->client->request('PUT', '/api/v1/spaces/'.$space->_real()->getDomain().'/maintenance', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode($payload));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($activated, $content['maintenanceActivated']);
        $this->assertEquals($message, $content['maintenanceMessage']);
    }

    public static function dataForTestSetSpaceMaintenance(): array
    {
        return [
            [
                ['activated' => true, 'message' => 'oops'],
                true,
                'oops',
            ],
            [
                ['activated' => false, 'message' => 'OK'],
                false,
                'OK',
            ],
            [
                ['activated' => false],
                false,
                null,
            ],
        ];
    }

    public function testListConfig()
    {
        $config1 = ConfigFactory::createOne();
        $config2 = ConfigFactory::createOne();

        $this->client->request('GET', '/api/v1/config', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($config1->_get('key'), $content['items'][0]['key']);
        $this->assertEquals($config2->_get('key'), $content['items'][1]['key']);
    }

    public function testGetConfig()
    {
        $config = ConfigFactory::createOne();

        $this->client->request('GET', '/api/v1/config/'.$config->_get('key'), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($config->_get('key'), $content['key']);
        $this->assertEquals($config->_get('value'), $content['value']);

        $this->client->request('GET', '/api/v1/config/not_found', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testSetConfig()
    {
        $this->client->request('PUT', '/api/v1/config', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode(['key' => 'my_custom_config', 'value' => '33.8', 'type' => 3]));
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('my_custom_config', $content['key']);
        $this->assertEquals('33.8', $content['value']);
        $this->assertEquals(3, $content['type']);
        $this->assertEquals(33.8, $content['typedValue']);

        $this->client->request('PUT', '/api/v1/config', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ], json_encode(['key' => 'my_custom_config', 'value' => 'hello', 'type' => 1]));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals('my_custom_config', $content['key']);
        $this->assertEquals('hello', $content['value']);
        $this->assertEquals(1, $content['type']);
        $this->assertEquals('hello', $content['typedValue']);
    }

    public function testDeleteConfig()
    {
        $config = ConfigFactory::createOne();

        $this->client->request('DELETE', '/api/v1/config/'.$config->_get('key'), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
        $this->assertEmpty($this->client->getResponse()->getContent());
    }

    public function testDeleteConfigNotFound()
    {
        $this->client->request('DELETE', '/api/v1/config/not_found', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto',
        ]);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }
}
