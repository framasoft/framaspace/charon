<?php

namespace App\Tests\Controller;

use App\Entity\Nextcloud\Group;
use App\Entity\Nextcloud\User;
use App\Entity\Nextcloud\UserImport;
use App\Factory\SpaceFactory;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SpaceControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    public function testImportWithoutLogin(): void
    {
        $this->client->request('GET', '/space/import-users');
        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects('/login');
    }

    #[DataProvider(methodName: 'dataForTestImportSingleUser')]
    public function testImportSingleUser(string $path, UserImport $userImport, string $responseMessage): void
    {
        $space = SpaceFactory::createOne();

        $this->client->loginUser($space->_real());

        $crawler = $this->client->request('GET', '/space/import-users');
        $buttonCrawlerNode = $crawler->selectButton('user_provisioning_import[submit]');
        $form = $buttonCrawlerNode->form();
        $form['user_provisioning_import[file]']->upload($path);
        $this->assertEquals(\UPLOAD_ERR_OK, $form['user_provisioning_import[file]']->getValue()['error'], 'File not found');

        $this->client->submit($form);
        $this->assertResponseRedirects('/space');
        $transport = $this->getContainer()->get('messenger.transport.async');
        $this->assertCount(1, $transport->getSent());
        $crawler = $this->client->followRedirect();
        $this->assertEquals($responseMessage, $crawler->filter('.flash-notice.alert.alert-success')->first()->text());
    }

    public static function dataForTestImportSingleUser(): array
    {
        return [
            [
                __DIR__.'/../../public/import.csv',
                new UserImport([
                    (new User())->setUserId('example')->setEmail('example@demo.net')->setDisplayName('Madame Example'),
                ], []),
                'The import of one user has been scheduled.',
            ],
            [
                __DIR__.'/../Fixtures/import-multiple.csv',
                new UserImport([
                    (new User())->setUserId('example')->setEmail('example@demo.net')->setDisplayName('Madame Example'),
                    (new User())->setUserId('another')->setEmail('another@one.fr')->setDisplayName("It's me!")->setGroupIds(['mygroup']),
                ], [(new Group())->setGroupId('mygroup')]),
                'The import of 2 users has been scheduled.',
            ],
        ];
    }

    #[DataProvider(methodName: 'dataForTestImportBadEncodedFile')]
    public function testImportBadEncodedFile(string $encoding)
    {
        $space = SpaceFactory::createOne();

        $this->client->loginUser($space->_real());

        $crawler = $this->client->request('GET', '/space/import-users');
        $buttonCrawlerNode = $crawler->selectButton('user_provisioning_import[submit]');
        $form = $buttonCrawlerNode->form();
        $form['user_provisioning_import[file]']->upload(__DIR__.'/../Fixtures/import-repaired-'.$encoding.'.csv');
        $this->assertEquals(\UPLOAD_ERR_OK, $form['user_provisioning_import[file]']->getValue()['error'], 'File not found');
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        $this->assertEquals('The import of one user has been scheduled.', $crawler->filter('.flash-notice.alert.alert-success')->first()->text());
    }

    public static function dataForTestImportBadEncodedFile(): array
    {
        return [
            ['ISO-8859-1'],
            ['Windows-1252'],
        ];
    }
}
