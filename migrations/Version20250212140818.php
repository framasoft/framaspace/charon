<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250212140818 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add indexes on monitoring reports and space tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX monitoring_report_active_last_year_number_updated_at_idx ON monitoring_report (users_last_active_last_year_number, updated_at)');
        $this->addSql('CREATE INDEX monitoring_report_active3_month_number_updated_at_idx ON monitoring_report (users_last_active3_month_number, updated_at)');
        $this->addSql('CREATE INDEX monitoring_report_space_id_updated_at_idx ON monitoring_report (space_id, updated_at)');
        $this->addSql('CREATE INDEX space_status_idx ON space (status)');
        $this->addSql('CREATE INDEX job_status_idx ON job (status)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX monitoring_report_active_last_year_number_updated_at_idx');
        $this->addSql('DROP INDEX monitoring_report_active3_month_number_updated_at_idx');
        $this->addSql('DROP INDEX monitoring_report_space_id_updated_at_idx');
        $this->addSql('DROP INDEX space_status_idx');
        $this->addSql('DROP INDEX job_status_idx');
    }
}
