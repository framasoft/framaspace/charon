<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240808074317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE monitoring_report_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE monitoring_report (id INT NOT NULL, space_id INT NOT NULL, used_space BIGINT DEFAULT NULL, users_number INT DEFAULT NULL, users_last_active24_hnumber INT DEFAULT NULL, files_number BIGINT DEFAULT NULL, shares_number BIGINT DEFAULT NULL, return_status_code INT NOT NULL, return_error_details TEXT DEFAULT NULL, next_run TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D7115FED23575340 ON monitoring_report (space_id)');
        $this->addSql('COMMENT ON COLUMN monitoring_report.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN monitoring_report.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE monitoring_report ADD CONSTRAINT FK_D7115FED23575340 FOREIGN KEY (space_id) REFERENCES space (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE monitoring_report_id_seq CASCADE');
        $this->addSql('ALTER TABLE monitoring_report DROP CONSTRAINT FK_D7115FED23575340');
        $this->addSql('DROP TABLE monitoring_report');
    }
}
