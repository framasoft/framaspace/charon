<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109092932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE space ADD object TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD topics JSON DEFAULT \'[]\' NOT NULL');
        $this->addSql('ALTER TABLE space ADD main_actions TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD creation_year VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD website VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD nb_employees BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD nb_members BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD nb_beneficiaries BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD budget BIGINT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD accounts_needed VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD country VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD responsible INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE space ADD organisation_language VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD organisation_type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD reasons TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE space ADD external_identifier VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE space DROP object');
        $this->addSql('ALTER TABLE space DROP topics');
        $this->addSql('ALTER TABLE space DROP main_actions');
        $this->addSql('ALTER TABLE space DROP creation_year');
        $this->addSql('ALTER TABLE space DROP website');
        $this->addSql('ALTER TABLE space DROP nb_employees');
        $this->addSql('ALTER TABLE space DROP nb_members');
        $this->addSql('ALTER TABLE space DROP nb_beneficiaries');
        $this->addSql('ALTER TABLE space DROP budget');
        $this->addSql('ALTER TABLE space DROP accounts_needed');
        $this->addSql('ALTER TABLE space DROP country');
        $this->addSql('ALTER TABLE space DROP responsible');
        $this->addSql('ALTER TABLE space DROP organisation_language');
        $this->addSql('ALTER TABLE space DROP organisation_type');
        $this->addSql('ALTER TABLE space DROP reasons');
        $this->addSql('ALTER TABLE space DROP external_identifier');
    }
}
