<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241022093323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active1_month_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active6_month_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active5_minutes_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active1_hour_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active7_days_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active3_month_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monitoring_report ADD users_last_active_last_year_number INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active1_month_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active6_month_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active5_minutes_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active1_hour_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active7_days_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active3_month_number');
        $this->addSql('ALTER TABLE monitoring_report DROP users_last_active_last_year_number');
    }
}
