Ce dossier partagé est accessible à l'ensemble des utilisateur⋅ices de cet espace. C'est à dire que toute personne ayant un compte sur cet espace pourra **lire, modifier ou supprimer** les fichiers ou sous-dossiers que ce dossier contient.

Évidemment, le responsable de l'espace peut en modifier les accès via l’icône "Partager".